﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GeneralProject.General.BL;

namespace Intranet.DesktopModules.GeneralProject.General.ModalForm
{      
    public partial class UserConfig : System.Web.UI.Page
    {

        private readonly UserBL _userBL = new UserBL();
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Title = "تنظیمات کاربری";
            if (HttpContext.Current.User == null || !HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/FrmLogin2.aspx");
            }
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            _userBL.ChangePassword(userId, txtPassword.Text);
            lblMessage.Text = "ثبت موفق";
        }
    }
}
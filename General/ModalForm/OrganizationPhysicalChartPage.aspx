﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrganizationPhysicalChartPage.aspx.cs"
    Inherits="Intranet.DesktopModules.Generalproject.General.ModalForm.OrganizationPhysicalChartPage" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
       <script type="text/javascript">

   function onNodeClicking(sender, args) {

       var oArg = new Object();
            oArg.Title = args.get_node().get_text() ;
            oArg.KeyId =args.get_node().get_value();    
            var oWnd = GetRadWindow();
            oWnd.close(oArg);

        }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
   
     
        </script>

    </telerik:RadCodeBlock>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table dir="rtl" align="right" width="100%">
            <tr>
        <td width="10%" >
        <asp:Label ID="labOrganizationPhysicalChart" runat="server" 
                Text="عنوان نمودارفیزیکی:"></asp:Label>
            
        </td>
           
        <td width="20%">
        
            <asp:TextBox ID="txtOrganizationPhysicalChartSearchTitle" runat="server" 
                Width="200px"></asp:TextBox>
        
        </td>
                  <td>                
                <asp:ImageButton ID="imgbtnOrganizationPhysicalChartHighlightall" runat="server" 
               ImageUrl="../Images/SearchOrganizationPhysicalChart.png" Width="20px" Height="20px" 
                CausesValidation="False"  ToolTip="جستجو" 
                          onclick="imgbtnOrganizationPhysicalChartHighlightall_Click" />
       
        </td>
            </tr>
            <tr>
                <td colspan="3">
                    <telerik:RadTreeView  ID="radtreeOrganizationPhysicalChart" runat="server" BorderColor="#999999" 
                        BorderStyle="Solid" BorderWidth="1px" dir="rtl"  Width="99%" CausesValidation="False"  OnClientNodeClicking="onNodeClicking" OnNodeExpand="radtreeOrganizationPhysicalChart_NodeExpand">
                    </telerik:RadTreeView>
                </td>
            </tr>
        </table>
    </div>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
</telerik:RadScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="radtreeOrganizationPhysicalChart">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radtreeOrganizationPhysicalChart" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="imgbtnOrganizationPhysicalChartHighlightall">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radtreeOrganizationPhysicalChart" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="txtOrganizationPhysicalChartSearchTitle" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Runat="server" Skin="Default">
    </telerik:RadAjaxLoadingPanel>
    </form>
</body>
</html>


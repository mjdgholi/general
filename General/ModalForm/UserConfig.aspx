﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebFormMaster.Master" AutoEventWireup="true"
    CodeBehind="UserConfig.aspx.cs" Inherits="Intranet.DesktopModules.GeneralProject.General.ModalForm.UserConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpCenter" runat="server">
    <table bgcolor="#0575C7" border="0" cellpadding="3" cellspacing="3">
        <tr>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                رمز جدید:
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPassword"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                تکرار رمز:
            </td>
            <td>
                 <asp:TextBox runat="server" ID="txtPassConfirm"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                 &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="right">
                 <asp:Button ID="BtnAdd" runat="server" Text="ثبت" onclick="BtnAdd_Click" 
                     Width="60px" />
                 <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.Generalproject.General.ModalForm
{

    public partial class OrganizationPhysicalChartPage : PortalPage
    {
        #region PublicParam:
        private readonly OrganizationPhysicalChartBL _organizationPhysicalChartBL = new OrganizationPhysicalChartBL();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadTree((Guid?) null, null);
                //SetLoadRadTreeView();
            }

        }
        #region Procedur:
        /// <summary>
        /// لود کردن نمودار سازمانی
        /// </summary>
        void SetLoadRadTreeView()
        {
            radtreeOrganizationPhysicalChart.DataTextField = "OrganizationPhysicalChartTitle";
            radtreeOrganizationPhysicalChart.DataValueField = "OrganizationPhysicalChartId";
            radtreeOrganizationPhysicalChart.DataFieldID = "OrganizationPhysicalChartId";
            radtreeOrganizationPhysicalChart.DataFieldParentID = "OwnerId";
            radtreeOrganizationPhysicalChart.DataSource = _organizationPhysicalChartBL.GetAllIsActive();
            radtreeOrganizationPhysicalChart.DataBind();
        }


        private void LoadTree(Guid? OrganizationPhysicalChartId, RadTreeNodeEventArgs NodeClick)
        {
            OrganizationPhysicalChartEntity organizationPhysicalChartEntity = new OrganizationPhysicalChartEntity()
            {
                OrganizationPhysicalChartId =OrganizationPhysicalChartId
            };

            List<OrganizationPhysicalChartEntity> list = _organizationPhysicalChartBL.GetAllOnDemandOrganizationPhysicalChart(organizationPhysicalChartEntity);

            for (int i = 0; i < list.Count; i++)
            {
                RadTreeNode node = new RadTreeNode();
                node.Text = list[i].OrganizationPhysicalChartTitle;
                node.Value = list[i].OrganizationPhysicalChartId.ToString();
                if (Convert.ToInt32(list[i].ChildCount) > 0)
                {
                    node.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                }
                if (NodeClick == null)
                    radtreeOrganizationPhysicalChart.Nodes.Add(node);
                else
                {
                    NodeClick.Node.Nodes.Add(node);
                    NodeClick.Node.Expanded = true;
                    NodeClick.Node.ExpandMode = TreeNodeExpandMode.ClientSide;
                }
                 
            }
        }

        #endregion

        protected void radtreeOrganizationPhysicalChart_NodeExpand(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {
            LoadTree(Guid.Parse(e.Node.Value), e);
        }

        protected void imgbtnOrganizationPhysicalChartHighlightall_Click(object sender, ImageClickEventArgs e)
        {

            if (txtOrganizationPhysicalChartSearchTitle.Text != "")
            {
                radtreeOrganizationPhysicalChart.Nodes.Clear();
                LoadTree((Guid?)null, null);

                OrganizationPhysicalChartEntity organizationPhysicalChartEntity = new OrganizationPhysicalChartEntity()
                {
                    OrganizationPhysicalChartTitle = FarsiToArabic.ToArabic(txtOrganizationPhysicalChartSearchTitle.Text)
                };

                List<OrganizationPhysicalChartEntity> list = _organizationPhysicalChartBL.SearchOnDemandOrganizationPhysicalChart(organizationPhysicalChartEntity);

                for (int i = 0; i < list.Count; i++)
                {
                    List<RadTreeNode> foundNodes = null;

                    var radTreeNode=radtreeOrganizationPhysicalChart.FindNodeByValue(list[i].OrganizationPhysicalChartId.ToString());
                    radTreeNode.Expanded = true;
                    var radTreeNodeEventArgs = new RadTreeNodeEventArgs(radTreeNode);


                    if (radTreeNodeEventArgs.Node.ExpandMode != TreeNodeExpandMode.ClientSide)
                        radtreeOrganizationPhysicalChart_NodeExpand(radtreeOrganizationPhysicalChart, radTreeNodeEventArgs);
                    if (list[i].FlagSearch== 1)
                    {

                        radTreeNode.Text = radTreeNode.Text.Replace(ITC.Library.Classes.FarsiToArabic.ToArabic(txtOrganizationPhysicalChartSearchTitle.Text), string.Format("<font  color='blue'>{0}</font>", ITC.Library.Classes.FarsiToArabic.ToArabic(txtOrganizationPhysicalChartSearchTitle.Text)));
                        //Session["OrganizationPhysicalChartId"] = Session["OrganizationPhysicalChartId"] + ((Session["OrganizationPhysicalChartId"] == "") ? nodeobj.Value : "," + nodeobj.Value);
                    }


                    //foundNodes = LoadRadTreeView.FindNodeExpandByValue(RadtreeOrganizationPhysicalChart.Nodes, (dataSet.Tables[0].Rows[i][5].ToString()), node =>
                    //{
                    //    node.Expanded = true;
                    //});
                    //RadTreeNodeEventArgs radTreeNodeEventArgs = new RadTreeNodeEventArgs(foundNodes[0]);

                    //if (radTreeNodeEventArgs.Node.ExpandMode != TreeNodeExpandMode.ClientSide)
                    //    RadtreeOrganizationPhysicalChart_NodeExpand(RadtreeOrganizationPhysicalChart, radTreeNodeEventArgs);

                    //if (dataSet.Tables[0].Rows[i][3].ToString() == "1")
                    //{
                    //    var nodeobj = RadtreeOrganizationPhysicalChart.FindNodeByValue((dataSet.Tables[0].Rows[i][5]).ToString());
                    //    nodeobj.Text = nodeobj.Text.Replace(Variables.ConvertTo1256(txtOrganizationPhysicalChartSearchTitle.Text), string.Format("<font  color='blue'>{0}</font>", Variables.ConvertTo1256(txtOrganizationPhysicalChartSearchTitle.Text)));
                    //    Session["OrganizationPhysicalChartId"] = Session["OrganizationPhysicalChartId"] + ((Session["OrganizationPhysicalChartId"] == "") ? nodeobj.Value : "," + nodeobj.Value);
                    //}
                }
            }
            else
            {
                radtreeOrganizationPhysicalChart.Nodes.Clear();

                LoadTree((Guid?)null, null);
            }
        }





    }
}
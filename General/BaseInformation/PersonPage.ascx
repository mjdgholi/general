﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonPage.ascx.cs" Inherits="GeneralProject.General.BaseInformation.PersonPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<telerik:RadScriptBlock runat="server" ID="RadCodeBlock1">    
    <script type="text/javascript">
        function RowSelected(sender, eventArgs) {
            var oArg = new Object();
            oArg.Title = eventArgs._dataKeyValues.FirstName + ' ' + eventArgs._dataKeyValues.LastName;
            oArg.KeyId = eventArgs._dataKeyValues.PersonId;
            var oWnd = GetRadWindow();
            oWnd.close(oArg);
        }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function OnClientCloseHandler(oWindow) {
            document.form1.submit();
        }

    </script>
</telerik:RadScriptBlock>
<asp:Panel ID="Panel1" runat="server" style="width: 100%; " class="MainTableOfASCX" Width="100%">
    <table width="100%" dir="rtl">
        <tr>
            <td dir="rtl" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click">
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" align="right">
                <asp:Panel ID="pnlDetail" runat="server" Width="100%" >
                    <table width="100%">
                        <tr>
                            <td width="8%">
                                <asp:Label CssClass="LabelCSS" ID="lblFirstName" runat="server">نام<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="25%" >
                                <asp:TextBox ID="txtFirstName" runat="server" Width="100px" ></asp:TextBox>
                            </td>
                            <td width="2%">
                                <asp:RequiredFieldValidator ID="ValidPersonTitle" runat="server" 
                                    ControlToValidate="txtFirstName" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td width="8%">
                                <asp:Label ID="lblBirthDate" runat="server">تاریخ تولد<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td width="25%">
                                <cc1:CustomItcCalendar ID="TxtBirthDate" runat="server" Width="200px"></cc1:CustomItcCalendar>
                            </td>
                            <td width="2%">
                                <asp:RequiredFieldValidator ID="ValidBirthDate" runat="server" 
                                    ControlToValidate="TxtBirthDate" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblFirstName0" runat="server">نام خانوادگی فعلی<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                                <asp:TextBox ID="txtLastName" runat="server" ></asp:TextBox>
                            </td>
                            <td >
                                &nbsp;</td>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblDeathDate" runat="server">تاریخ فوت:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomItcCalendar ID="TxtDeathDate" runat="server" ></cc1:CustomItcCalendar>
                            </td>
                            <td >
      
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblFirstName1" runat="server">نام خانوادگی قبلی:</asp:Label>
                            </td>
                            <td >
                                <asp:TextBox ID="txtPreviousLastName" runat="server" ></asp:TextBox>
                            </td>
                            <td >
                                &nbsp;</td>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblCertificateNo" runat="server">شماره شناسنامه<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                                <asp:TextBox ID="txtCertificateNo" runat="server" ></asp:TextBox>
                            </td>
                            <td >
                                            <asp:RequiredFieldValidator ID="rfvCertificateNo" runat="server" 
                                    ControlToValidate="txtCertificateNo" ErrorMessage="*"></asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td >
                                <asp:CheckBox ID="ChkHasNationalNo" runat="server" AutoPostBack="True" 
                                    Checked="False" OnCheckedChanged="ChkHasNationalNo_CheckedChanged" 
                                    Text="آیا کد ملی دارد؟" />
                            </td>
                            <td >
                                <asp:TextBox ID="txtNationalNo" runat="server" DisplayText="کد ملی" 
                                    MaxLength="10" ></asp:TextBox>
                            </td>
                            <td >
                                &nbsp;</td>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="Label1" runat="server"> استان محل تولد:<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                               <table width="100%">
                                    <tr>
                                        <td align="right">
                                            <cc1:CustomRadComboBox ID="CmbBirthPlaceProvince" runat="server" AppendDataBoundItems="True"
                                                AutoPostBack="True" OnSelectedIndexChanged="CmbBirthPlaceProvince_SelectedIndexChanged" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True"
                                                 CausesValidation="False" Height="250">
                                            </cc1:CustomRadComboBox>
                                        </td>
                                        <td align="left">
                                            <asp:Label CssClass="LabelCSS" ID="lblBirthPlaceCity" runat="server">شهر<font 
                                    color="red">*</font>:</asp:Label>
                                        </td>
                                        <td align="right">
                                            <cc1:CustomRadComboBox ID="CmbBirthPlaceCity" runat="server"  AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" Height="250">
                                            </cc1:CustomRadComboBox>
                                        </td>
                                    </tr>
                                </table></td>
                            <td >
                                <asp:RequiredFieldValidator ID="ValidBirthPlaceProvince" runat="server" 
                                    ControlToValidate="CmbBirthPlaceCity" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblGender" runat="server">جنسیت<font 
                                    color="red">*</font>:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="CmbGender" runat="server" 
                                    AppendDataBoundItems="True" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="ValidGender" runat="server" 
                                    ControlToValidate="CmbGender" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblBirthPlaceSection" runat="server">بخش محل تولد:</asp:Label>
                            </td>
                            <td >
                                <asp:TextBox ID="txtBirthPlaceSection" runat="server" ></asp:TextBox>
                            </td>
                            <td >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblBloodGroup" runat="server">گروه خونی:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="CmbBloodGroup" runat="server" 
                                    AppendDataBoundItems="True"  AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
                                &nbsp;</td>
                            <td  nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblIssuePlaceProvince" runat="server"> استان محل صدور شناسنامه:</asp:Label>
                            </td>
                            <td >
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <cc1:CustomRadComboBox ID="CmbIssuePlaceProvince" runat="server" AppendDataBoundItems="True"
                                             Height="250"   AutoPostBack="True"  CausesValidation="False" OnSelectedIndexChanged="CmbIssuePlaceProvince_SelectedIndexChanged" AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                            </cc1:CustomRadComboBox>
                                        </td>
                                        <td align="left">
                                            <asp:Label CssClass="LabelCSS" ID="lblIssuePlaceCity" runat="server"> شهر:</asp:Label>
                                        </td>
                                        <td>
                                            <cc1:CustomRadComboBox ID="CmbIssuePlaceCity" runat="server"  AllowCustomText="True" Filter="Contains" MarkFirstMatch="True" Height="250">
                                            </cc1:CustomRadComboBox>
                                        </td>
                                    </tr>
                                </table></td>
                            <td >
                 
                            </td>
                        </tr>
                        <tr>
                                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblFirstName2" runat="server">نام پدر:</asp:Label>
                            </td>
                            <td >
                                <asp:TextBox ID="txtFatherName" runat="server" ></asp:TextBox>
                            </td>
      
                            <td >
                                &nbsp;</td>
                            <td  nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblIssuePlaceSection" runat="server"> بخش محل صدور شناسنامه:</asp:Label>
                            </td>
                            <td >
                                <asp:TextBox ID="txtIssuePlaceSection" runat="server"></asp:TextBox>
                            </td>
                            <td >
                                &nbsp;</td>
                        </tr>
                        <tr>
                                              <td >
                                <asp:Label CssClass="LabelCSS" ID="lblEnglishFirstName" runat="server">نام به انگلیسی</asp:Label>
                            </td>
                            <td >
                                <asp:TextBox ID="txtEnglishFirstName" runat="server" ></asp:TextBox>
                            </td>

                            <td >
                                &nbsp;</td>
                            <td nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblIssueDate" runat="server">تاریخ صدور شناسنامه:</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomItcCalendar ID="TxtIssueDate" runat="server" ></cc1:CustomItcCalendar>
                            </td>
                            <td >
                                &nbsp;</td>
                        </tr>
                        <tr>
                                        <td  nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblEnglishLastName" runat="server">نام خانوادگی انگلیسی:</asp:Label>
                            </td>
                            <td >
                                <asp:TextBox ID="txtEnglishLastName" runat="server" ></asp:TextBox>
                            </td>
                            <td >
                                &nbsp;</td>
                            <td  nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblCertificateAlphabet" runat="server">سری حرفی شناسنامه</asp:Label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="CmbCertificateAlphabet" runat="server" 
                                    AppendDataBoundItems="True"  AllowCustomText="True" Filter="Contains" MarkFirstMatch="True">
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
     
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <label>
                                وضیعت رکورد<font color="red">*</font>:</label>&nbsp;
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbIsActive" runat="server" Width="100px">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" Value="" />
                                        <telerik:RadComboBoxItem Text="فعال" Value="True" />
                                        <telerik:RadComboBoxItem Text="غیرفعال" Value="False" />
                                    </Items>
                                </cc1:CustomRadComboBox>
                            </td>
                            <td >
                                <asp:RequiredFieldValidator ID="rfvalidatorVisibility" runat="server" 
                                    ControlToValidate="cmbIsActive" ErrorMessage="وضیعت رکورد" InitialValue=" ">*</asp:RequiredFieldValidator>
                            </td>
                            <td  nowrap="nowrap">
                                <asp:Label CssClass="LabelCSS" ID="lblCertificateNumericSeries" runat="server">سریال عددی شناسنامه:</asp:Label>
                            </td>
                            <td >
                                <asp:TextBox ID="txtCertificateNumericSeries" runat="server" ></asp:TextBox>
                            </td>
                            <td >
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td >
                                &nbsp;</td>
                            <td >
                                &nbsp;</td>
                            <td >
                                &nbsp;</td>
                            <td >
                                <asp:Label CssClass="LabelCSS" ID="lblCertificateSerialNo" runat="server">سریال شناسنامه:</asp:Label>
                            </td>
                            <td >
                                <asp:TextBox ID="txtCertificateSerialNo" runat="server" ></asp:TextBox>
                            </td>
                            <td >
                                &nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdPerson" runat="server" AllowCustomPaging="True" AllowPaging="True"
                                    AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" OnItemCommand="grdPerson_ItemCommand" OnPageIndexChanged="grdPerson_PagePersonChanged"
                                    OnPageSizeChanged="grdPerson_PageSizeChanged" OnSortCommand="grdPerson_SortCommand">
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="PersonId" Dir="RTL" GroupsDefaultExpanded="False"
                                        ClientDataKeyNames="FirstName,LastName,PersonId,NationalNo" NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="FirstName" HeaderText="نام " SortExpression="FirstName">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LastName" HeaderText="نام خانوادگی" SortExpression="LastName">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="NationalNo" HeaderText="کد ملی" SortExpression="NationalNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="FatherName" HeaderText="نام پدر" SortExpression="FatherName">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="BirthDate" HeaderText="تاریخ تولد" SortExpression="BirthDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="BirthPlaceCityTitle" HeaderText="شهر محل تولد"
                                                SortExpression="BirthPlaceCityTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="وضیعت رکورد" SortExpression="IsActive">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridCheckBoxColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("PersonId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("PersonId").ToString() %>' CommandName="_MyِDelete"
                                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                        <ClientEvents OnRowSelected="RowSelected"></ClientEvents>
                                        <Selecting AllowRowSelect="True"></Selecting>
                                    </ClientSettings>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="ChkHasNationalNo">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="txtNationalNo" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="CmbBirthPlaceProvince">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CustomMessageErrorControl" />
                <telerik:AjaxUpdatedControl ControlID="CmbBirthPlaceCity" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="CmbIssuePlaceProvince">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="CustomMessageErrorControl" />
                <telerik:AjaxUpdatedControl ControlID="CmbIssuePlaceCity" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdPerson">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>

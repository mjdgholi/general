﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace GeneralProject.General.BaseInformation
{
    public partial class PersonPage : ItcBaseControl
    {
        //
        /// <summary>
        // Author:		<Narges Kamran>
        // Create date: <1393/07/06>
        // Description:	<فرم اشخاص>
        /// </summary>

        #region PublicParam:
        private readonly PersonBL _personBL = new PersonBL();
        private readonly GenderBL _genderBL= new GenderBL();
        private readonly BloodGroupBL _bloodGroupBL = new BloodGroupBL();
        private readonly CertificateAlphabetBL _certificateAlphabetBL = new CertificateAlphabetBL();
        private readonly ProvinceBL _provinceBL = new ProvinceBL();
        private readonly CityBL _cityBL = new CityBL();
        private readonly AccessSystemGeneralBL _accessSystemGeneralBl = new AccessSystemGeneralBL();

        private const string TableName = "General.v_Person";
        private const string PrimaryKey = "PersonId";

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>
        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                //var gridParamEntity = SetGridParam(grdPerson.MasterTableView.PageSize, 0, new Guid());
                //DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetCmbGender();
                SetCmbBloodGroup();
                SetCmbCertificateAlphabet();
                SetCmbBirthPlaceProvince();
                SetCmbIssuePlaceProvince();
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtFirstName.Text = "";
            CmbGender.ClearSelection();
            CmbBloodGroup.ClearSelection();
            
            txtNationalNo.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtPreviousLastName.Text = "";
            txtFatherName.Text = "";
            TxtBirthDate.Text = "";
            TxtDeathDate.Text = "";
            txtCertificateNo.Text = "";
            
            CmbBirthPlaceProvince.ClearSelection();            
            CmbBirthPlaceCity.Items.Clear();
            CmbBirthPlaceCity.ClearSelection();
            CmbBirthPlaceCity.Text = "";
            txtBirthPlaceSection.Text = "";
            
            CmbIssuePlaceProvince.ClearSelection();
            CmbIssuePlaceCity.Items.Clear();
            CmbIssuePlaceCity.ClearSelection();
            CmbIssuePlaceCity.Text = "";
            txtIssuePlaceSection.Text = "";
            TxtIssueDate.Text = "";
            CmbCertificateAlphabet.SelectedValue = "";
            txtCertificateNumericSeries.Text = "";
            txtCertificateSerialNo.Text = "";
            txtEnglishFirstName.Text = "";
            txtEnglishLastName.Text = "";
            cmbIsActive.SelectedValue = "";
            cmbIsActive.SelectedIndex = 1;

            ChkHasNationalNo.Checked = false;
            txtNationalNo.Enabled = false;
            txtNationalNo.BackColor = Color.Silver;
            txtNationalNo.Text = "";

            if (CmbGender.FindItemByValue("f342333f-7407-441c-8df0-69f9da65988b") != null)
            {
                CmbGender.SelectedValue = "f342333f-7407-441c-8df0-69f9da65988b";
            }

        }

        /// <summary>
        /// لود کردن اطلاعات جنسیت در لیست باز شونده
        /// </summary>
        private void SetCmbGender()
        {
            CmbGender.Items.Clear();
            CmbGender.Items.Add(new RadComboBoxItem(""));
            CmbGender.DataTextField = "GenderTitle";
            CmbGender.DataValueField = "GenderId";
            CmbGender.DataSource = _genderBL.GetAllIsActive();
            CmbGender.DataBind();
        }

        /// <summary>
        /// لود کردن اطلاعات گروه خونی در لیست باز شونده
        /// </summary>
        private void SetCmbBloodGroup()
        {
            CmbBloodGroup.Items.Clear();
            CmbBloodGroup.Items.Add(new RadComboBoxItem(""));
            CmbBloodGroup.DataTextField = "BloodGroupTitle";
            CmbBloodGroup.DataValueField = "BloodGroupId";
            CmbBloodGroup.DataSource = _bloodGroupBL.GetAllIsActive();
            CmbBloodGroup.DataBind();
        }

        /// <summary>
        /// لود کردن اطلاعات شناسه حرفی شناسنامه در لیست باز شونده
        /// </summary>
        private void SetCmbCertificateAlphabet()
        {
            CmbCertificateAlphabet.Items.Clear();
            CmbCertificateAlphabet.Items.Add(new RadComboBoxItem(""));
            CmbCertificateAlphabet.DataTextField = "CertificateAlphabetTitle";
            CmbCertificateAlphabet.DataValueField = "CertificateAlphabetId";
            CmbCertificateAlphabet.DataSource = _certificateAlphabetBL.GetAllIsActive();
            CmbCertificateAlphabet.DataBind();
        }

        /// <summary>
        /// لود کردن اطلاعات استان محل تولد در لیست باز شونده
        /// </summary>
        private void SetCmbBirthPlaceProvince()
        {
            CmbBirthPlaceProvince.Items.Clear();
            CmbBirthPlaceProvince.Items.Add(new RadComboBoxItem(""));
            CmbBirthPlaceProvince.DataTextField = "ProvinceTitlePersian";
            CmbBirthPlaceProvince.DataValueField = "ProvinceId";
            CmbBirthPlaceProvince.DataSource = _provinceBL.GetAllIsActive();
            CmbBirthPlaceProvince.DataBind();
        }

        /// <summary>
        /// لود کردن اطلاعات شهر بر اساس استان انتخابی محل تولد در لیست باز شونده
        /// </summary>
        private void SetCmbBirthPlaceCity(Guid provinceId)
        {
            var cityEntity =new CityEntity {ProvinceId = provinceId};
            CmbBirthPlaceCity.Items.Clear();
            CmbBirthPlaceCity.Items.Add(new RadComboBoxItem(""));
            CmbBirthPlaceCity.DataTextField = "CityTitlePersian";
            CmbBirthPlaceCity.DataValueField = "CityId";
            CmbBirthPlaceCity.DataSource = _cityBL.GetCityCollectionByProvince(cityEntity);
            CmbBirthPlaceCity.DataBind();
        }

        /// <summary>
        /// لود کردن اطلاعات شهر  بر اساس استان انتخابی محل صدور شناسنامه در لیست باز شونده
        /// </summary>
        private void SetCmbIssuePlaceCity(Guid provinceId)
        {
            var cityEntity = new CityEntity { ProvinceId = provinceId };
            CmbIssuePlaceCity.Items.Clear();
            CmbIssuePlaceCity.Items.Add(new RadComboBoxItem(""));
            CmbIssuePlaceCity.DataTextField = "CityTitlePersian";
            CmbIssuePlaceCity.DataValueField = "CityId";
            CmbIssuePlaceCity.DataSource = _cityBL.GetCityCollectionByProvince(cityEntity);
            CmbIssuePlaceCity.DataBind();
        }

        /// <summary>
        /// لود کردن اطلاعات استان محل صدور شناسنامه در لیست باز شونده
        /// </summary>
        private void SetCmbIssuePlaceProvince()
        {
            CmbIssuePlaceProvince.Items.Clear();
            CmbIssuePlaceProvince.Items.Add(new RadComboBoxItem(""));
            CmbIssuePlaceProvince.DataTextField = "ProvinceTitlePersian";
            CmbIssuePlaceProvince.DataValueField = "ProvinceId";
            CmbIssuePlaceProvince.DataSource = _provinceBL.GetAllIsActive();
            CmbIssuePlaceProvince.DataBind();
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var personEntity = new PersonEntity();
            {
                personEntity.PersonId = new Guid(ViewState["PersonId"].ToString());
            }
            var myPersonEntity = _personBL.GetSingleById(personEntity);                                          
            CmbGender.SelectedValue = myPersonEntity.GenderId.ToString();
            CmbBloodGroup.SelectedValue = myPersonEntity.BloodGroupId.ToString();
            ChkHasNationalNo.Checked = myPersonEntity.HasNationalNo;
            ChkHasNationalNo_CheckedChanged(ChkHasNationalNo,new EventArgs());
            txtNationalNo.Text = myPersonEntity.NationalNo;
            txtFirstName.Text = myPersonEntity.FirstName;
            txtLastName.Text = myPersonEntity.LastName;
            txtPreviousLastName.Text = myPersonEntity.PreviousLastName;
            txtFatherName.Text = myPersonEntity.FatherName;
            TxtBirthDate.Text = myPersonEntity.BirthDate;
            TxtDeathDate.Text = myPersonEntity.DeathDate;
            txtCertificateNo.Text = myPersonEntity.CertificateNo;

            if (myPersonEntity.BirthPlaceCityId !=null)
            {
                var birthPlaceCityEntity = new CityEntity {CityId = Guid.Parse(myPersonEntity.BirthPlaceCityId.ToString())};
            birthPlaceCityEntity = _cityBL.GetSingleById(birthPlaceCityEntity);
            var birthPlaceProvinceId = birthPlaceCityEntity.ProvinceId;
            CmbBirthPlaceProvince.SelectedValue = birthPlaceProvinceId.ToString();
            SetCmbBirthPlaceCity(birthPlaceProvinceId);
            CmbBirthPlaceCity.SelectedValue = myPersonEntity.BirthPlaceCityId.ToString();
            }
            

            txtBirthPlaceSection.Text = myPersonEntity.BirthPlaceSection;

            if (myPersonEntity.IssuePlaceCityId != null)
            {
                var issuePlaceCityEntity = new CityEntity {CityId = Guid.Parse(myPersonEntity.IssuePlaceCityId.ToString())};
                issuePlaceCityEntity = _cityBL.GetSingleById(issuePlaceCityEntity);
                var issuePlaceProvinceId = issuePlaceCityEntity.ProvinceId;
                CmbIssuePlaceProvince.SelectedValue = issuePlaceProvinceId.ToString();
                SetCmbIssuePlaceCity(issuePlaceProvinceId);
                CmbIssuePlaceCity.SelectedValue = myPersonEntity.IssuePlaceCityId.ToString();
            }
            txtIssuePlaceSection.Text = myPersonEntity.IssuePlaceSection;
            TxtIssueDate.Text = myPersonEntity.IssueDate;
            CmbCertificateAlphabet.SelectedValue = myPersonEntity.CertificateAlphabetId.ToString();
            txtCertificateNumericSeries.Text = myPersonEntity.CertificateNumericSeries;
            txtCertificateSerialNo.Text = myPersonEntity.CertificateSerialNo;
            txtEnglishFirstName.Text = myPersonEntity.EnglishFirstName;
            txtEnglishLastName.Text = myPersonEntity.EnglishLastName;
            cmbIsActive.SelectedValue = myPersonEntity.IsActive.ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }

        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdPerson,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            var userId = HttpContext.Current.User.Identity.Name;
            bool isPubPersonShowAll = (Request["SetFilterByModuleId"] == null || Convert.ToBoolean(Convert.ToInt16(Request["SetFilterByModuleId"])) == false);
            if (isPubPersonShowAll == false && _accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId)))
            // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
            {
                var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "")
                    ? " NationalNo='" + user.UserName + "'"
                    : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
            }

            return gridParamEntity;
        }

        #endregion

        #region ControlEvent:

        /// <summary>
        /// در زمانی که فرم به صورت مودال باز میشود به جای اینیت کنترل این متد اجرا میشود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try 
            {
                if (!IsPostBack)
                {
                      ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                //var gridParamEntity = SetGridParam(grdPerson.MasterTableView.PageSize, 0, new Guid());
                //DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                
                SetCmbGender();
                SetCmbBloodGroup();
                SetCmbCertificateAlphabet();
                SetCmbBirthPlaceProvince();
                SetCmbIssuePlaceProvince();
                SetClearToolBox();
                }
              
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// لود کردن شهر بر اساس استان انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>       
        protected void CmbBirthPlaceProvince_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(e.Value))
                {
                    CmbBirthPlaceCity.Text = "";
                    CmbBirthPlaceCity.Items.Clear();
                    var provinceId = new Guid(CmbBirthPlaceProvince.SelectedValue);
                    SetCmbBirthPlaceCity(provinceId);
                }
                else
                {
                    CmbBirthPlaceCity.Text = "";
                    CmbBirthPlaceCity.Items.Clear();
                }

 
            }
            catch (Exception exception)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(exception));
            }

        }

        /// <summary>
        /// لود کردن شهر صادر کننده شناسنامه بر اساس استان انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>       
        protected void CmbIssuePlaceProvince_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(e.Value))
                {
                    CmbIssuePlaceCity.Text = "";
                    CmbIssuePlaceCity.Items.Clear();
                    var provinceId = new Guid(CmbIssuePlaceProvince.SelectedValue);
                    SetCmbIssuePlaceCity(provinceId);
                }
                else
                {
                    CmbIssuePlaceCity.Text = "";
                    CmbIssuePlaceCity.Items.Clear();
                }

                
            }
            catch (Exception exception)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(exception));
            }
        }

        /// <summary>
        /// اگر کد ملی ندارد جعبه متن غیر فعال می گردد
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>       
        protected void ChkHasNationalNo_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkHasNationalNo.Checked)
            {
                txtNationalNo.Enabled = true;
                txtNationalNo.BackColor = Color.White;
            }
            else
            {
                txtNationalNo.Enabled = false;
                txtNationalNo.BackColor = Color.Silver;
                txtNationalNo.Text = "";
            }
        }


        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid personId;

                SetPanelFirst();

                var personEntity = new PersonEntity
                                       {
                                           GenderId = new Guid(CmbGender.SelectedValue),
                                           BloodGroupId = (CmbBloodGroup.SelectedIndex>0?new Guid(CmbBloodGroup.SelectedValue):(Guid?)null),
                                           HasNationalNo = ChkHasNationalNo.Checked,
                                           NationalNo = txtNationalNo.Text,
                                           FirstName = txtFirstName.Text,
                                           LastName = txtLastName.Text,
                                           PreviousLastName = txtPreviousLastName.Text,
                                           FatherName = txtFatherName.Text,
                                           BirthDate = ItcToDate.ShamsiToMiladi(TxtBirthDate.Text),
                                           DeathDate = ItcToDate.ShamsiToMiladi(TxtDeathDate.Text),
                                           CertificateNo = txtCertificateNo.Text,
                                           BirthPlaceCityId = new Guid(CmbBirthPlaceCity.SelectedValue),
                                           BirthPlaceSection = txtBirthPlaceSection.Text,
                                           IssuePlaceCityId = (CmbIssuePlaceCity.SelectedIndex > 0 ? new Guid(CmbIssuePlaceCity.SelectedValue) : (Guid?)null),
                                           IssuePlaceSection = txtIssuePlaceSection.Text,
                                           IssueDate = ItcToDate.ShamsiToMiladi(TxtIssueDate.Text),
                                           CertificateAlphabetId = (CmbCertificateAlphabet.SelectedIndex>0?new Guid(CmbCertificateAlphabet.SelectedValue):(Guid?)null),
                                           CertificateNumericSeries = txtCertificateNumericSeries.Text,
                                           CertificateSerialNo = txtCertificateSerialNo.Text,
                                           EnglishFirstName = txtEnglishFirstName.Text,
                                           EnglishLastName = txtEnglishLastName.Text,
                                           IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)
                                       };

                _personBL.Add(personEntity, out personId);
                ViewState["WhereClause"] = " personId = '" + personId + "'";
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdPerson.MasterTableView.PageSize, 0, personId);
                var userId = HttpContext.Current.User.Identity.Name;
                //if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId)))
                //    // کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                //{
                //    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                //    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "")
                //        ? " NationalNo='" + user.UserName + "'"
                //        : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                //}
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);               
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtCertificateNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CertificateNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtCertificateNo.Text.Trim()) + "%'";
                if (txtBirthPlaceSection.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and BirthPlaceSection Like N'%" +
                                               FarsiToArabic.ToArabic(txtBirthPlaceSection.Text.Trim()) + "%'";
                if (txtCertificateNumericSeries.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CertificateNumericSeries Like N'%" +
                                               FarsiToArabic.ToArabic(txtCertificateNumericSeries.Text.Trim()) + "%'";
                if (txtCertificateSerialNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CertificateSerialNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtCertificateSerialNo.Text.Trim()) + "%'";
                if (txtEnglishFirstName.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EnglishFirstName Like N'%" +
                                               FarsiToArabic.ToArabic(txtEnglishFirstName.Text.Trim()) + "%'";
                if (txtEnglishLastName.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EnglishLastName Like N'%" +
                                               FarsiToArabic.ToArabic(txtEnglishLastName.Text.Trim()) + "%'";
                if (txtFatherName.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FatherName Like N'%" +
                                               FarsiToArabic.ToArabic(txtFatherName.Text.Trim()) + "%'";

                if (txtFirstName.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FirstName Like N'%" +
                                               FarsiToArabic.ToArabic(txtFirstName.Text.Trim()) + "%'";
                if (txtIssuePlaceSection.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and IssuePlaceSection Like N'%" +
                                               FarsiToArabic.ToArabic(txtIssuePlaceSection.Text.Trim()) + "%'";
                if (txtLastName.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and LastName Like N'%" +
                                               FarsiToArabic.ToArabic(txtLastName.Text.Trim()) + "%'";
                if (txtNationalNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and NationalNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtNationalNo.Text.Trim()) + "%'";
                if (txtPreviousLastName.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PreviousLastName Like N'%" +
                                               FarsiToArabic.ToArabic(txtPreviousLastName.Text.Trim()) + "%'";
                if (TxtBirthDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and BirthDate Like N'%" +
                                               FarsiToArabic.ToArabic(TxtBirthDate.Text.Trim()) + "%'";
                if (TxtDeathDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and DeathDate Like N'%" +
                                               FarsiToArabic.ToArabic(TxtDeathDate.Text.Trim()) + "%'";
                if (TxtIssueDate.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and IssueDate Like N'%" +
                                               FarsiToArabic.ToArabic(TxtIssueDate.Text.Trim()) + "%'";                

                if (cmbIsActive.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                if (CmbBirthPlaceProvince.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And BirthProvinceId='" +
                                               (CmbBirthPlaceProvince.SelectedItem.Value.Trim()) + "'";
                if (CmbBirthPlaceCity.SelectedIndex > 0) 
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And BirthPlaceCityId='" +
                                               (CmbBirthPlaceCity.SelectedItem.Value.Trim()) + "'";
                if (CmbIssuePlaceProvince.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IssueProvinceId='" +
                                               (CmbIssuePlaceProvince.SelectedItem.Value.Trim()) + "'";
                if (CmbBloodGroup.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And BloodGroupId='" +
                                               (CmbBloodGroup.SelectedItem.Value.Trim()) + "'";
                if (CmbCertificateAlphabet.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And CertificateAlphabetId='" +
                                               (CmbCertificateAlphabet.SelectedItem.Value.Trim()) + "'";
                if (CmbGender.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And GenderId='" +
                                               (CmbGender.SelectedItem.Value.Trim()) + "'";
                if (CmbIssuePlaceCity.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IssuePlaceCityId='" +
                                               (CmbIssuePlaceCity.SelectedItem.Value.Trim()) + "'";
               


                grdPerson.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;               
                var gridParamEntity = SetGridParam(grdPerson.MasterTableView.PageSize, 0, new Guid());                
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);  
                if (grdPerson.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdPerson.MasterTableView.PageSize, 0, new Guid());
                var userId = HttpContext.Current.User.Identity.Name;
                //if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId)))
                //// کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                //{
                //    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                //    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "")
                //        ? " NationalNo='" + user.UserName + "'"
                //        : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                //}
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);  
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var PersonEntity = new PersonEntity
                                       {
                                           PersonId = new Guid(ViewState["PersonId"].ToString()),
                                           GenderId = new Guid(CmbGender.SelectedValue),
                                           BloodGroupId = (CmbBloodGroup.SelectedIndex > 0 ? new Guid(CmbBloodGroup.SelectedValue) : (Guid?)null),
                                           HasNationalNo = ChkHasNationalNo.Checked,
                                           NationalNo = txtNationalNo.Text,
                                           FirstName = txtFirstName.Text,
                                           LastName = txtLastName.Text,
                                           PreviousLastName = txtPreviousLastName.Text,
                                           FatherName = txtFatherName.Text,
                                           BirthDate = ItcToDate.ShamsiToMiladi(TxtBirthDate.Text),
                                           DeathDate = ItcToDate.ShamsiToMiladi(TxtDeathDate.Text),
                                           CertificateNo = txtCertificateNo.Text,
                                           BirthPlaceCityId = new Guid(CmbBirthPlaceCity.SelectedValue),
                                           BirthPlaceSection = txtBirthPlaceSection.Text,
                                           IssuePlaceCityId = (CmbIssuePlaceCity.SelectedIndex>0?new Guid(CmbIssuePlaceCity.SelectedValue):(Guid?)null),
                                           IssuePlaceSection = txtIssuePlaceSection.Text,
                                           IssueDate = ItcToDate.ShamsiToMiladi(TxtIssueDate.Text),
                                           CertificateAlphabetId = (CmbCertificateAlphabet.SelectedIndex > 0 ? new Guid(CmbCertificateAlphabet.SelectedValue) : (Guid?)null),
                                           CertificateNumericSeries = txtCertificateNumericSeries.Text,
                                           CertificateSerialNo = txtCertificateSerialNo.Text,
                                           EnglishFirstName = txtEnglishFirstName.Text,
                                           EnglishLastName = txtEnglishLastName.Text,
                                           IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)
                                       };
                _personBL.Update(PersonEntity);
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = " personId = '" + new Guid(ViewState["PersonId"].ToString())+"'";
                var gridParamEntity = SetGridParam(grdPerson.MasterTableView.PageSize, 0, new Guid(ViewState["PersonId"].ToString()));
                var userId = HttpContext.Current.User.Identity.Name;
                //if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId)))
                //// کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                //{
                //    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                //    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "")
                //        ? " NationalNo='" + user.UserName + "'"
                //        : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                //}
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);  
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdPerson_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["PersonId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    

                    var personEntity = new PersonEntity();
                    {
                        personEntity.PersonId = new Guid(e.CommandArgument.ToString());
                    }
                    _personBL.Delete(personEntity);
                    var gridParamEntity = SetGridParam(grdPerson.MasterTableView.PageSize, 0, new Guid());
                    //var userId = HttpContext.Current.User.Identity.Name;
                    //if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId)))
                    //// کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                    //{
                    //    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                    //    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "")
                    //        ? " NationalNo='" + user.UserName + "'"
                    //        : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                    //}
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);  

                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdPerson_PagePersonChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdPerson.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                //var userId = HttpContext.Current.User.Identity.Name;
                //if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId)))
                //// کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                //{
                //    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                //    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "")
                //        ? " NationalNo='" + user.UserName + "'"
                //        : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                //}
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);  
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdPerson_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdPerson.MasterTableView.PageSize, 0, new Guid());
                //var userId = HttpContext.Current.User.Identity.Name;
                //if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId)))
                //// کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                //{
                //    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                //    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "")
                //        ? " NationalNo='" + user.UserName + "'"
                //        : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                //}
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);  
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdPerson_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdPerson.MasterTableView.PageSize, 0, new Guid());
                var userId = HttpContext.Current.User.Identity.Name;
                //if (_accessSystemGeneralBl.IsOnlyOwnInfoUser(Int32.Parse(userId)))
                //// کاربر فقط اجازه ویرایش اطلاعات خود را دارد
                //{
                //    var user = Intranet.Security.UserDB.GetSingleUser(Int32.Parse(userId));
                //    gridParamEntity.WhereClause = (gridParamEntity.WhereClause.Trim() == "")
                //        ? " NationalNo='" + user.UserName + "'"
                //        : ViewState["WhereClause"] + " and  NationalNo='" + user.UserName + "'";
                //}
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);  

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion

       
        
        

    }
    }

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GeneralProject.General.BL;
using GeneralProject.General.Entity;
using ITC.Library.Classes;
using ITC.Library.Controls;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.GeneralProject.General.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/04>
    /// Description: <دسترسی به آیکن منو>
    /// </summary>
    public partial class ModuleArrangementAccessPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly ModuleArrangementAccessBL _moduleArrangementAccessBL = new ModuleArrangementAccessBL();
        private readonly UserBL _userBL = new UserBL();
        private readonly ModuleArrangementBL _moduleArrangementBL = new ModuleArrangementBL();
        private const string TableName = "General.V_ModuleArrangementAccess";
        private const string PrimaryKey = "ModuleArrangementAccessId";
        #endregion


        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            
            cmbModuleArrangement.ClearSelection();
            cmbModuleArrangementIcon.ClearSelection();
            cmbUser.ClearSelection();

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var moduleArrangementAccessEntity = new ModuleArrangementAccessEntity()
            {
                ModuleArrangementAccessId = int.Parse(ViewState["ModuleArrangementAccessId"].ToString())
            };
            var mymoduleArrangementAccess = _moduleArrangementAccessBL.GetSingleById(moduleArrangementAccessEntity);
            cmbModuleArrangement.SelectedValue = mymoduleArrangementAccess.ModuleArrangementOwnerId.ToString();
            cmbUser.SelectedValue = mymoduleArrangementAccess.UserId.ToString();
            setModuleArrangment(mymoduleArrangementAccess.ModuleArrangementOwnerId);
            cmbModuleArrangementIcon.SelectedValue = mymoduleArrangementAccess.ModuleArrangementId.ToString();

        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grModuleArrangementAccess,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbUser.Items.Clear();
            cmbUser.Items.Add(new RadComboBoxItem(""));
            cmbUser.DataSource = _userBL.GetAllIsActive();
            cmbUser.DataTextField = "FullName";
            cmbUser.DataValueField = "UserID";
            cmbUser.DataBind();

            cmbModuleArrangement.Items.Clear();
            cmbModuleArrangement.Items.Add(new RadComboBoxItem(""));
            ModuleArrangementEntity moduleArrangementEntity = new ModuleArrangementEntity()
            {
                OwnerId = (int?)null
            };
            cmbModuleArrangement.DataSource = _moduleArrangementBL.GetSingleByOwnerId(moduleArrangementEntity);
            cmbModuleArrangement.DataTextField = "ModuleToolTip";
            cmbModuleArrangement.DataValueField = "ModuleArrangementId";
            cmbModuleArrangement.DataBind();


        }

      void  setModuleArrangment(int OwnerId )
        {
            cmbModuleArrangementIcon.Items.Clear();
            cmbModuleArrangementIcon.Items.Add(new RadComboBoxItem(""));
            ModuleArrangementEntity moduleArrangementEntity=new ModuleArrangementEntity()
            {
                OwnerId = OwnerId
            };
            cmbModuleArrangementIcon.DataSource = _moduleArrangementBL.GetSingleByOwnerId(moduleArrangementEntity);
            cmbModuleArrangementIcon.DataTextField = "ModuleToolTip";
            cmbModuleArrangementIcon.DataValueField = "ModuleArrangementId";
            cmbModuleArrangementIcon.DataBind();

        }

        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grModuleArrangementAccess.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = "  ";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grModuleArrangementAccess.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    SetComboBox();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }
        }
 

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int moduleArrangementAccessId;
                ViewState["WhereClause"] = "";
                SetPanelFirst();

                var moduleArrangementAccessEntity = new ModuleArrangementAccessEntity()
                {
                    ModuleArrangementId = int.Parse(cmbModuleArrangementIcon.SelectedValue),
                    UserId = int.Parse(cmbUser.SelectedValue),
                    

                };

                _moduleArrangementAccessBL.Add(moduleArrangementAccessEntity, out moduleArrangementAccessId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grModuleArrangementAccess.MasterTableView.PageSize, 0, moduleArrangementAccessId);
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (cmbUser.SelectedIndex>0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and UserId = '" +
                                              int.Parse(cmbUser.SelectedValue.Trim()) + "'";
                if (cmbModuleArrangementIcon.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And ModuleArrangementId='" +
                                               (cmbModuleArrangementIcon.SelectedValue.Trim()) + "'";
                if (cmbModuleArrangement.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And ModuleArrangementOwnerId='" +
                                               (cmbModuleArrangement.SelectedValue.Trim()) + "'";
                grModuleArrangementAccess.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grModuleArrangementAccess.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                if (grModuleArrangementAccess.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grModuleArrangementAccess.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var moduleArrangementAccessEntity = new ModuleArrangementAccessEntity()
                {
                    ModuleArrangementAccessId = int.Parse(ViewState["ModuleArrangementAccessId"].ToString()),
                    ModuleArrangementId = int.Parse(cmbModuleArrangementIcon.SelectedValue),
                    UserId = int.Parse(cmbUser.SelectedValue),


                };
                _moduleArrangementAccessBL.Update(moduleArrangementAccessEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grModuleArrangementAccess.MasterTableView.PageSize, 0, int.Parse(ViewState["ModuleArrangementAccessId"].ToString()));
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grModuleArrangementAccess_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["ModuleArrangementAccessId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";

                    var moduleArrangementAccessId = new ModuleArrangementAccessEntity()
                    {
                        ModuleArrangementAccessId = int.Parse(e.CommandArgument.ToString()),

                    };
                    _moduleArrangementAccessBL.Delete(moduleArrangementAccessId);
                    var gridParamEntity = SetGridParam(grModuleArrangementAccess.MasterTableView.PageSize, 0,-1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grModuleArrangementAccess_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grModuleArrangementAccess.MasterTableView.PageSize, e.NewPageIndex, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grModuleArrangementAccess_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grModuleArrangementAccess.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grModuleArrangementAccess_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grModuleArrangementAccess.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion

        protected void cmbModuleArrangement_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.Value))
            {
                setModuleArrangment(int.Parse(e.Value));
            }
            else
            {
                cmbModuleArrangementIcon.Items.Clear();
                cmbModuleArrangementIcon.Text = "";
            }
        }



    }
}
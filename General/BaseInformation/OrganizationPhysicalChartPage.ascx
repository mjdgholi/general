﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrganizationPhysicalChartPage.ascx.cs" Inherits="Intranet.DesktopModules.Generalproject.General.BaseInformation.OrganizationPhysicalChartPage" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
<script type="text/javascript" >
    function NodeClicking(sender, eventArgs) {
        var node = eventArgs.get_node();
        var isSelected = node.get_selected();

        if (isSelected) {
            node.set_selected(false);
        }
        else {
            node.set_selected(true);
        }

        eventArgs.set_cancel(true);
    }

</script>

</telerik:RadScriptBlock>
<asp:Panel ID="pnlGeneral" runat="server">
    <table style="width: 100%; " class="MainTableOfASCX"  dir="rtl">

        <tr>
            <td dir="rtl" style="width: 100%; " valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td align="right">
                                <cc1:CustomRadButton ID="btnSave" runat="server" OnClick="btnSave_Click">
                                </cc1:CustomRadButton>
                            </td>
                            <td align="right">
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click">
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                                                        <td align="right">
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" onclick="btnBack_Click" >
                              
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                              
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                   
                                  <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                   
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Panel ID="pnlDetail" runat="server">
                    <table width="100%">
                        
                                  <tr>
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblOrganizationPhysicalChartTitle">عنوان واحد سازمانی<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtOrganizationPhysicalChartTitle" runat="server" 
                                    ></telerik:RadTextBox>
                                <asp:RequiredFieldValidator ID="rfvOrganizationPhysicalChartTitle" runat="server" ControlToValidate="txtOrganizationPhysicalChartTitle"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>

                            </td>
                        </tr>
                                 
                                                <tr>
                     <td width="15%">
                                <asp:Label CssClass="LabelCSS" ID="lblIsActive" runat="server">وضیعت رکورد<font color="red">*</font>:</asp:Label>
                            </td>
                            <td width="35%" >
                                <cc1:CustomRadComboBox ID="cmbIsActive" runat="server">
                                    <Items>
                                        <telerik:RadComboBoxItem Text="فعال" Value="True" />
                                        <telerik:RadComboBoxItem Text="غیرفعال" Value="False" />
                                    </Items>
                                </cc1:CustomRadComboBox>
                            </td>

                        </tr>

     
                        <tr>
                            <td width="10%" nowrap="nowrap" valign="top" bgcolor="White">
                                <asp:Label CssClass="LabelCSS" ID="lblMenu" runat="server" Text="منو دربرگیرنده:"></asp:Label>
                            </td>
                            <td bgcolor="White">
                                <telerik:RadTreeView ID="radtreOrganizationPhysicalChart" runat="server" 
                                    BorderColor="#999999" BorderStyle="Solid"
                                    BorderWidth="1px" dir="rtl"  Width="99%" CausesValidation="False"
                                                OnClientNodeClicking="NodeClicking" 
                                    oncontextmenuitemclick="radtreOrganizationPhysicalChart_ContextMenuItemClick">
                                    <ContextMenus>
                                        <telerik:RadTreeViewContextMenu ID="MainContextMenu" runat="server" >
                                            <Items>
                                                <telerik:RadMenuItem Value="edit" Text="ویرایش" ImageUrl="../Images/Edit.png" PostBack="True">
                                                </telerik:RadMenuItem>
                                                <telerik:RadMenuItem Value="remove" Text="حذف" ImageUrl="../Images/Delete.png" OnClientClick="if (!confirm('آیا اطلاعات رکورد حذف شود؟?')) return false;">
                                                </telerik:RadMenuItem>
                                            </Items>
                                            <CollapseAnimation Type="OutQuint"></CollapseAnimation>
                                        </telerik:RadTreeViewContextMenu>
                                    </ContextMenus>
                                </telerik:RadTreeView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>

    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy runat="server">
    <ajaxsettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="radtreOrganizationPhysicalChart">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </updatedcontrols>
        </telerik:AjaxSetting>
    </ajaxsettings>
</telerik:RadAjaxManagerProxy>


<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Runat="server" 
    >
</telerik:RadAjaxLoadingPanel>




﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using ITC.Library.Controls;


namespace Intranet.DesktopModules.GeneralProject.General.BaseInformation
{
    public partial class FaithPage : ItcBaseControl
    {
        /// <summary>
        /// Author:		 <Narges.Kamran>
        /// Create date: <1393/07/07>
        /// Description: <دین>
        /// </summary>
        /// 
        /// 
        #region PublicParam:
        private readonly FaithBL _faithBL = new FaithBL();

        private const string TableName = "General.t_Faith";
        private const string PrimaryKey = "FaithId";

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>
        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdAssesmentFaith.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;            
            Panel1.DefaultButton = "btnSave";           
            btnSave.Focus();
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
            Panel1.DefaultButton = "btnEdit";
           btnEdit.Focus();
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtFaithTitle.Text = "";
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var faithEntity = new FaithEntity();
            {
                faithEntity.FaithId = new Guid(ViewState["FaithId"].ToString());
            }
            var myFaithBL = _faithBL.GetSingleById(faithEntity);
            txtFaithTitle.Text = myFaithBL.FaithTitle.ToString();
            cmbIsActive.SelectedValue = myFaithBL.IsActive.ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
                                      {
                                          TableName = TableName,
                                          PrimaryKey = PrimaryKey,
                                          RadGrid = grdAssesmentFaith,
                                          PageSize = pageSize,
                                          CurrentPage = currentpPage,
                                          WhereClause = ViewState["WhereClause"].ToString(),
                                          OrderBy = ViewState["SortExpression"].ToString(),
                                          SortType = ViewState["SortType"].ToString(),
                                          RowSelectGuidId = rowSelectGuidId
                                      };
            return gridParamEntity;
        }

        #endregion

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    try
            //    {
            //        ViewState["WhereClause"] = "  ";
            //        ViewState["SortExpression"] = " ";
            //        ViewState["SortType"] = "Asc";
            //        var gridParamEntity = SetGridParam(grdAssesmentFaith.MasterTableView.PageSize, 0, new Guid());
            //        DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            //        SetPanelFirst();
            //        SetClearToolBox();
            //    }
            //    catch (Exception ex)
            //    {

            //        CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            //    }
            //}
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid faithId;
                ViewState["WhereClause"] = "";
                

                var faithEntity = new FaithEntity
                                      {
                                          FaithTitle = FarsiToArabic.ToArabic(txtFaithTitle.Text.Trim()),
                                          IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)
                                      };

                _faithBL.Add(faithEntity, out faithId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdAssesmentFaith.MasterTableView.PageSize, 0, faithId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtFaithTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FaithTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtFaithTitle.Text.Trim()) + "%'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdAssesmentFaith.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;                
                var gridParamEntity = SetGridParam(grdAssesmentFaith.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdAssesmentFaith.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdAssesmentFaith.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var FaithEntity = new FaithEntity
                                      {
                                          FaithId = new Guid(ViewState["FaithId"].ToString()),
                                          FaithTitle = FarsiToArabic.ToArabic(txtFaithTitle.Text.Trim()),
                                          IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)
                                      };
                _faithBL.Update(FaithEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdAssesmentFaith.MasterTableView.PageSize, 0, new Guid(ViewState["FaithId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdAssesmentFaith_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["FaithId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";

                    var FaithEntity = new FaithEntity();
                    {
                        FaithEntity.FaithId = new Guid(e.CommandArgument.ToString());
                    }
                    _faithBL.Delete(FaithEntity);
                    var gridParamEntity = SetGridParam(grdAssesmentFaith.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdAssesmentFaith_PageFaithChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
                try
                {
                    var gridParamEntity = SetGridParam(grdAssesmentFaith.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }

            /// <summary>
            /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdAssesmentFaith_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdAssesmentFaith.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdAssesmentFaith_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
                try
                {
                    if (ViewState["SortExpression"].ToString() == e.SortExpression)
                    {
                        ViewState["SortExpression"] = e.SortExpression;
                        SetSortType();
                    }
                    else
                    {
                        ViewState["SortType"] = "ASC";
                        ViewState["SortExpression"] = e.SortExpression;
                    }
                    var gridParamEntity = SetGridParam(grdAssesmentFaith.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }

            }

            #endregion

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        throw (new ItcApplicationErrorManagerException("TextError"));  
        //        ITC.Library.Classes.ItcToDate.ShamsiToMiladi((CustomeRadMaskedTextBox1.AddDateSlash8Digit(CustomeRadMaskedTextBox1.Text)));
        //    }
        //    catch (Exception exception)
        //    {               

        //   CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(exception));
        //    }
        //}


    }
} 
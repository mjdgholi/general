﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.GeneralProject.General.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/06>
    /// Description: <استان>
    /// </summary>
    public partial class ProvincePage : ItcBaseControl
    {
        #region PublicParam:
        private readonly ProvinceBL _provinceBL = new ProvinceBL();
        private readonly CountryBL _countryBL = new CountryBL();
        private const string TableName = "General.V_Province";
        private const string PrimaryKey = "ProvinceId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtProvinceTitleEnglish.Text = "";
            txtProvinceTitlePersian.Text = "";
            cmbCountry.ClearSelection();
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var provinceEntity = new ProvinceEntity();
            {
                provinceEntity.ProvinceId=new Guid(ViewState["ProvinceId"].ToString());
            }
            var myProvince = _provinceBL.GetSingleById(provinceEntity);
            txtProvinceTitleEnglish.Text = myProvince.ProvinceTitleEnglish;
            txtProvinceTitlePersian.Text = myProvince.ProvinceTitlePersian;
            cmbIsActive.SelectedValue = myProvince.IsActive.ToString();
            cmbCountry.SelectedValue = myProvince.CountryId.ToString();

            if (cmbCountry.FindItemByValue(myProvince.CountryId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد کشور انتخاب شده معتبر نمی باشد.");                
            }
            
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdProvince,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbCountry.Items.Clear();
            cmbCountry.Items.Add(new RadComboBoxItem(""));
            cmbCountry.DataSource = _countryBL.GetAllIsActive();
            cmbCountry.DataTextField = "CountryTitlePersian";
            cmbCountry.DataValueField = "CountryId";
            cmbCountry.DataBind();



        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdProvince.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid ProvinceId;
                ViewState["WhereClause"] = "";
                SetPanelFirst();

                var provinceEntity = new ProvinceEntity()
                                         {
                                             ProvinceTitleEnglish = txtProvinceTitleEnglish.Text,
                                             ProvinceTitlePersian = txtProvinceTitlePersian.Text,
                                             CountryId = new Guid(cmbCountry.SelectedValue.ToString()),
                                             IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)

                                         };

                _provinceBL.Add(provinceEntity, out ProvinceId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdProvince.MasterTableView.PageSize, 0, ProvinceId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtProvinceTitleEnglish.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ProvinceTitleEnglish Like N'%" +
                                               FarsiToArabic.ToArabic(txtProvinceTitleEnglish.Text.Trim()) + "%'";
                if (txtProvinceTitlePersian.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ProvinceTitlePersian Like N'%" +
                                               FarsiToArabic.ToArabic(txtProvinceTitlePersian.Text.Trim()) + "%'";
                if(cmbCountry.SelectedIndex>0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CountryId ='" +
                                               new Guid(cmbCountry.SelectedValue) + "'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdProvince.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;                
                var gridParamEntity = SetGridParam(grdProvince.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdProvince.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdProvince.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                var provinceEntity = new ProvinceEntity()
                {
                    ProvinceId = new Guid(ViewState["ProvinceId"].ToString()),
                    ProvinceTitleEnglish = txtProvinceTitleEnglish.Text,
                    ProvinceTitlePersian = txtProvinceTitlePersian.Text,
                    CountryId = new Guid(cmbCountry.SelectedValue.ToString()),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)

                };
                _provinceBL.Update(provinceEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdProvince.MasterTableView.PageSize, 0, new Guid(ViewState["ProvinceId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdProvince_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
                try
                {
                    if (e.CommandName == "_MyِEdit")
                    {
                        ViewState["ProvinceId"] = e.CommandArgument;
                        e.Item.Selected = true;
                        SetDataShow();
                        SetPanelLast();
                    }
                    if (e.CommandName == "_MyِDelete")
                    {
                        ViewState["WhereClause"] = "";

                        var provinceEntity = new ProvinceEntity()
                        {
                            ProvinceId = new Guid(e.CommandArgument.ToString()),               

                        };
                        _provinceBL.Delete(provinceEntity);
                        var gridParamEntity = SetGridParam(grdProvince.MasterTableView.PageSize, 0, new Guid());
                        DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

                        SetPanelFirst();
                        SetClearToolBox();
                        CustomMessageErrorControl.ShowSuccesMessage(
                            ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                    }
                }
                catch (Exception ex)
                {
                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

                }
            }

            /// <summary>
            /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdProvince_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
                try
                {
                    var gridParamEntity = SetGridParam(grdProvince.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }

            /// <summary>
            /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e">تعداد رکوردهای هر صفحه</param>


        protected void grdProvince_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdProvince.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdProvince_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdProvince.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion

    }
}
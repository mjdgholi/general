﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.GeneralProject.General.BaseInformation
{

         /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/06>
    /// Description: <شهر>
    /// </summary>
    public partial class CityPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly CityBL _cityBL = new CityBL();
        private readonly ProvinceBL _provinceBL = new ProvinceBL();
        private readonly GeographicScopeBL _geographicScopeBL = new GeographicScopeBL();
        private const string TableName = "General.V_City";
        private const string PrimaryKey = "CityId";

        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtCityTitleEnglish.Text = "";
            txtCityTitlePersian.Text = "";
            cmbGeographicScope.ClearSelection();
            cmbProvince.ClearSelection();
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var cityEntity = new CityEntity()
                                 {
                                     CityId = new Guid(ViewState["CityId"].ToString())
                                 };
            var myCity = _cityBL.GetSingleById(cityEntity);
            txtCityTitleEnglish.Text = myCity.CityTitleEnglish;
            txtCityTitlePersian.Text = myCity.CityTitlePersian;
            cmbIsActive.SelectedValue = myCity.IsActive.ToString();            
            
            cmbProvince.SelectedValue = myCity.ProvinceId.ToString();
            if (cmbProvince.FindItemByValue(myCity.ProvinceId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد استان انتخاب شده معتبر نمی باشد.");
            }
            cmbGeographicScope.SelectedValue = myCity.GeographicScopeId.ToString();
            if (cmbGeographicScope.FindItemByValue(myCity.GeographicScopeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد حوزه جغرافیایی انتخاب شده معتبر نمی باشد.");   
            }

                      
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdCity,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetProvinceComboBox()
        {
            cmbProvince.Items.Clear();
            cmbProvince.Items.Add(new RadComboBoxItem(""));
            cmbProvince.DataSource = _provinceBL.GetAllIsActive();
            cmbProvince.DataTextField = "ProvinceTitlePersian";
            cmbProvince.DataValueField = "ProvinceId";
            cmbProvince.DataBind();
        }

        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetGeographicScopeComboBox()
        {

            cmbGeographicScope.Items.Clear();
            cmbGeographicScope.Items.Add(new RadComboBoxItem(""));
            cmbGeographicScope.DataSource = _geographicScopeBL.GetAllIsActive();
            cmbGeographicScope.DataTextField = "GeographicScopePersianTitle";
            cmbGeographicScope.DataValueField = "GeographicScopeId";
            cmbGeographicScope.DataBind();
        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdCity.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetGeographicScopeComboBox();
                SetProvinceComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid cityId;
                ViewState["WhereClause"] = "";
                SetPanelFirst();

                var cityEntity = new CityEntity()
                {
                    CityTitleEnglish =txtCityTitleEnglish.Text,
                    CityTitlePersian = txtCityTitlePersian.Text,
                    GeographicScopeId = new Guid(cmbGeographicScope.SelectedValue),
                    ProvinceId = new Guid(cmbProvince.SelectedValue),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)

                };

                _cityBL.Add(cityEntity, out cityId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdCity.MasterTableView.PageSize, 0, cityId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtCityTitleEnglish.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CityTitleEnglish Like N'%" +
                                               FarsiToArabic.ToArabic(txtCityTitleEnglish.Text.Trim()) + "%'";
                if (txtCityTitlePersian.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CityTitlePersian Like N'%" +
                                               FarsiToArabic.ToArabic(txtCityTitlePersian.Text.Trim()) + "%'";
                if (cmbGeographicScope.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and GeographicScopeId ='" +
                                               new Guid(cmbGeographicScope.SelectedValue) + "'";
                if (cmbProvince.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ProvinceId ='" +
                                               new Guid(cmbProvince.SelectedValue) + "'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdCity.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;                
                var gridParamEntity = SetGridParam(grdCity.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdCity.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdCity.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                var cityEntity = new CityEntity()
                {
                    CityId = new Guid(ViewState["CityId"].ToString()),
                    CityTitleEnglish = txtCityTitleEnglish.Text,
                    CityTitlePersian = txtCityTitlePersian.Text,
                    GeographicScopeId = new Guid(cmbGeographicScope.SelectedValue),
                    ProvinceId = new Guid(cmbProvince.SelectedValue),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)

                };
                _cityBL.Update(cityEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdCity.MasterTableView.PageSize, 0, new Guid(ViewState["CityId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {

            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdCity_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["CityId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";

                    var cityEntity = new CityEntity()
                    {
                        CityId = new Guid(e.CommandArgument.ToString()),

                    };
                    _cityBL.Delete(cityEntity);
                    var gridParamEntity = SetGridParam(grdCity.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdCity_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdCity.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>


        protected void grdCity_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdCity.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdCity_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdCity.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #endregion
    }
}
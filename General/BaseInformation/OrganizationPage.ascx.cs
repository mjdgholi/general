﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;

using Telerik.Web.UI;

namespace Intranet.DesktopModules.GeneralProject.General.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/04>
    /// Description: <سازمان>
    /// </summary>

    public partial class OrganizationPage : ItcBaseControl
    {
        #region PublicParam:

        private readonly OrganizationBL _organizationBL = new OrganizationBL();
        private readonly ProvinceBL _provinceBL = new ProvinceBL();
        private readonly CityBL _cityBL = new CityBL();
        private const string TableName = "General.V_Organization";
        private const string PrimaryKey = "OrganizationId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtOrganizationAddress.Text = "";
            txtOrganizationDescription.Text = "";
            txtOrganizationEconomicCode.Text = "";
            txtOrganizationEmail.Text = "";
            txtOrganizationEnglishTitle.Text = "";
            txtOrganizationFoxNo.Text = "";
            txtOrganizationPersianTitle.Text = "";
            txtOrganizationTelephoneNo.Text = "";
            txtZipCode.Text = "";
            cmbCity.ClearSelection();
            cmbprovince.ClearSelection();
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var organizationEntity = new OrganizationEntity()
                                         {
                                             OrganizationId = new Guid(ViewState["OrganizationId"].ToString())
                                         };
            var myorganization = _organizationBL.GetSingleById(organizationEntity);
            txtOrganizationAddress.Text = myorganization.OrganizationAddress;
            txtOrganizationDescription.Text = myorganization.OrganizationDescription;
            cmbIsActive.SelectedValue = myorganization.IsActive.ToString();
            txtOrganizationEmail.Text = myorganization.OrganizationEmail;
            txtOrganizationEconomicCode.Text = myorganization.OrganizationEconomicCode;
            txtOrganizationFoxNo.Text = myorganization.OrganizationFoxNo;
            txtOrganizationPersianTitle.Text = myorganization.OrganizationPersianTitle;
            txtZipCode.Text = myorganization.ZipCode;
            txtOrganizationEnglishTitle.Text = myorganization.OrganizationEnglishTitle;
            cmbprovince.SelectedValue = myorganization.ProvinceId.ToString();
            if (cmbprovince.FindItemByValue(myorganization.ProvinceId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورداستان انتخاب شده معتبر نمی باشد.");
            }
            SetCmboboxCity(myorganization.ProvinceId==null?new Guid():Guid.Parse(myorganization.ProvinceId.ToString()));
            cmbCity.SelectedValue = myorganization.CityId.ToString();
            if (cmbCity.FindItemByValue(myorganization.CityId.ToString())==null && myorganization.ProvinceId !=null )
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردشهر انتخاب شده معتبر نمی باشد.");
            }

        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
                                      {
                                          TableName = TableName,
                                          PrimaryKey = PrimaryKey,
                                          RadGrid = grdOrganization,
                                          PageSize = pageSize,
                                          CurrentPage = currentpPage,
                                          WhereClause = ViewState["WhereClause"].ToString(),
                                          OrderBy = ViewState["SortExpression"].ToString(),
                                          SortType = ViewState["SortType"].ToString(),
                                          RowSelectGuidId = rowSelectGuidId
                                      };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {
            cmbprovince.Items.Clear();
            cmbprovince.Items.Add(new RadComboBoxItem(""));
            cmbprovince.DataSource = _provinceBL.GetAllIsActive();
            cmbprovince.DataTextField = "ProvinceTitlePersian";
            cmbprovince.DataValueField = "ProvinceId";
            cmbprovince.DataBind();
        }




        private void SetCmboboxCity(Guid ProvinceId)
        {
            if (ProvinceId != new Guid())
            {
                var cityEntity = new CityEntity { ProvinceId = ProvinceId };
                cmbCity.Items.Clear();
                cmbCity.Items.Add(new RadComboBoxItem(""));
                cmbCity.DataTextField = "CityTitlePersian";
                cmbCity.DataValueField = "CityId";
                cmbCity.DataSource = _cityBL.GetCityCollectionByProvince(cityEntity);
                cmbCity.DataBind();
            }

        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdOrganization.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid OrganizationId;
                ViewState["WhereClause"] = "";
                var OrganizationEntity = new OrganizationEntity()
                                             {
                                                 CityId =
                                                     (cmbCity.SelectedIndex > 0
                                                          ? Guid.Parse(cmbCity.SelectedValue)
                                                          : new Guid()),
                                                 ZipCode = txtZipCode.Text,
                                                 IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                                                 OrganizationAddress = txtOrganizationAddress.Text,
                                                 OrganizationDescription = txtOrganizationDescription.Text,
                                                 OrganizationEconomicCode = txtOrganizationEconomicCode.Text,
                                                 OrganizationEmail = txtOrganizationEmail.Text,
                                                 OrganizationEnglishTitle = txtOrganizationEnglishTitle.Text,
                                                 OrganizationFoxNo = txtOrganizationFoxNo.Text,
                                                 OrganizationPersianTitle = txtOrganizationPersianTitle.Text,
                                                 OrganizationWebSite = txtrganizationWebSite.Text,
                                                 OrganizationTelephoneNo = txtOrganizationTelephoneNo.Text,

                                             };

                _organizationBL.Add(OrganizationEntity, out OrganizationId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdOrganization.MasterTableView.PageSize, 0, OrganizationId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(
                    ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtOrganizationEnglishTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationCenterEnglishTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtOrganizationEnglishTitle.Text.Trim()) + "%'";
                if (txtOrganizationPersianTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EducationCenterPersianTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtOrganizationPersianTitle.Text.Trim()) + "%'";
                if (txtOrganizationAddress.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OrganizationAddress Like N'%" +
                                               FarsiToArabic.ToArabic(txtOrganizationAddress.Text.Trim()) + "%'";
                if (txtOrganizationDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OrganizationDescription Like N'%" +
                                               FarsiToArabic.ToArabic(txtOrganizationDescription.Text.Trim()) + "%'";
                if (txtOrganizationEconomicCode.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OrganizationEconomicCode Like N'%" +
                                               FarsiToArabic.ToArabic(txtOrganizationEconomicCode.Text.Trim()) + "%'";
                if (txtOrganizationEmail.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OrganizationEmail Like N'%" +
                                               FarsiToArabic.ToArabic(txtOrganizationEmail.Text.Trim()) + "%'";
                if (txtZipCode.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ZipCode Like N'%" +
                                               FarsiToArabic.ToArabic(txtZipCode.Text.Trim()) + "%'";
                if (txtrganizationWebSite.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and organizationWebSite Like N'%" +
                                               FarsiToArabic.ToArabic(txtrganizationWebSite.Text.Trim()) + "%'";
                if (txtOrganizationFoxNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OrganizationFoxNo Like N'%" +
                                               FarsiToArabic.ToArabic(txtOrganizationFoxNo.Text.Trim()) + "%'";
                if (cmbprovince.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ProvinceId ='" +
                                               new Guid(cmbprovince.SelectedValue) + "'";
                if (cmbCity.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CityId ='" +
                                               new Guid(cmbCity.SelectedValue) + "'";

                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdOrganization.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdOrganization.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdOrganization.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdOrganization.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                var OrganizationEntity = new OrganizationEntity()
                                             {
                                                 OrganizationId = Guid.Parse(ViewState["OrganizationId"].ToString()),
                                                 CityId =
                                                     (cmbCity.SelectedIndex > 0
                                                          ? Guid.Parse(cmbCity.SelectedValue)
                                                          : new Guid()),
                                                 ZipCode = txtZipCode.Text,
                                                 IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                                                 OrganizationAddress = txtOrganizationAddress.Text,
                                                 OrganizationDescription = txtOrganizationDescription.Text,
                                                 OrganizationEconomicCode = txtOrganizationEconomicCode.Text,
                                                 OrganizationEmail = txtOrganizationEmail.Text,
                                                 OrganizationEnglishTitle = txtOrganizationEnglishTitle.Text,
                                                 OrganizationFoxNo = txtOrganizationFoxNo.Text,
                                                 OrganizationPersianTitle = txtOrganizationPersianTitle.Text,
                                                 OrganizationWebSite = txtrganizationWebSite.Text,
                                                 OrganizationTelephoneNo = txtOrganizationTelephoneNo.Text,

                                             };
                _organizationBL.Update(OrganizationEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdOrganization.MasterTableView.PageSize, 0,
                                                   new Guid(ViewState["OrganizationId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(
                    ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdOrganization_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["OrganizationId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";

                    var OrganizationEntity = new OrganizationEntity()
                                                 {
                                                     OrganizationId = Guid.Parse(e.CommandArgument.ToString()),

                                                 };
                    _organizationBL.Delete(OrganizationEntity);
                    var gridParamEntity = SetGridParam(grdOrganization.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdOrganization_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdOrganization.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>


        protected void grdOrganization_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdOrganization.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>
        protected void grdOrganization_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdOrganization.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }
        protected void cmbprovince_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            cmbCity.Items.Clear();
            SetCmboboxCity(e.Value.ToString()==""?new Guid(): Guid.Parse(e.Value.ToString()));
        }
        #endregion

        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GeneralProject.General.BL;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;

namespace Intranet.DesktopModules.GeneralProject.General.BaseInformation
{
    public partial class UserManagementPage : ItcBaseControl
    {

        #region PublicParam:
        private readonly UserBL _userBL = new UserBL();
        private const string TableName = "dbo.t_User";
        private const string PrimaryKey = "UserId";

        #endregion

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
            TxtFirstName.Enabled = true;
            TxtLastName.Enabled = true;
            TxtUserName.Enabled = true;
            Txtpassword.BackColor = Color.White;
            TxtPasswordConfirm.BackColor = Color.White;
            Txtpassword.Enabled = true;
            TxtPasswordConfirm.Enabled = true;
            ViewState["Mode"] = "";
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
            TxtFirstName.Enabled = true;
            TxtLastName.Enabled = true;
            TxtUserName.Enabled = true;
            Txtpassword.BackColor = Color.Silver;
            TxtPasswordConfirm.BackColor = Color.Silver;
            Txtpassword.Enabled = false;
            Txtpassword.Text = "";
            TxtPasswordConfirm.Enabled = false;
            TxtPasswordConfirm.Text = "";
            ViewState["Mode"] = "";

        }

        private void SetPanelForCHangePass()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
            TxtFirstName.Enabled = false;
            TxtLastName.Enabled = false;
            TxtUserName.Enabled = false;
            Txtpassword.Enabled = true;
            TxtPasswordConfirm.Enabled = true;
            Txtpassword.BackColor = Color.White;
            TxtPasswordConfirm.BackColor = Color.White;
            ViewState["Mode"] = "ChangePass";
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            TxtFirstName.Text = "";
            TxtLastName.Text = "";
            TxtUserName.Text = "";
            Txtpassword.Text = "";
            TxtPasswordConfirm.Text = "";
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var userId = Int32.Parse(ViewState["UserId"].ToString());
            var user = Intranet.Security.UserDB.GetSingleUser(userId);
            TxtUserName.Text = user.UserName;
            TxtFirstName.Text = user.FirstName;
            TxtLastName.Text = user.LastName;
            //Txtpassword.Text = user.Password;
            //TxtPasswordConfirm.Text = user.Password;           
            cmbIsActive.SelectedValue = (!user.IsLocked).ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdUser,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = rowSelectId
            };
            return gridParamEntity;
        }

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            ViewState["Mode"] = "";
        }

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    Response.Redirect("~/FrmLogin2.aspx");
                }
                if (!IsPostBack)
                {
                    Page.Title = "مدیریت کاربران";
                    ViewState["WhereClause"] = "  ";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grdUser.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                }

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Txtpassword.Text.Trim() != TxtPasswordConfirm.Text.Trim())
                {
                    throw new ItcApplicationErrorManagerException("تکرار رمز عبور اشتباه است!");
                }
                ViewState["WhereClause"] = "";
                var userId = _userBL.AddUser(TxtUserName.Text, TxtFirstName.Text, TxtLastName.Text, Txtpassword.Text,bool.Parse(cmbIsActive.SelectedValue));
                if (userId <= 0)
                {
                    throw new Intranet.Common.ItcException.ItcApplicationErrorManagerException("خطا در ثبت");
                }
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdUser.MasterTableView.PageSize, 0, userId);
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(
                    ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (TxtUserName.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and UserName Like N'%" +
                                               FarsiToArabic.ToArabic(TxtUserName.Text.Trim()) + "%'";
                if (TxtFirstName.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FirstName Like N'%" +
                                               FarsiToArabic.ToArabic(TxtFirstName.Text.Trim()) + "%'";
                if (TxtLastName.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and LastName Like N'%" +
                                               FarsiToArabic.ToArabic(TxtLastName.Text.Trim()) + "%'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsLocked<>'" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdUser.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdUser.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                if (grdUser.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdUser.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"].ToString() == "ChangePass")
                {
                    if (Txtpassword.Text.Trim() != TxtPasswordConfirm.Text.Trim())
                    {
                        throw new ItcApplicationErrorManagerException("تکرار رمز عبور اشتباه است!");
                    }
                    var userId = Int32.Parse(ViewState["UserId"].ToString());
                    _userBL.ChangePassword(userId, Txtpassword.Text);
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                    SetPanelFirst();
                    SetClearToolBox();
                }
                else
                {

                    var userId = Int32.Parse(ViewState["UserId"].ToString());
                    var res0 = _userBL.UpdateUser(userId, TxtUserName.Text, TxtFirstName.Text, TxtLastName.Text,bool.Parse(cmbIsActive.SelectedValue));
                    // Intranet.Security.UserDB.UpdateUserPassword(userId, Txtpassword.Text);
                    SetPanelFirst();
                    SetClearToolBox();
                    ViewState["WhereClause"] = "";
                    var gridParamEntity = SetGridParam(grdUser.MasterTableView.PageSize, 0, userId);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }

            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>


        protected void grdUser_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["UserId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";
                    var userId = Int32.Parse(e.CommandArgument.ToString());
                    Intranet.Security.UserDB.DeleteUser(userId);

                    var gridParamEntity = SetGridParam(grdUser.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
                if (e.CommandName == "_ChangePass")
                {
                    ViewState["Mode"] = "ChangePass";
                    ViewState["UserId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelForCHangePass();
                }

            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }



        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdUser_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdUser.MasterTableView.PageSize, e.NewPageIndex, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdUser_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdUser.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdUser_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {

            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdUser.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion
    }
}
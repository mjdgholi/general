﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProvincePage.ascx.cs" Inherits="Intranet.DesktopModules.GeneralProject.General.BaseInformation.ProvincePage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>


<asp:Panel ID="Panel1" runat="server">
    <table style="width: 100%; " class="MainTableOfASCX"  dir="rtl">

        <tr>
            <td dir="rtl" style="width: 100%; " valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                         
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" 
                                    CustomeButtonType="Add" onclick="btnSave_Click" >
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click">
                                    <icon primaryiconcssclass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click">
                                    <icon primaryiconcssclass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click">
                                    <icon primaryiconcssclass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click">
                                    <icon primaryiconcssclass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                                
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Panel ID="pnlDetail" runat="server" Width="100%">
                    <table>
                        
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblCountry">عنوان کشور<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                              <cc1:CustomRadComboBox ID="cmbCountry" runat="server" AppendDataBoundItems="True" AllowCustomText="True"  Filter="Contains" MarkFirstMatch="True"/>
                       
                                <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="cmbCountry"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                                                <tr>
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblProvinceTitlePersian">عنوان فارسی استان<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                                            <telerik:RadTextBox ID="txtProvinceTitlePersian" runat="server" 
                                   ></telerik:RadTextBox>
                                <asp:RequiredFieldValidator ID="rfvProvinceTitlePersian" runat="server" ControlToValidate="txtProvinceTitlePersian"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>

                            </td>
                        </tr>

                        <tr>
                            <td nowrap="nowrap">
                                <asp:Label runat="server" ID="lblProvinceTitleEnglish">عنوان انگلیسی استان<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtProvinceTitleEnglish" runat="server" 
                                ></telerik:RadTextBox>
                                <asp:RequiredFieldValidator ID="ValidProvinceTitleEnglish" runat="server" ControlToValidate="txtProvinceTitleEnglish"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    وضیعت رکورد<font color="red">*</font>:</label>
                            </td>
                            <td >
                                <cc1:CustomRadComboBox ID="cmbIsActive" runat="server">
                                    <items>
                                        <telerik:RadComboBoxItem Text=""  Value="" />
                                        <telerik:RadComboBoxItem Text="فعال" Value="True" />
                                        <telerik:RadComboBoxItem Text="غیرفعال" Value="False" />
                                    </items>
                                </cc1:CustomRadComboBox>
                                                                          <asp:RequiredFieldValidator ID="rfvalidatorVisibility" runat="server" ControlToValidate="cmbIsActive"
                                    ErrorMessage="وضیعت رکورد" InitialValue=" ">*</asp:RequiredFieldValidator>
                                
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdProvince" runat="server" AllowCustomPaging="True"
                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None"
                                    HorizontalAlign="Center"  AutoGenerateColumns="False" 
                                    onitemcommand="grdProvince_ItemCommand" 
                                    onpageindexchanged="grdProvince_PageIndexChanged" 
                                    onpagesizechanged="grdProvince_PageSizeChanged" 
                                    onsortcommand="grdProvince_SortCommand" >
                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                    </HeaderContextMenu>
                                    <MasterTableView DataKeyNames="ProvinceId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="CountryTitlePersian" HeaderText="عنوان فارسی کشور"
                                                SortExpression="CountryTitlePersian">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                            </telerik:GridBoundColumn>
                                                                   <telerik:GridBoundColumn DataField="ProvinceTitlePersian" HeaderText="عنوان فارسی استان"
                                                SortExpression="ProvinceTitlePersian">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                            </telerik:GridBoundColumn>
                                                                             <telerik:GridBoundColumn DataField="ProvinceTitleEnglish" HeaderText="عنوان انگلیسی استان"
                                                SortExpression="ProvinceTitleEnglish">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="وضیعت رکورد" SortExpression="IsActive">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridCheckBoxColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("ProvinceId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("ProvinceId").ToString() %>' CommandName="_MyِDelete"
                                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle  FirstPageToolTip="صفحه اول" HorizontalAlign="Center"
                                            LastPageToolTip="صفحه آخر" NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی"
                                            PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <ajaxsettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdProvince">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </ajaxsettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" >
</telerik:RadAjaxLoadingPanel>

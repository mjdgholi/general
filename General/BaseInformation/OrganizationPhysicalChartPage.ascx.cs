﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.GeneralProject.General.BL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using ITC.Library.Controls;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.Generalproject.General.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/23>
    /// Description: <واحد سازمانی>
    /// </summary>
    public partial class OrganizationPhysicalChartPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly OrganizationPhysicalChartBL _organizationPhysicalChartBL = new OrganizationPhysicalChartBL();
        private const string TableName = "General.t_OrganizationPhysicalChart";
        private const string PrimaryKey = "OrganizationPhysicalChartId";

        #endregion

        #region Procedure


        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
     

        void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnEdit.Visible = false;
            btnBack.Visible = false;

        }
        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        void SetPanelLast()
        {
            btnSave.Visible = false;
            btnEdit.Visible = true;
            btnBack.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        void SetClearToolBox()
        {
            txtOrganizationPhysicalChartTitle.Text = "";
            radtreOrganizationPhysicalChart.UnselectAllNodes();
            cmbIsActive.SelectedIndex = 0;
        }
        /// <summary>
        /// لود کردن نمودار سازمانی
        /// </summary>
        void SetLoadRadTreeView()
        {
            radtreOrganizationPhysicalChart.DataTextField = "OrganizationPhysicalChartTitle";
            radtreOrganizationPhysicalChart.DataValueField = "OrganizationPhysicalChartId";
            radtreOrganizationPhysicalChart.DataFieldID = "OrganizationPhysicalChartId";
            radtreOrganizationPhysicalChart.DataFieldParentID = "OwnerId";
            radtreOrganizationPhysicalChart.DataSource = _organizationPhysicalChartBL.GetAllIsActive();
            radtreOrganizationPhysicalChart.DataBind();
        }
        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        void SetDataShow()
        {
            var organizationPhysicalChartEntity = new OrganizationPhysicalChartEntity()
                                                      {
                                                          OrganizationPhysicalChartId = Guid.Parse(ViewState["OrganizationPhysicalChartId"].ToString())
                                                      };
            var MyorganizationPhysicalChart = _organizationPhysicalChartBL.GetSingleById(organizationPhysicalChartEntity);            
            SetSelectedNodeForTree((MyorganizationPhysicalChart.OwnerId == null) ? (Guid?)null : Guid.Parse(MyorganizationPhysicalChart.OwnerId.ToString()));            
            txtOrganizationPhysicalChartTitle.Text = MyorganizationPhysicalChart.OrganizationPhysicalChartTitle;
            cmbIsActive.SelectedValue = MyorganizationPhysicalChart.IsActive.ToString();
        }
        /// <summary>
        /// انتخاب شده بر اساس شناسه نمودار سازمانی فرستاده شده
        /// </summary>
        /// <param name="MenuId"></param>
        protected void SetSelectedNodeForTree(Guid? OrganizationPhysicalChartId)
        {
            SetLoadRadTreeView();
            if (OrganizationPhysicalChartId != null)
            {
                foreach (var radTreeNode in radtreOrganizationPhysicalChart.GetAllNodes())
                {
                    if (Guid.Parse(radTreeNode.Value) == OrganizationPhysicalChartId)
                    {
                        radTreeNode.ExpandParentNodes();
                        radTreeNode.Selected = true;

                    }
                }
            }

        }


        #endregion
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
                SetLoadRadTreeView();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region ControlEvent:
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid OrganizationPhysicalChartId;
                ViewState["WhereClause"] = "";
                SetPanelFirst();

                var organizationPhysicalChartEntity = new OrganizationPhysicalChartEntity()
                {
                    OrganizationPhysicalChartTitle= FarsiToArabic.ToArabic(txtOrganizationPhysicalChartTitle.Text),
                    OwnerId = ((radtreOrganizationPhysicalChart.SelectedValue)=="" ? (Guid?)null : Guid.Parse(radtreOrganizationPhysicalChart.SelectedNode.Value)),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),                    

                };

                _organizationPhysicalChartBL.Add(organizationPhysicalChartEntity, out OrganizationPhysicalChartId);
                SetClearToolBox();
                SetPanelFirst();
                SetSelectedNodeForTree(OrganizationPhysicalChartId);                
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "";
                SetPanelFirst();

                var organizationPhysicalChartEntity = new OrganizationPhysicalChartEntity()
                {
                    OrganizationPhysicalChartId = Guid.Parse(ViewState["OrganizationPhysicalChartId"].ToString()),
                    OrganizationPhysicalChartTitle = FarsiToArabic.ToArabic(txtOrganizationPhysicalChartTitle.Text),
                    OwnerId = ((radtreOrganizationPhysicalChart.SelectedValue) == "" ? (Guid?)null : Guid.Parse(radtreOrganizationPhysicalChart.SelectedNode.Value)),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),

                };

                _organizationPhysicalChartBL.Update(organizationPhysicalChartEntity);
                SetClearToolBox();
                SetPanelFirst();
                SetSelectedNodeForTree(Guid.Parse(ViewState["OrganizationPhysicalChartId"].ToString()));
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            SetClearToolBox();
            SetPanelFirst();
        }

        protected void radtreOrganizationPhysicalChart_ContextMenuItemClick(object sender, Telerik.Web.UI.RadTreeViewContextMenuEventArgs e)
        {
            try
            {
                RadTreeNode clickedNode = e.Node;
                if (e.MenuItem.Value.ToLower() == "edit")
                {
                    ViewState["OrganizationPhysicalChartId"] = Guid.Parse(clickedNode.Value);
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.MenuItem.Value.ToLower() == "remove")
                {

                    RadTreeNode radTreeNode = e.Node.ParentNode;
                    var organizationPhysicalChartEntity = new OrganizationPhysicalChartEntity()
                    {
                        OrganizationPhysicalChartId = Guid.Parse(e.Node.Value)                        

                    };
                    _organizationPhysicalChartBL.Delete(organizationPhysicalChartEntity);
                    SetLoadRadTreeView();                    
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion
    }
}
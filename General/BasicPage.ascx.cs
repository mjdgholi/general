﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.Generalproject.General
{
    public partial class BasicPage : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessageErorr.Visible = false;
            if (Session["MessageError"]!=null)
            {
                pnlMessageErorr.Visible = true;
                lblMessageError.Text = Session["MessageError"].ToString();
                Session["MessageError"] = null;
            }
            if (HttpContext.Current.User == null || !HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/FrmLogin2.aspx");
            }

            if (!IsPostBack)
            {
                this.Page.Title = "سامانه اطلاعات پایه عمومی";
                CustomItcMenuWithRowAccess1.LoadMenuItc("[General].[v_PubItcMenu]");
            }
        }

        #region Procedure:

        public string    GetModuleId()
        {
            return ModuleConfiguration.ModuleID.ToString();
        }
        private void AddPageView()
        {
            var pageView = new RadPageView() { ID = ViewState["MenuPageName"].ToString().Replace(@"\", "") };
            radmultipageMenu.PageViews.Add(pageView);
        }

        //public string GetSingleFilrtNameAndLastnameuser()
        //{
        //    DataSet ds = Classes.User.GetSingleUserFirstNameAndLastName(Classes.Variables.GetUserId());
        //    string s = "";
        //    s = s + ds.Tables[0].Rows[0]["FirstName"].ToString();
        //    s = s + " " + ds.Tables[0].Rows[0]["LastName"].ToString();
        //    return s;

        //}

        #endregion

        protected void radmultipageMenu_PageViewCreated(object sender, RadMultiPageEventArgs e)
        {
            string userControlName = @"DeskTopModules\" + ViewState["MenuPageName"] + ".ascx";
            var userControl = (Page.LoadControl(userControlName));
            if (userControl != null)
            {
                userControl.ID = ViewState["MenuPageName"].ToString().Replace(@"\", "") + "_userControl";
            }
            e.PageView.Controls.Add(userControl);
            e.PageView.Selected = true;

        }

        protected void CustomItcMenu1_ItemClick(object sender, RadPanelBarEventArgs e)
        {
            pnlMessageErorr.Visible = false;
            var ClassItcMenu = ITC.Library.Classes.JsonExecutor.DeserializationCustomData(e.Item.Value, new ITC.Library.Classes.ItcMenuParameter());
            var ClassItcMenuParameter = ((ITC.Library.Classes.ItcMenuParameter)(ClassItcMenu));
            this.Page.Title = e.Item.Text;
            if (ClassItcMenuParameter.IsLoadControl == true)
            {
                if (!string.IsNullOrEmpty(ClassItcMenuParameter.PageName))
                {
                    ViewState["MenuPageName"] = ClassItcMenuParameter.PageName;
                    radmultipageMenu.PageViews.Clear();
                    AddPageView();
                    ((radmultipageMenu.PageViews[0]).Controls[0] as ITC.Library.Classes.ITabedControl).InitControl();
                    lblSubControlTitle.Text = e.Item.Text;
                }
            }
            else
            {
                string PageUrl;
                Response.Redirect(Intranet.Common.IntranetUI.BuildUrl("/Page.aspx?mID=" +
                ModuleConfiguration.ModuleID + "&page=" + ClassItcMenuParameter.PageName));
            }
        }       
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BasicPage.ascx.cs" Inherits="Intranet.DesktopModules.Generalproject.General.BasicPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>

<table dir="rtl" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" class="HeaderOfHomePage" height="30">
            <asp:Label CssClass="LabelCSS" ID="lblSubControlTitle" runat="server" 
                Font-Names="B Titr" Font-Size="12pt" ForeColor="White">سامانه اطلاعات پایه عمومی</asp:Label>
        </td>
    </tr>
</table>
            <asp:Panel ID="pnlMessageErorr" runat="server" Visible="False" 
    BackColor="White" >
                <table>
                    <tr>
                        <td>
                          <asp:Image ID="imgMessage" runat="server" ImageUrl="Images\Error.png" />  
                        </td>
                        <td>
                           <asp:Label ID="lblMessageError" runat="server" Text="Label" Font-Bold="True" Font-Size="10"></asp:Label> 
                        </td>
                    </tr>
                </table>
                
            </asp:Panel>

<telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Height="1200px"
    CssClass="MainTableOfASCX" >
    
&nbsp;&nbsp;&nbsp; <telerik:RadPane ID="RadPane1" runat="server" Width="22px" Scrolling="None">
        <telerik:RadSlidingZone ID="RadSlidingZone1" runat="server" Width="22px" DockedPaneId="RadSlidingPane1"
            ExpandedPaneId="RadSlidingPane1" ClickToOpen="True">
            <telerik:RadSlidingPane ID="RadSlidingPane1" runat="server" Title="مـنـوی اصلی" Width="250px" 
                DockOnOpen="True" Font-Names="Tahoma">
                <cc1:CustomItcMenuWithRowAccess ID="CustomItcMenuWithRowAccess1" runat="server" MenuTableName="[pub].[t_PubItcMenu]"
                     OnItemClick="CustomItcMenu1_ItemClick" CausesValidation="False"
                    ExpandMode="SingleExpandedItem" AllowCollapseAllItems="True">
                </cc1:CustomItcMenuWithRowAccess>
     
            </telerik:RadSlidingPane>
        </telerik:RadSlidingZone>
    </telerik:RadPane>
&nbsp;&nbsp;&nbsp; <telerik:RadPane ID="RadPane2" runat="server" Height="1200px" Scrolling="None">
        <telerik:RadMultiPage ID="radmultipageMenu" runat="server" Width="100%" OnPageViewCreated="radmultipageMenu_PageViewCreated"
            Height="1200px">
        </telerik:RadMultiPage>
    </telerik:RadPane>
</telerik:RadSplitter>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="CustomItcMenuWithRowAccess1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="lblSubControlTitle" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="CustomItcMenuWithRowAccess1" />
                <telerik:AjaxUpdatedControl ControlID="radmultipageMenu" />
                <telerik:AjaxUpdatedControl ControlID="pnlMessageErorr" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="radmultipageMenu">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="lblSubControlTitle" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="CustomItcMenuWithRowAccess1" />
                <telerik:AjaxUpdatedControl ControlID="radmultipageMenu" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
<cc1:ResourceJavaScript ID="ResourceJavaScript1" runat="server" />

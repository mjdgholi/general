﻿using System;
using Intranet.Configuration.Settings;

namespace Intranet.DesktopModules.Generalproject.General    
{
    public partial class LoadPage : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect(Intranet.Common.IntranetUI.BuildUrl("/Page.aspx?mID=" + ModuleConfiguration.ModuleID + "&page=GeneralProject/General/BasicPage"));
        }
    }
}
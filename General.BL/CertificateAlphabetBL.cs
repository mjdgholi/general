﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/06>
    // Description:	<فرم شناسه حرفی شناسنامه>
    /// </summary>
    public class CertificateAlphabetBL
    {
        private readonly CertificateAlphabetDB _CertificateAlphabetDB;

        public CertificateAlphabetBL()
        {
            _CertificateAlphabetDB = new CertificateAlphabetDB();
        }

        public void Add(CertificateAlphabetEntity CertificateAlphabetEntityParam, out Guid CertificateAlphabetId)
        {
            _CertificateAlphabetDB.AddCertificateAlphabetDB(CertificateAlphabetEntityParam, out CertificateAlphabetId);
        }

        public void Update(CertificateAlphabetEntity CertificateAlphabetEntityParam)
        {
            _CertificateAlphabetDB.UpdateCertificateAlphabetDB(CertificateAlphabetEntityParam);
        }

        public void Delete(CertificateAlphabetEntity CertificateAlphabetEntityParam)
        {
            _CertificateAlphabetDB.DeleteCertificateAlphabetDB(CertificateAlphabetEntityParam);
        }

        public CertificateAlphabetEntity GetSingleById(CertificateAlphabetEntity CertificateAlphabetEntityParam)
        {
            CertificateAlphabetEntity o = GetCertificateAlphabetFromCertificateAlphabetDB(
                _CertificateAlphabetDB.GetSingleCertificateAlphabetDB(CertificateAlphabetEntityParam));

            return o;
        }

        public List<CertificateAlphabetEntity> GetAll()
        {
            List<CertificateAlphabetEntity> lst = new List<CertificateAlphabetEntity>();
            //string key = "CertificateAlphabet_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<CertificateAlphabetEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetCertificateAlphabetCollectionFromCertificateAlphabetDBList(
                _CertificateAlphabetDB.GetAllCertificateAlphabetDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<CertificateAlphabetEntity> GetAllIsActive()
        {
            List<CertificateAlphabetEntity> lst = new List<CertificateAlphabetEntity>();
            //string key = "CertificateAlphabet_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<CertificateAlphabetEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetCertificateAlphabetCollectionFromCertificateAlphabetDBList(
                _CertificateAlphabetDB.GetAllIsActiveCertificateAlphabetDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<CertificateAlphabetEntity> GetAllPaging(int currentPage, int pageSize,
                                                            string

                                                                sortExpression, out int count, string whereClause)
        {
            //string key = "CertificateAlphabet_List_Page_" + currentPage ;
            //string countKey = "CertificateAlphabet_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<CertificateAlphabetEntity> lst = new List<CertificateAlphabetEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<CertificateAlphabetEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetCertificateAlphabetCollectionFromCertificateAlphabetDBList(
                _CertificateAlphabetDB.GetPageCertificateAlphabetDB(pageSize, currentPage,
                                                                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private CertificateAlphabetEntity GetCertificateAlphabetFromCertificateAlphabetDB(CertificateAlphabetEntity o)
        {
            if (o == null)
                return null;
            CertificateAlphabetEntity ret = new CertificateAlphabetEntity(o.CertificateAlphabetId, o.CertificateAlphabetTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<CertificateAlphabetEntity> GetCertificateAlphabetCollectionFromCertificateAlphabetDBList(List<CertificateAlphabetEntity> lst)
        {
            List<CertificateAlphabetEntity> RetLst = new List<CertificateAlphabetEntity>();
            foreach (CertificateAlphabetEntity o in lst)
            {
                RetLst.Add(GetCertificateAlphabetFromCertificateAlphabetDB(o));
            }
            return RetLst;

        }
    }
}
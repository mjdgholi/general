﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.GeneralProject.General.DAL;

namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// متدهای عمومی سامانه دسترسی ها
    /// مجید قلی بیگیان
    /// </summary>
    public class AccessSystemGeneralBL
    {
        private readonly AccessSystemGeneralDB _accessSystemDb = new AccessSystemGeneralDB();

        // چک میکند که آیا کاربر عضو گروهی است که تنها می تواند اطلاعات خود را ویرایش نماید
        public bool IsOnlyOwnInfoUser(int userId)
        {
            return _accessSystemDb.IsOnlyOwnInfoUser(userId);
        }
    }
}
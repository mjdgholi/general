﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/09>
    /// Description: <نوع وابستگی>
    /// </summary>

    public class DependentTypeBL
    {
        private readonly DependentTypeDB _DependentTypeDB;

        public DependentTypeBL()
        {
            _DependentTypeDB = new DependentTypeDB();
        }

        public void Add(DependentTypeEntity DependentTypeEntityParam, out Guid DependentTypeId)
        {
            _DependentTypeDB.AddDependentTypeDB(DependentTypeEntityParam, out DependentTypeId);
        }

        public void Update(DependentTypeEntity DependentTypeEntityParam)
        {
            _DependentTypeDB.UpdateDependentTypeDB(DependentTypeEntityParam);
        }

        public void Delete(DependentTypeEntity DependentTypeEntityParam)
        {
            _DependentTypeDB.DeleteDependentTypeDB(DependentTypeEntityParam);
        }

        public DependentTypeEntity GetSingleById(DependentTypeEntity DependentTypeEntityParam)
        {
            DependentTypeEntity o = GetDependentTypeFromDependentTypeDB(
                _DependentTypeDB.GetSingleDependentTypeDB(DependentTypeEntityParam));

            return o;
        }

        public List<DependentTypeEntity> GetAll()
        {
            List<DependentTypeEntity> lst = new List<DependentTypeEntity>();
            //string key = "DependentType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DependentTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDependentTypeCollectionFromDependentTypeDBList(
                _DependentTypeDB.GetAllDependentTypeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<DependentTypeEntity> GetAllIsActive()
        {
            List<DependentTypeEntity> lst = new List<DependentTypeEntity>();
            //string key = "DependentType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DependentTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetDependentTypeCollectionFromDependentTypeDBList(
                _DependentTypeDB.GetAllDependentTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<DependentTypeEntity> GetAllPaging(int currentPage, int pageSize,
                                                      string

                                                          sortExpression, out int count, string whereClause)
        {
            //string key = "DependentType_List_Page_" + currentPage ;
            //string countKey = "DependentType_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<DependentTypeEntity> lst = new List<DependentTypeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<DependentTypeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetDependentTypeCollectionFromDependentTypeDBList(
                _DependentTypeDB.GetPageDependentTypeDB(pageSize, currentPage,
                                                        whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private DependentTypeEntity GetDependentTypeFromDependentTypeDB(DependentTypeEntity o)
        {
            if (o == null)
                return null;
            DependentTypeEntity ret = new DependentTypeEntity(o.DependentTypeId, o.DependentTypeTitle, o.CreationDate,
                                                              o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<DependentTypeEntity> GetDependentTypeCollectionFromDependentTypeDBList(
            List<DependentTypeEntity> lst)
        {
            List<DependentTypeEntity> RetLst = new List<DependentTypeEntity>();
            foreach (DependentTypeEntity o in lst)
            {
                RetLst.Add(GetDependentTypeFromDependentTypeDB(o));
            }
            return RetLst;

        }


    }


}

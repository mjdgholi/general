﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{

    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <دین>
    /// </summary>
    
    public class FaithBL
    {
        private readonly FaithDB _FaithDB;

        public FaithBL()
        {
            _FaithDB = new FaithDB();
        }

        public void Add(FaithEntity FaithEntityParam, out Guid FaithId)
        {
            _FaithDB.AddFaithDB(FaithEntityParam, out FaithId);
        }

        public void Update(FaithEntity FaithEntityParam)
        {
            _FaithDB.UpdateFaithDB(FaithEntityParam);
        }

        public void Delete(FaithEntity FaithEntityParam)
        {
            _FaithDB.DeleteFaithDB(FaithEntityParam);
        }

        public FaithEntity GetSingleById(FaithEntity FaithEntityParam)
        {
            FaithEntity o = GetFaithFromFaithDB(
                _FaithDB.GetSingleFaithDB(FaithEntityParam));

            return o;
        }

        public List<FaithEntity> GetAll()
        {
            List<FaithEntity> lst = new List<FaithEntity>();
            //string key = "Faith_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<FaithEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetFaithCollectionFromFaithDBList(
                _FaithDB.GetAllFaithDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<FaithEntity> GetAllIsActive()
        {
            List<FaithEntity> lst = new List<FaithEntity>();
            //string key = "Faith_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<FaithEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetFaithCollectionFromFaithDBList(
                _FaithDB.GetAllFaithIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<FaithEntity> GetAllPaging(int currentPage, int pageSize,
                                              string

                                                  sortExpression, out int count, string whereClause)
        {
            //string key = "Faith_List_Page_" + currentPage ;
            //string countKey = "Faith_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<FaithEntity> lst = new List<FaithEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<FaithEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetFaithCollectionFromFaithDBList(
                _FaithDB.GetPageFaithDB(pageSize, currentPage,
                                        whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private FaithEntity GetFaithFromFaithDB(FaithEntity o)
        {
            if (o == null)
                return null;
            FaithEntity ret = new FaithEntity(o.FaithId, o.FaithTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<FaithEntity> GetFaithCollectionFromFaithDBList(List<FaithEntity> lst)
        {
            List<FaithEntity> RetLst = new List<FaithEntity>();
            foreach (FaithEntity o in lst)
            {
                RetLst.Add(GetFaithFromFaithDB(o));
            }
            return RetLst;

        }
    }
}
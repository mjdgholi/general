﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/23>
    /// Description: <واحد سازمانی>
    /// </summary>


	public class OrganizationPhysicalChartBL 
	{	
	  	 private readonly OrganizationPhysicalChartDB _OrganizationPhysicalChartDB;					
			
		public OrganizationPhysicalChartBL()
		{
			_OrganizationPhysicalChartDB = new OrganizationPhysicalChartDB();
		}			
	
		public  void Add(OrganizationPhysicalChartEntity  OrganizationPhysicalChartEntityParam, out Guid OrganizationPhysicalChartId)
		{ 
			_OrganizationPhysicalChartDB.AddOrganizationPhysicalChartDB(OrganizationPhysicalChartEntityParam,out OrganizationPhysicalChartId);			
		}

		public  void Update(OrganizationPhysicalChartEntity  OrganizationPhysicalChartEntityParam)
		{
			_OrganizationPhysicalChartDB.UpdateOrganizationPhysicalChartDB(OrganizationPhysicalChartEntityParam);		
		}

		public  void Delete(OrganizationPhysicalChartEntity  OrganizationPhysicalChartEntityParam)
		{
			 _OrganizationPhysicalChartDB.DeleteOrganizationPhysicalChartDB(OrganizationPhysicalChartEntityParam);			
		}

		public  OrganizationPhysicalChartEntity GetSingleById(OrganizationPhysicalChartEntity  OrganizationPhysicalChartEntityParam)
		{
			OrganizationPhysicalChartEntity o = GetOrganizationPhysicalChartFromOrganizationPhysicalChartDB(
			_OrganizationPhysicalChartDB.GetSingleOrganizationPhysicalChartDB(OrganizationPhysicalChartEntityParam));
			
			return o;
		}

		public  List<OrganizationPhysicalChartEntity> GetAll()
		{
			List<OrganizationPhysicalChartEntity> lst = new List<OrganizationPhysicalChartEntity>();
			//string key = "OrganizationPhysicalChart_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<OrganizationPhysicalChartEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetOrganizationPhysicalChartCollectionFromOrganizationPhysicalChartDBList(
				_OrganizationPhysicalChartDB.GetAllOrganizationPhysicalChartDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<OrganizationPhysicalChartEntity> GetAllIsActive()
        {
            List<OrganizationPhysicalChartEntity> lst = new List<OrganizationPhysicalChartEntity>();
            //string key = "OrganizationPhysicalChart_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<OrganizationPhysicalChartEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetOrganizationPhysicalChartCollectionFromOrganizationPhysicalChartDBList(
            _OrganizationPhysicalChartDB.GetAllOrganizationPhysicalChartIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<OrganizationPhysicalChartEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "OrganizationPhysicalChart_List_Page_" + currentPage ;
			//string countKey = "OrganizationPhysicalChart_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<OrganizationPhysicalChartEntity> lst = new List<OrganizationPhysicalChartEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<OrganizationPhysicalChartEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetOrganizationPhysicalChartCollectionFromOrganizationPhysicalChartDBList(
				_OrganizationPhysicalChartDB.GetPageOrganizationPhysicalChartDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  OrganizationPhysicalChartEntity GetOrganizationPhysicalChartFromOrganizationPhysicalChartDB(OrganizationPhysicalChartEntity o)
		{
	if(o == null)
                return null;
			OrganizationPhysicalChartEntity ret = new OrganizationPhysicalChartEntity(o.OrganizationPhysicalChartId ,o.OwnerId ,o.OrganizationPhysicalChartTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive,o.ChildCount,o.FlagSearch );
			return ret;
		}
		
		private  List<OrganizationPhysicalChartEntity> GetOrganizationPhysicalChartCollectionFromOrganizationPhysicalChartDBList( List<OrganizationPhysicalChartEntity> lst)
		{
			List<OrganizationPhysicalChartEntity> RetLst = new List<OrganizationPhysicalChartEntity>();
			foreach(OrganizationPhysicalChartEntity o in lst)
			{
				RetLst.Add(GetOrganizationPhysicalChartFromOrganizationPhysicalChartDB(o));
			}
			return RetLst;
			
		}


        public List<OrganizationPhysicalChartEntity> GetAllOnDemandOrganizationPhysicalChart(OrganizationPhysicalChartEntity organizationPhysicalChartEntityParam)
        {
            List<OrganizationPhysicalChartEntity> lst = new List<OrganizationPhysicalChartEntity>();
            //string key = "OrganizationPhysicalChart_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<OrganizationPhysicalChartEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetOrganizationPhysicalChartCollectionFromOrganizationPhysicalChartDBList(
            _OrganizationPhysicalChartDB.GetAllOnDemandOrganizationPhysicalChartDB(organizationPhysicalChartEntityParam));
            return lst;
            //	InsertIntoCache(lst, key, 600);
        }

        public List<OrganizationPhysicalChartEntity> SearchOnDemandOrganizationPhysicalChart(OrganizationPhysicalChartEntity organizationPhysicalChartEntity)
        {
            List<OrganizationPhysicalChartEntity> lst = new List<OrganizationPhysicalChartEntity>();
            //string key = "OrganizationPhysicalChart_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<OrganizationPhysicalChartEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetOrganizationPhysicalChartCollectionFromOrganizationPhysicalChartDBList(
            _OrganizationPhysicalChartDB.SearchOnDemandOrganizationPhysicalChartDB(organizationPhysicalChartEntity));
            return lst;
            //	InsertIntoCache(lst, key, 600);
        }
	}


}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes.ItcException;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/07/06>
    // Description:	<فرم اشخاص>
    /// </summary>
    public class PersonBL
    {
        private readonly PersonDB _PersonDB;

        public PersonBL()
        {
            _PersonDB = new PersonDB();
        }

        private void RequiredFieldValidation(PersonEntity personEntityParam)
        {
            if (personEntityParam.HasNationalNo && personEntityParam.NationalNo.Trim() == "")
            {
                throw new ItcApplicationErrorManagerException("کد ملی را وارد نمایید");
            }
        }

        public void Add(PersonEntity personEntityParam, out Guid personId)
        {
            RequiredFieldValidation(personEntityParam);
            _PersonDB.AddPersonDB(personEntityParam, out personId);
        }

        public void Update(PersonEntity personEntityParam)
        {
            RequiredFieldValidation(personEntityParam);
            _PersonDB.UpdatePersonDB(personEntityParam);
        }

        public void Delete(PersonEntity PersonEntityParam)
        {
            _PersonDB.DeletePersonDB(PersonEntityParam);
        }

        public PersonEntity GetSingleById(PersonEntity PersonEntityParam)
        {
            PersonEntity o = GetPersonFromPersonDB(
                _PersonDB.GetSinglePersonDB(PersonEntityParam));

            return o;
        }

        public List<PersonEntity> GetAll()
        {
            List<PersonEntity> lst = new List<PersonEntity>();
            //string key = "Person_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<PersonEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetPersonCollectionFromPersonDBList(
                _PersonDB.GetAllPersonDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<PersonEntity> GetAllPaging(int currentPage, int pageSize, string sortExpression, out int count, string whereClause)
        {
            //string key = "Person_List_Page_" + currentPage ;
            //string countKey = "Person_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<PersonEntity> lst = new List<PersonEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<PersonEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetPersonCollectionFromPersonDBList(
                _PersonDB.GetPagePersonDB(pageSize, currentPage,
                                          whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private PersonEntity GetPersonFromPersonDB(PersonEntity o)
        {
            if (o == null)
                return null;
            PersonEntity ret = new PersonEntity(o.PersonId, o.GenderId, o.BloodGroupId, o.HasNationalNo, o.NationalNo, o.FirstName, o.LastName, o.PreviousLastName, o.FatherName, o.BirthDate, o.DeathDate, o.CertificateNo, o.BirthPlaceCityId, o.BirthPlaceSection, o.IssuePlaceCityId, o.IssuePlaceSection, o.IssueDate, o.CertificateAlphabetId, o.CertificateNumericSeries, o.CertificateSerialNo, o.EnglishFirstName, o.EnglishLastName, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<PersonEntity> GetPersonCollectionFromPersonDBList(List<PersonEntity> lst)
        {
            List<PersonEntity> RetLst = new List<PersonEntity>();
            foreach (PersonEntity o in lst)
            {
                RetLst.Add(GetPersonFromPersonDB(o));
            }
            return RetLst;

        }

        public List<PersonEntity> GetPersonCollectionByBloodGroup(int BloodGroupId)
        {
            return GetPersonCollectionFromPersonDBList(_PersonDB.GetPersonDBCollectionByBloodGroupDB(BloodGroupId));
        }

        public List<PersonEntity> GetPersonCollectionByBirthPlaceCity(int BirthPlaceCityId)
        {
            return GetPersonCollectionFromPersonDBList(_PersonDB.GetPersonDBCollectionByBirthPlaceCityDB(BirthPlaceCityId));
        }

        public List<PersonEntity> GetPersonCollectionByIssuePlaceCity(int IssuePlaceCityId)
        {
            return GetPersonCollectionFromPersonDBList(_PersonDB.GetPersonDBCollectionByIssuePlaceCityDB(IssuePlaceCityId));
        }

        public List<PersonEntity> GetPersonCollectionByGender(int GenderId)
        {
            return GetPersonCollectionFromPersonDBList(_PersonDB.GetPersonDBCollectionByGenderDB(GenderId));
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;

namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/08>
    /// Description: <مذهب>
    /// </summary>

    public class ReligionBL
    {
        private readonly ReligionDB _ReligionDB;

        public ReligionBL()
        {
            _ReligionDB = new ReligionDB();
        }

        public void Add(ReligionEntity ReligionEntityParam, out Guid ReligionId)
        {
            _ReligionDB.AddReligionDB(ReligionEntityParam, out ReligionId);
        }

        public void Update(ReligionEntity ReligionEntityParam)
        {
            _ReligionDB.UpdateReligionDB(ReligionEntityParam);
        }

        public void Delete(ReligionEntity ReligionEntityParam)
        {
            _ReligionDB.DeleteReligionDB(ReligionEntityParam);
        }

        public ReligionEntity GetSingleById(ReligionEntity ReligionEntityParam)
        {
            ReligionEntity o = GetReligionFromReligionDB(
                _ReligionDB.GetSingleReligionDB(ReligionEntityParam));

            return o;
        }

        public List<ReligionEntity> GetAll()
        {
            List<ReligionEntity> lst = new List<ReligionEntity>();
            //string key = "Religion_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ReligionEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetReligionCollectionFromReligionDBList(
                _ReligionDB.GetAllReligionDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ReligionEntity> GetAllIsActive()
        {
            List<ReligionEntity> lst = new List<ReligionEntity>();
            //string key = "Religion_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ReligionEntity>)HttpContext.Current.Cache[key];
            //}e
            //else
            //{
            lst = GetReligionCollectionFromReligionDBList(
                _ReligionDB.GetAllReligionIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<ReligionEntity> GetAllPaging(int currentPage, int pageSize,
                                                 string

                                                     sortExpression, out int count, string whereClause)
        {
            //string key = "Religion_List_Page_" + currentPage ;
            //string countKey = "Religion_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ReligionEntity> lst = new List<ReligionEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ReligionEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetReligionCollectionFromReligionDBList(
                _ReligionDB.GetPageReligionDB(pageSize, currentPage,
                                              whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ReligionEntity GetReligionFromReligionDB(ReligionEntity o)
        {
            if (o == null)
                return null;
            ReligionEntity ret = new ReligionEntity(o.ReligionId, o.ReligionTitle, o.CreationDate, o.ModificationDate,
                                                    o.IsActive);
            return ret;
        }

        private List<ReligionEntity> GetReligionCollectionFromReligionDBList(List<ReligionEntity> lst)
        {
            List<ReligionEntity> RetLst = new List<ReligionEntity>();
            foreach (ReligionEntity o in lst)
            {
                RetLst.Add(GetReligionFromReligionDB(o));
            }
            return RetLst;

        }


    }

}

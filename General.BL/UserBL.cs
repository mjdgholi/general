﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using GeneralProject.General.Entity;
using Intranet.Configuration;
using Intranet.DesktopModules.GeneralProject.General.DAL;

namespace GeneralProject.General.BL
{

	public class UserBL : DeskTopObj
	{	
	  	 private readonly UserDB _UserDB;					
			
		public UserBL()
		{
			_UserDB = new UserDB();
		}			
	
		public  void Add(UserEntity  UserEntityParam,out int UserID)
		{ 
			_UserDB.AddUserDB(UserEntityParam,out UserID);			
		}

		public  void Update(UserEntity  UserEntityParam)
		{
			_UserDB.UpdateUserDB(UserEntityParam);		
		}

		public  void Delete(UserEntity  UserEntityParam)
		{
			 _UserDB.DeleteUserDB(UserEntityParam);			
		}

		public  UserEntity GetSingleById(UserEntity  UserEntityParam)
		{
			UserEntity o = GetUserFromUserDB(
			_UserDB.GetSingleUserDB(UserEntityParam));
			
			return o;
		}

		public  List<UserEntity> GetAll()
		{
			List<UserEntity> lst = new List<UserEntity>();
			//string key = "User_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<UserEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetUserCollectionFromUserDBList(
				_UserDB.GetAllUserDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public DataSet GetAllIsActive()
        {
            List<UserEntity> lst = new List<UserEntity>();
            //string key = "User_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<UserEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            return _UserDB.GetAllUserIsActiveDB();
            
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<UserEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "User_List_Page_" + currentPage ;
			//string countKey = "User_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<UserEntity> lst = new List<UserEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<UserEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetUserCollectionFromUserDBList(
				_UserDB.GetPageUserDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  UserEntity GetUserFromUserDB(UserEntity o)
		{
	if(o == null)
                return null;
			UserEntity ret = new UserEntity(o.UserID ,o.FirstName ,o.LastName ,o.UserPass ,o.Email ,o.UserStyle ,o.UserName ,o.PortalID ,o.IsSuperUser ,o.IsLocked ,o.LastLoginDate ,o.LastPasswordChangeDate ,o.LastLockDate ,o.LastUnLockDate ,o.FailedPasswordAttemptCount ,o.FailedPasswordAttemptStart ,o.Mobile ,o.HasChangedPassword ,o.JoinDate ,o.ConfirmDate ,o.IsAnonymous ,o.UserGuid ,o.LastIP,o.FullName );
			return ret;
		}
		
		private  List<UserEntity> GetUserCollectionFromUserDBList( List<UserEntity> lst)
		{
			List<UserEntity> RetLst = new List<UserEntity>();
			foreach(UserEntity o in lst)
			{
				RetLst.Add(GetUserFromUserDB(o));
			}
			return RetLst;
			
		}


        public int AddUser(string userName, string fName, string lName, string passWord,bool isActive)
        {
            return _UserDB.AddUserDB(userName, fName, lName, passWord,isActive);
        }

	    public void ChangePassword(int userId, string text)
	    {
	        _UserDB.ChangePasswordDB(userId, text);
	    }

	    public bool UpdateUser(int userId,string userName, string fName, string lName,bool isActive)
	    {
	        return _UserDB.UpdateUserDB(userId, userName, fName, lName,isActive);
	    }


	}



}


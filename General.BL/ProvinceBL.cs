﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/06>
    /// Description: <استان>
    /// </summary>
    public class ProvinceBL
    {
        private readonly ProvinceDB _ProvinceDB;

        public ProvinceBL()
        {
            _ProvinceDB = new ProvinceDB();
        }

        public void Add(ProvinceEntity ProvinceEntityParam, out Guid ProvinceId)
        {
            _ProvinceDB.AddProvinceDB(ProvinceEntityParam, out ProvinceId);
        }

        public void Update(ProvinceEntity ProvinceEntityParam)
        {
            _ProvinceDB.UpdateProvinceDB(ProvinceEntityParam);
        }

        public void Delete(ProvinceEntity ProvinceEntityParam)
        {
            _ProvinceDB.DeleteProvinceDB(ProvinceEntityParam);
        }

        public ProvinceEntity GetSingleById(ProvinceEntity ProvinceEntityParam)
        {
            ProvinceEntity o = GetProvinceFromProvinceDB(
                _ProvinceDB.GetSingleProvinceDB(ProvinceEntityParam));

            return o;
        }

        public List<ProvinceEntity> GetAll()
        {
            List<ProvinceEntity> lst = new List<ProvinceEntity>();
            //string key = "Province_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProvinceEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProvinceCollectionFromProvinceDBList(
                _ProvinceDB.GetAllProvinceDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ProvinceEntity> GetAllIsActive()
        {
            List<ProvinceEntity> lst = new List<ProvinceEntity>();
            //string key = "Province_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProvinceEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetProvinceCollectionFromProvinceDBList(
                _ProvinceDB.GetAllProvinceIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        // ---------- لود کردن بر اساس سامانه دسترسی ها
        public List<ProvinceEntity> GetAllProvinceIsActivePerRowAccessDB(int webUserId, string objectTitlesForRowAccessStr)
        {
            return _ProvinceDB.GetAllProvinceIsActivePerRowAccessDB(webUserId, objectTitlesForRowAccessStr);
        }

        // ---------- لود کردن بر اساس سامانه دسترسی ها

        public List<ProvinceEntity> GetAllPaging(int currentPage, int pageSize,
                                                 string

                                                     sortExpression, out int count, string whereClause)
        {
            //string key = "Province_List_Page_" + currentPage ;
            //string countKey = "Province_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ProvinceEntity> lst = new List<ProvinceEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ProvinceEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetProvinceCollectionFromProvinceDBList(
                _ProvinceDB.GetPageProvinceDB(pageSize, currentPage,
                                              whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ProvinceEntity GetProvinceFromProvinceDB(ProvinceEntity o)
        {
            if (o == null)
                return null;
            ProvinceEntity ret = new ProvinceEntity(o.ProvinceId, o.CountryId, o.ProvinceTitlePersian,
                                                    o.ProvinceTitleEnglish, o.CreationDate, o.ModificationDate,
                                                    o.IsActive);
            return ret;
        }

        private List<ProvinceEntity> GetProvinceCollectionFromProvinceDBList(List<ProvinceEntity> lst)
        {
            List<ProvinceEntity> RetLst = new List<ProvinceEntity>();
            foreach (ProvinceEntity o in lst)
            {
                RetLst.Add(GetProvinceFromProvinceDB(o));
            }
            return RetLst;

        }


        public List<ProvinceEntity> GetProvinceCollectionByCountry(ProvinceEntity ProvinceEntityParam)
        {
            return
                GetProvinceCollectionFromProvinceDBList(
                    _ProvinceDB.GetProvinceDBCollectionByCountryDB(ProvinceEntityParam));
        }

    }
}


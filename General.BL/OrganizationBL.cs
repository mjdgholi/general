﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/04>
    /// Description: <سازمان>
    /// </summary>

	public class OrganizationBL 
	{	
	  	 private readonly OrganizationDB _OrganizationDB;					
			
		public OrganizationBL()
		{
			_OrganizationDB = new OrganizationDB();
		}			
	
		public  void Add(OrganizationEntity  OrganizationEntityParam, out Guid OrganizationId)
		{ 
			_OrganizationDB.AddOrganizationDB(OrganizationEntityParam,out OrganizationId);			
		}

		public  void Update(OrganizationEntity  OrganizationEntityParam)
		{
			_OrganizationDB.UpdateOrganizationDB(OrganizationEntityParam);		
		}

		public  void Delete(OrganizationEntity  OrganizationEntityParam)
		{
			 _OrganizationDB.DeleteOrganizationDB(OrganizationEntityParam);			
		}

		public  OrganizationEntity GetSingleById(OrganizationEntity  OrganizationEntityParam)
		{
			OrganizationEntity o = GetOrganizationFromOrganizationDB(
			_OrganizationDB.GetSingleOrganizationDB(OrganizationEntityParam));
			
			return o;
		}

		public  List<OrganizationEntity> GetAll()
		{
			List<OrganizationEntity> lst = new List<OrganizationEntity>();
			//string key = "Organization_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<OrganizationEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetOrganizationCollectionFromOrganizationDBList(
				_OrganizationDB.GetAllOrganizationDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<OrganizationEntity> GetAllIsActive()
        {
            List<OrganizationEntity> lst = new List<OrganizationEntity>();
            //string key = "Organization_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<OrganizationEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetOrganizationCollectionFromOrganizationDBList(
            _OrganizationDB.GetAllOrganizationIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<OrganizationEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Organization_List_Page_" + currentPage ;
			//string countKey = "Organization_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<OrganizationEntity> lst = new List<OrganizationEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<OrganizationEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetOrganizationCollectionFromOrganizationDBList(
				_OrganizationDB.GetPageOrganizationDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  OrganizationEntity GetOrganizationFromOrganizationDB(OrganizationEntity o)
		{
	if(o == null)
                return null;
			OrganizationEntity ret = new OrganizationEntity(o.OrganizationId ,o.OrganizationPersianTitle ,o.OrganizationEnglishTitle ,o.CityId ,o.OrganizationEconomicCode ,o.OrganizationTelephoneNo ,o.OrganizationFoxNo ,o.OrganizationAddress ,o.OrganizationWebSite ,o.OrganizationEmail ,o.ZipCode ,o.OrganizationDescription ,o.CreationDate ,o.ModificationDate,o.IsActive,o.ProvinceId );
			return ret;
		}
		
		private  List<OrganizationEntity> GetOrganizationCollectionFromOrganizationDBList( List<OrganizationEntity> lst)
		{
			List<OrganizationEntity> RetLst = new List<OrganizationEntity>();
			foreach(OrganizationEntity o in lst)
			{
				RetLst.Add(GetOrganizationFromOrganizationDB(o));
			}
			return RetLst;
			
		} 
				
		
	}

}


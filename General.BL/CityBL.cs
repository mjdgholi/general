﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/06>
    /// Description: <شهر>
    /// </summary>


    public class CityBL
    {
        private readonly CityDB _CityDB;

        public CityBL()
        {
            _CityDB = new CityDB();
        }

        public void Add(CityEntity CityEntityParam, out Guid CityId)
        {
            _CityDB.AddCityDB(CityEntityParam, out CityId);
        }

        public void Update(CityEntity CityEntityParam)
        {
            _CityDB.UpdateCityDB(CityEntityParam);
        }

        public void Delete(CityEntity CityEntityParam)
        {
            _CityDB.DeleteCityDB(CityEntityParam);
        }

        public CityEntity GetSingleById(CityEntity CityEntityParam)
        {
            CityEntity o = GetCityFromCityDB(
                _CityDB.GetSingleCityDB(CityEntityParam));

            return o;
        }

        public List<CityEntity> GetAll()
        {
            List<CityEntity> lst = new List<CityEntity>();
            //string key = "City_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<CityEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetCityCollectionFromCityDBList(
                _CityDB.GetAllCityDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<CityEntity> GetAllIsActive()
        {
            List<CityEntity> lst = new List<CityEntity>();
            //string key = "City_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<CityEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetCityCollectionFromCityDBList(
                _CityDB.GetAllIsActiveCityDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<CityEntity> GetAllPaging(int currentPage, int pageSize,
                                             string

                                                 sortExpression, out int count, string whereClause)
        {
            //string key = "City_List_Page_" + currentPage ;
            //string countKey = "City_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<CityEntity> lst = new List<CityEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<CityEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetCityCollectionFromCityDBList(
                _CityDB.GetPageCityDB(pageSize, currentPage,
                                      whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private CityEntity GetCityFromCityDB(CityEntity o)
        {
            if (o == null)
                return null;
            CityEntity ret = new CityEntity(o.CityId, o.ProvinceId, o.GeographicScopeId, o.CityTitlePersian,
                                            o.CityTitleEnglish, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<CityEntity> GetCityCollectionFromCityDBList(List<CityEntity> lst)
        {
            List<CityEntity> RetLst = new List<CityEntity>();
            foreach (CityEntity o in lst)
            {
                RetLst.Add(GetCityFromCityDB(o));
            }
            return RetLst;

        }




        public List<CityEntity> GetCityCollectionByGeographicScope(CityEntity CityEntityParam)
        {
            return GetCityCollectionFromCityDBList(_CityDB.GetCityDBCollectionByGeographicScopeDB(CityEntityParam));
        }

        public List<CityEntity> GetCityCollectionByProvince(CityEntity CityEntityParam)
        {
            return GetCityCollectionFromCityDBList(_CityDB.GetCityDBCollectionByProvinceDB(CityEntityParam));
        }



    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeneralProject.General.DAL;
using GeneralProject.General.Entity;
using Intranet.Configuration;

namespace GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/04>
    /// Description: <دسترسی به آیکن منو>
    /// </summary>
	public class ModuleArrangementAccessBL : DeskTopObj
	{	
	  	 private readonly ModuleArrangementAccessDB _ModuleArrangementAccessDB;					
			
		public ModuleArrangementAccessBL()
		{
			_ModuleArrangementAccessDB = new ModuleArrangementAccessDB();
		}			
	
		public  void Add(ModuleArrangementAccessEntity  ModuleArrangementAccessEntityParam,out int ModuleArrangementAccessId)
		{ 
			_ModuleArrangementAccessDB.AddModuleArrangementAccessDB(ModuleArrangementAccessEntityParam,out ModuleArrangementAccessId);			
		}

		public  void Update(ModuleArrangementAccessEntity  ModuleArrangementAccessEntityParam)
		{
			_ModuleArrangementAccessDB.UpdateModuleArrangementAccessDB(ModuleArrangementAccessEntityParam);		
		}

		public  void Delete(ModuleArrangementAccessEntity  ModuleArrangementAccessEntityParam)
		{
			 _ModuleArrangementAccessDB.DeleteModuleArrangementAccessDB(ModuleArrangementAccessEntityParam);			
		}

		public  ModuleArrangementAccessEntity GetSingleById(ModuleArrangementAccessEntity  ModuleArrangementAccessEntityParam)
		{
			ModuleArrangementAccessEntity o = GetModuleArrangementAccessFromModuleArrangementAccessDB(
			_ModuleArrangementAccessDB.GetSingleModuleArrangementAccessDB(ModuleArrangementAccessEntityParam));
			
			return o;
		}

		public  List<ModuleArrangementAccessEntity> GetAll()
		{
			List<ModuleArrangementAccessEntity> lst = new List<ModuleArrangementAccessEntity>();
			//string key = "ModuleArrangementAccess_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ModuleArrangementAccessEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetModuleArrangementAccessCollectionFromModuleArrangementAccessDBList(
				_ModuleArrangementAccessDB.GetAllModuleArrangementAccessDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<ModuleArrangementAccessEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "ModuleArrangementAccess_List_Page_" + currentPage ;
			//string countKey = "ModuleArrangementAccess_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<ModuleArrangementAccessEntity> lst = new List<ModuleArrangementAccessEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ModuleArrangementAccessEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetModuleArrangementAccessCollectionFromModuleArrangementAccessDBList(
				_ModuleArrangementAccessDB.GetPageModuleArrangementAccessDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  ModuleArrangementAccessEntity GetModuleArrangementAccessFromModuleArrangementAccessDB(ModuleArrangementAccessEntity o)
		{
	if(o == null)
                return null;
			ModuleArrangementAccessEntity ret = new ModuleArrangementAccessEntity(o.ModuleArrangementAccessId ,o.UserId ,o.ModuleArrangementId,o.ModuleArrangementOwnerId );
			return ret;
		}
		
		private  List<ModuleArrangementAccessEntity> GetModuleArrangementAccessCollectionFromModuleArrangementAccessDBList( List<ModuleArrangementAccessEntity> lst)
		{
			List<ModuleArrangementAccessEntity> RetLst = new List<ModuleArrangementAccessEntity>();
			foreach(ModuleArrangementAccessEntity o in lst)
			{
				RetLst.Add(GetModuleArrangementAccessFromModuleArrangementAccessDB(o));
			}
			return RetLst;
			
		} 
				

}

}

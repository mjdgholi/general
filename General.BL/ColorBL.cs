﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/25>
    /// Description: <رنگ>
    /// </summary>

    public class ColorBL
    {
        private readonly ColorDB _ColorDB;

        public ColorBL()
        {
            _ColorDB = new ColorDB();
        }

        public void Add(ColorEntity ColorEntityParam, out Guid ColorId)
        {
            _ColorDB.AddColorDB(ColorEntityParam, out ColorId);
        }

        public void Update(ColorEntity ColorEntityParam)
        {
            _ColorDB.UpdateColorDB(ColorEntityParam);
        }

        public void Delete(ColorEntity ColorEntityParam)
        {
            _ColorDB.DeleteColorDB(ColorEntityParam);
        }

        public ColorEntity GetSingleById(ColorEntity ColorEntityParam)
        {
            ColorEntity o = GetColorFromColorDB(
                _ColorDB.GetSingleColorDB(ColorEntityParam));

            return o;
        }

        public List<ColorEntity> GetAll()
        {
            List<ColorEntity> lst = new List<ColorEntity>();
            //string key = "Color_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ColorEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetColorCollectionFromColorDBList(
                _ColorDB.GetAllColorDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ColorEntity> GetAllIsActive()
        {
            List<ColorEntity> lst = new List<ColorEntity>();
            //string key = "Color_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ColorEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetColorCollectionFromColorDBList(
                _ColorDB.GetAllColorIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ColorEntity> GetAllPaging(int currentPage, int pageSize,
                                              string

                                                  sortExpression, out int count, string whereClause)
        {
            //string key = "Color_List_Page_" + currentPage ;
            //string countKey = "Color_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ColorEntity> lst = new List<ColorEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ColorEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetColorCollectionFromColorDBList(
                _ColorDB.GetPageColorDB(pageSize, currentPage,
                                        whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ColorEntity GetColorFromColorDB(ColorEntity o)
        {
            if (o == null)
                return null;
            ColorEntity ret = new ColorEntity(o.ColorId, o.ColorTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<ColorEntity> GetColorCollectionFromColorDBList(List<ColorEntity> lst)
        {
            List<ColorEntity> RetLst = new List<ColorEntity>();
            foreach (ColorEntity o in lst)
            {
                RetLst.Add(GetColorFromColorDB(o));
            }
            return RetLst;

        }
    }
}

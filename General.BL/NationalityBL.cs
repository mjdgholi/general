﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/07>
    /// Description: <وضیعت ملیت>
    /// </summary>

//-------------------------------------------------------


	/// <summary>
	/// 
	/// </summary>
	public class NationalityBL 
	{	
	  	 private readonly NationalityDB _NationalityDB;					
			
		public NationalityBL()
		{
			_NationalityDB = new NationalityDB();
		}			
	
		public  void Add(NationalityEntity  NationalityEntityParam, out Guid NationalityId)
		{ 
			_NationalityDB.AddNationalityDB(NationalityEntityParam,out NationalityId);			
		}

		public  void Update(NationalityEntity  NationalityEntityParam)
		{
			_NationalityDB.UpdateNationalityDB(NationalityEntityParam);		
		}

		public  void Delete(NationalityEntity  NationalityEntityParam)
		{
			 _NationalityDB.DeleteNationalityDB(NationalityEntityParam);			
		}

		public  NationalityEntity GetSingleById(NationalityEntity  NationalityEntityParam)
		{
			NationalityEntity o = GetNationalityFromNationalityDB(
			_NationalityDB.GetSingleNationalityDB(NationalityEntityParam));
			
			return o;
		}

		public  List<NationalityEntity> GetAll()
		{
			List<NationalityEntity> lst = new List<NationalityEntity>();
			//string key = "Nationality_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<NationalityEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetNationalityCollectionFromNationalityDBList(
				_NationalityDB.GetAllNationalityDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<NationalityEntity> GetAllIsActive()
        {
            List<NationalityEntity> lst = new List<NationalityEntity>();
            //string key = "Nationality_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<NationalityEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetNationalityCollectionFromNationalityDBList(
            _NationalityDB.GetAllNationalityIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		
		public  List<NationalityEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Nationality_List_Page_" + currentPage ;
			//string countKey = "Nationality_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<NationalityEntity> lst = new List<NationalityEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<NationalityEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetNationalityCollectionFromNationalityDBList(
				_NationalityDB.GetPageNationalityDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  NationalityEntity GetNationalityFromNationalityDB(NationalityEntity o)
		{
	if(o == null)
                return null;
			NationalityEntity ret = new NationalityEntity(o.NationalityId ,o.CountryId ,o.NationalityTitlePersian ,o.NationalityTitleEnglish ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<NationalityEntity> GetNationalityCollectionFromNationalityDBList( List<NationalityEntity> lst)
		{
			List<NationalityEntity> RetLst = new List<NationalityEntity>();
			foreach(NationalityEntity o in lst)
			{
				RetLst.Add(GetNationalityFromNationalityDB(o));
			}
			return RetLst;
			
		} 
				
		
	

public  List<NationalityEntity> GetNationalityCollectionByCountry(NationalityEntity  NationalityEntityParam)
{
	return GetNationalityCollectionFromNationalityDBList(_NationalityDB.GetNationalityDBCollectionByCountryDB(NationalityEntityParam));
}




}

}

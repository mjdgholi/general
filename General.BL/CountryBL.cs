﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/08>
    /// Description: <کشور>
    /// </summary>
	public class CountryBL 
	{	
	  	 private readonly CountryDB _CountryDB;					
			
		public CountryBL()
		{
			_CountryDB = new CountryDB();
		}			
	
		public  void Add(CountryEntity  CountryEntityParam, out Guid CountryId)
		{ 
			_CountryDB.AddCountryDB(CountryEntityParam,out CountryId);			
		}

		public  void Update(CountryEntity  CountryEntityParam)
		{
			_CountryDB.UpdateCountryDB(CountryEntityParam);		
		}

		public  void Delete(CountryEntity  CountryEntityParam)
		{
			 _CountryDB.DeleteCountryDB(CountryEntityParam);			
		}

		public  CountryEntity GetSingleById(CountryEntity  CountryEntityParam)
		{
			CountryEntity o = GetCountryFromCountryDB(
			_CountryDB.GetSingleCountryDB(CountryEntityParam));
			
			return o;
		}

		public  List<CountryEntity> GetAll()
		{
			List<CountryEntity> lst = new List<CountryEntity>();
			//string key = "Country_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<CountryEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetCountryCollectionFromCountryDBList(
				_CountryDB.GetAllCountryDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<CountryEntity> GetAllIsActive()
        {
            List<CountryEntity> lst = new List<CountryEntity>();
            //string key = "Country_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<CountryEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetCountryCollectionFromCountryDBList(
            _CountryDB.GetAllCountryIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<CountryEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Country_List_Page_" + currentPage ;
			//string countKey = "Country_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<CountryEntity> lst = new List<CountryEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<CountryEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetCountryCollectionFromCountryDBList(
				_CountryDB.GetPageCountryDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  CountryEntity GetCountryFromCountryDB(CountryEntity o)
		{
	if(o == null)
                return null;
			CountryEntity ret = new CountryEntity(o.CountryId ,o.CountryTitlePersian ,o.CountryTitleEnglish ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<CountryEntity> GetCountryCollectionFromCountryDBList( List<CountryEntity> lst)
		{
			List<CountryEntity> RetLst = new List<CountryEntity>();
			foreach(CountryEntity o in lst)
			{
				RetLst.Add(GetCountryFromCountryDB(o));
			}
			return RetLst;
			
		} 
				
		
	}


}

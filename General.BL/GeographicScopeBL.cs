﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/06>
    /// Description: <حوزه جغرافیایی>
    /// </summary>

	public class GeographicScopeBL 
	{	
	  	 private readonly GeographicScopeDB _GeographicScopeDB;					
			
		public GeographicScopeBL()
		{
			_GeographicScopeDB = new GeographicScopeDB();
		}			
	
		public  void Add(GeographicScopeEntity  GeographicScopeEntityParam, out Guid GeographicScopeId)
		{ 
			_GeographicScopeDB.AddGeographicScopeDB(GeographicScopeEntityParam,out GeographicScopeId);			
		}

		public  void Update(GeographicScopeEntity  GeographicScopeEntityParam)
		{
			_GeographicScopeDB.UpdateGeographicScopeDB(GeographicScopeEntityParam);		
		}

		public  void Delete(GeographicScopeEntity  GeographicScopeEntityParam)
		{
			 _GeographicScopeDB.DeleteGeographicScopeDB(GeographicScopeEntityParam);			
		}

		public  GeographicScopeEntity GetSingleById(GeographicScopeEntity  GeographicScopeEntityParam)
		{
			GeographicScopeEntity o = GetGeographicScopeFromGeographicScopeDB(
			_GeographicScopeDB.GetSingleGeographicScopeDB(GeographicScopeEntityParam));
			
			return o;
		}

		public  List<GeographicScopeEntity> GetAll()
		{
			List<GeographicScopeEntity> lst = new List<GeographicScopeEntity>();
			//string key = "GeographicScope_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<GeographicScopeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetGeographicScopeCollectionFromGeographicScopeDBList(
				_GeographicScopeDB.GetAllGeographicScopeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<GeographicScopeEntity> GetAllIsActive()
        {
            List<GeographicScopeEntity> lst = new List<GeographicScopeEntity>();
            //string key = "GeographicScope_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<GeographicScopeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetGeographicScopeCollectionFromGeographicScopeDBList(
            _GeographicScopeDB.GetAllGeographicScopeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<GeographicScopeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "GeographicScope_List_Page_" + currentPage ;
			//string countKey = "GeographicScope_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<GeographicScopeEntity> lst = new List<GeographicScopeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<GeographicScopeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetGeographicScopeCollectionFromGeographicScopeDBList(
				_GeographicScopeDB.GetPageGeographicScopeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  GeographicScopeEntity GetGeographicScopeFromGeographicScopeDB(GeographicScopeEntity o)
		{
	if(o == null)
                return null;
			GeographicScopeEntity ret = new GeographicScopeEntity(o.GeographicScopeId ,o.GeographicScopePersianTitle ,o.GeographicScopeEnglishTitle ,o.Priority ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<GeographicScopeEntity> GetGeographicScopeCollectionFromGeographicScopeDBList( List<GeographicScopeEntity> lst)
		{
			List<GeographicScopeEntity> RetLst = new List<GeographicScopeEntity>();
			foreach(GeographicScopeEntity o in lst)
			{
				RetLst.Add(GetGeographicScopeFromGeographicScopeDB(o));
			}
			return RetLst;
			
		} 
				
		
	}



}



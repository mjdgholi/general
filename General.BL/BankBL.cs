﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/27>
    /// Description: <بانک>
    /// </summary>
    public class BankBL
    {
        private readonly BankDB _BankDB;

        public BankBL()
        {
            _BankDB = new BankDB();
        }

        public void Add(BankEntity BankEntityParam, out Guid BankId)
        {
            _BankDB.AddBankDB(BankEntityParam, out BankId);
        }

        public void Update(BankEntity BankEntityParam)
        {
            _BankDB.UpdateBankDB(BankEntityParam);
        }

        public void Delete(BankEntity BankEntityParam)
        {
            _BankDB.DeleteBankDB(BankEntityParam);
        }

        public BankEntity GetSingleById(BankEntity BankEntityParam)
        {
            BankEntity o = GetBankFromBankDB(
                _BankDB.GetSingleBankDB(BankEntityParam));

            return o;
        }

        public List<BankEntity> GetAll()
        {
            List<BankEntity> lst = new List<BankEntity>();
            //string key = "Bank_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BankEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetBankCollectionFromBankDBList(
                _BankDB.GetAllBankDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<BankEntity> GetAllIsActive()
        {
            List<BankEntity> lst = new List<BankEntity>();
            //string key = "Bank_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BankEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetBankCollectionFromBankDBList(
                _BankDB.GetAllIsActiveBankDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<BankEntity> GetAllPaging(int currentPage, int pageSize,
                                             string

                                                 sortExpression, out int count, string whereClause)
        {
            //string key = "Bank_List_Page_" + currentPage ;
            //string countKey = "Bank_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<BankEntity> lst = new List<BankEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BankEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetBankCollectionFromBankDBList(
                _BankDB.GetPageBankDB(pageSize, currentPage,
                                      whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private BankEntity GetBankFromBankDB(BankEntity o)
        {
            if (o == null)
                return null;
            BankEntity ret = new BankEntity(o.BankId, o.BankTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<BankEntity> GetBankCollectionFromBankDBList(List<BankEntity> lst)
        {
            List<BankEntity> RetLst = new List<BankEntity>();
            foreach (BankEntity o in lst)
            {
                RetLst.Add(GetBankFromBankDB(o));
            }
            return RetLst;

        }


    }
}
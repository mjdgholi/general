﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/07>
    /// Description: <وضیعت تاهل>
    /// </summary>

	public class MarriageStatusBL 
	{	
	  	 private readonly MarriageStatusDB _MarriageStatusDB;					
			
		public MarriageStatusBL()
		{
			_MarriageStatusDB = new MarriageStatusDB();
		}			
	
		public  void Add(MarriageStatusEntity  MarriageStatusEntityParam, out Guid MarriageStatusId)
		{ 
			_MarriageStatusDB.AddMarriageStatusDB(MarriageStatusEntityParam,out MarriageStatusId);			
		}

		public  void Update(MarriageStatusEntity  MarriageStatusEntityParam)
		{
			_MarriageStatusDB.UpdateMarriageStatusDB(MarriageStatusEntityParam);		
		}

		public  void Delete(MarriageStatusEntity  MarriageStatusEntityParam)
		{
			 _MarriageStatusDB.DeleteMarriageStatusDB(MarriageStatusEntityParam);			
		}

		public  MarriageStatusEntity GetSingleById(MarriageStatusEntity  MarriageStatusEntityParam)
		{
			MarriageStatusEntity o = GetMarriageStatusFromMarriageStatusDB(
			_MarriageStatusDB.GetSingleMarriageStatusDB(MarriageStatusEntityParam));
			
			return o;
		}

		public  List<MarriageStatusEntity> GetAll()
		{
			List<MarriageStatusEntity> lst = new List<MarriageStatusEntity>();
			//string key = "MarriageStatus_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<MarriageStatusEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetMarriageStatusCollectionFromMarriageStatusDBList(
				_MarriageStatusDB.GetAllMarriageStatusDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<MarriageStatusEntity> GetAllIsActive()
        {
            List<MarriageStatusEntity> lst = new List<MarriageStatusEntity>();
            //string key = "MarriageStatus_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MarriageStatusEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetMarriageStatusCollectionFromMarriageStatusDBList(
            _MarriageStatusDB.GetAllMarriageStatusIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		
		public  List<MarriageStatusEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "MarriageStatus_List_Page_" + currentPage ;
			//string countKey = "MarriageStatus_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<MarriageStatusEntity> lst = new List<MarriageStatusEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<MarriageStatusEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetMarriageStatusCollectionFromMarriageStatusDBList(
				_MarriageStatusDB.GetPageMarriageStatusDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  MarriageStatusEntity GetMarriageStatusFromMarriageStatusDB(MarriageStatusEntity o)
		{
	if(o == null)
                return null;
			MarriageStatusEntity ret = new MarriageStatusEntity(o.MarriageStatusId ,o.MarriageStatusTitle ,o.IsMarried ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<MarriageStatusEntity> GetMarriageStatusCollectionFromMarriageStatusDBList( List<MarriageStatusEntity> lst)
		{
			List<MarriageStatusEntity> RetLst = new List<MarriageStatusEntity>();
			foreach(MarriageStatusEntity o in lst)
			{
				RetLst.Add(GetMarriageStatusFromMarriageStatusDB(o));
			}
			return RetLst;
			
		} 
				
		
	}


}



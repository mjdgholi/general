﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/06>
    // Description:	<فرم گروه خونی>
    /// </summary>
    public class BloodGroupBL
    {
        private readonly BloodGroupDB _BloodGroupDB;

        public BloodGroupBL()
        {
            _BloodGroupDB = new BloodGroupDB();
        }

        public void Add(BloodGroupEntity BloodGroupEntityParam, out Guid BloodGroupId)
        {
            _BloodGroupDB.AddBloodGroupDB(BloodGroupEntityParam, out BloodGroupId);
        }

        public void Update(BloodGroupEntity BloodGroupEntityParam)
        {
            _BloodGroupDB.UpdateBloodGroupDB(BloodGroupEntityParam);
        }

        public void Delete(BloodGroupEntity BloodGroupEntityParam)
        {
            _BloodGroupDB.DeleteBloodGroupDB(BloodGroupEntityParam);
        }

        public BloodGroupEntity GetSingleById(BloodGroupEntity BloodGroupEntityParam)
        {
            BloodGroupEntity o = GetBloodGroupFromBloodGroupDB(
                _BloodGroupDB.GetSingleBloodGroupDB(BloodGroupEntityParam));

            return o;
        }

        public List<BloodGroupEntity> GetAll()
        {
            List<BloodGroupEntity> lst = new List<BloodGroupEntity>();
            //string key = "BloodGroup_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BloodGroupEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetBloodGroupCollectionFromBloodGroupDBList(
                _BloodGroupDB.GetAllBloodGroupDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<BloodGroupEntity> GetAllIsActive()
        {
            List<BloodGroupEntity> lst = new List<BloodGroupEntity>();
            //string key = "BloodGroup_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BloodGroupEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetBloodGroupCollectionFromBloodGroupDBList(
                _BloodGroupDB.GetAllIsActiveBloodGroupDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<BloodGroupEntity> GetAllPaging(int currentPage, int pageSize,
                                                   string

                                                       sortExpression, out int count, string whereClause)
        {
            //string key = "BloodGroup_List_Page_" + currentPage ;
            //string countKey = "BloodGroup_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<BloodGroupEntity> lst = new List<BloodGroupEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<BloodGroupEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetBloodGroupCollectionFromBloodGroupDBList(
                _BloodGroupDB.GetPageBloodGroupDB(pageSize, currentPage,
                                                  whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private BloodGroupEntity GetBloodGroupFromBloodGroupDB(BloodGroupEntity o)
        {
            if (o == null)
                return null;
            BloodGroupEntity ret = new BloodGroupEntity(o.BloodGroupId, o.BloodGroupTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<BloodGroupEntity> GetBloodGroupCollectionFromBloodGroupDBList(List<BloodGroupEntity> lst)
        {
            List<BloodGroupEntity> RetLst = new List<BloodGroupEntity>();
            foreach (BloodGroupEntity o in lst)
            {
                RetLst.Add(GetBloodGroupFromBloodGroupDB(o));
            }
            return RetLst;

        }
    }
}
﻿using System.Collections.Generic;
using GeneralProject.General.DAL;
using GeneralProject.General.Entity;
using Intranet.Configuration;

namespace GeneralProject.General.BL
{

         
	public class ModuleArrangementBL : DeskTopObj
	{	
	  	 private readonly ModuleArrangementDB _ModuleArrangementDB;					
			
		public ModuleArrangementBL()
		{
			_ModuleArrangementDB = new ModuleArrangementDB();
		}			
	
		public  void Add(ModuleArrangementEntity  ModuleArrangementEntityParam,out int ModuleArrangementId)
		{ 
			_ModuleArrangementDB.AddModuleArrangementDB(ModuleArrangementEntityParam,out ModuleArrangementId);			
		}

		public  void Update(ModuleArrangementEntity  ModuleArrangementEntityParam)
		{
			_ModuleArrangementDB.UpdateModuleArrangementDB(ModuleArrangementEntityParam);		
		}

		public  void Delete(ModuleArrangementEntity  ModuleArrangementEntityParam)
		{
			 _ModuleArrangementDB.DeleteModuleArrangementDB(ModuleArrangementEntityParam);			
		}

		public  ModuleArrangementEntity GetSingleById(ModuleArrangementEntity  ModuleArrangementEntityParam)
		{
			ModuleArrangementEntity o = GetModuleArrangementFromModuleArrangementDB(
			_ModuleArrangementDB.GetSingleModuleArrangementDB(ModuleArrangementEntityParam));
			
			return o;
		}

		public  List<ModuleArrangementEntity> GetAll()
		{
			List<ModuleArrangementEntity> lst = new List<ModuleArrangementEntity>();
			//string key = "ModuleArrangement_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ModuleArrangementEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetModuleArrangementCollectionFromModuleArrangementDBList(
				_ModuleArrangementDB.GetAllModuleArrangementDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<ModuleArrangementEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "ModuleArrangement_List_Page_" + currentPage ;
			//string countKey = "ModuleArrangement_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<ModuleArrangementEntity> lst = new List<ModuleArrangementEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ModuleArrangementEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetModuleArrangementCollectionFromModuleArrangementDBList(
				_ModuleArrangementDB.GetPageModuleArrangementDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  ModuleArrangementEntity GetModuleArrangementFromModuleArrangementDB(ModuleArrangementEntity o)
		{
	if(o == null)
                return null;
			ModuleArrangementEntity ret = new ModuleArrangementEntity(o.ModuleArrangementId ,o.ModuleId ,o.MainPicPath ,o.MouseOverPicPath ,o.PortalUrl ,o.ModuleToolTip ,o.ModuleOrder ,o.IsAllUser ,o.IsKnownUser ,o.IsActive ,o.CreationUserId ,o.ModificationUserId ,o.CreationDate ,o.ModificationDate ,o.IsInternal ,o.OwnerId );
			return ret;
		}
		
		private  List<ModuleArrangementEntity> GetModuleArrangementCollectionFromModuleArrangementDBList( List<ModuleArrangementEntity> lst)
		{
			List<ModuleArrangementEntity> RetLst = new List<ModuleArrangementEntity>();
			foreach(ModuleArrangementEntity o in lst)
			{
				RetLst.Add(GetModuleArrangementFromModuleArrangementDB(o));
			}
			return RetLst;
			
		}


        public List<ModuleArrangementEntity> GetSingleByOwnerId(ModuleArrangementEntity moduleArrangementEntityParam)
        {
            List<ModuleArrangementEntity> lst = new List<ModuleArrangementEntity>();
            //string key = "ModuleArrangement_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ModuleArrangementEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetModuleArrangementCollectionFromModuleArrangementDBList(
            _ModuleArrangementDB.GetSingleModuleArrangementByOwnerIdDB(moduleArrangementEntityParam));
            return lst;
        }
	}



}

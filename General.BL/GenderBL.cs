﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.DAL;
using Intranet.DesktopModules.GeneralProject.General.Entity;


namespace Intranet.DesktopModules.GeneralProject.General.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/06>
    // Description:	<جنسیت>
    /// </summary>
    public class GenderBL
    {
        private readonly GenderDB _GenderDB;

        public GenderBL()
        {
            _GenderDB = new GenderDB();
        }

        public void Add(GenderEntity GenderEntityParam, out Guid GenderId)
        {
            _GenderDB.AddGenderDB(GenderEntityParam, out GenderId);
        }

        public void Update(GenderEntity GenderEntityParam)
        {
            _GenderDB.UpdateGenderDB(GenderEntityParam);
        }

        public void Delete(GenderEntity GenderEntityParam)
        {
            _GenderDB.DeleteGenderDB(GenderEntityParam);
        }

        public GenderEntity GetSingleById(GenderEntity GenderEntityParam)
        {
            GenderEntity o = GetGenderFromGenderDB(
                _GenderDB.GetSingleGenderDB(GenderEntityParam));

            return o;
        }

        public List<GenderEntity> GetAll()
        {
            List<GenderEntity> lst = new List<GenderEntity>();
            //string key = "Gender_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<GenderEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetGenderCollectionFromGenderDBList(
                _GenderDB.GetAllGenderDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<GenderEntity> GetAllIsActive()
        {
            List<GenderEntity> lst = new List<GenderEntity>();
            //string key = "Gender_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<GenderEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetGenderCollectionFromGenderDBList(
                _GenderDB.GetAllIsActiveGenderDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<GenderEntity> GetAllPaging(int currentPage, int pageSize, string sortExpression, out int count, string whereClause)
        {
            //string key = "Gender_List_Page_" + currentPage ;
            //string countKey = "Gender_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<GenderEntity> lst = new List<GenderEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<GenderEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetGenderCollectionFromGenderDBList(_GenderDB.GetPageGenderDB(pageSize, currentPage, whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private GenderEntity GetGenderFromGenderDB(GenderEntity o)
        {
            if (o == null)
                return null;
            GenderEntity ret = new GenderEntity(o.GenderId, o.GenderTitle, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<GenderEntity> GetGenderCollectionFromGenderDBList(List<GenderEntity> lst)
        {
            List<GenderEntity> RetLst = new List<GenderEntity>();
            foreach (GenderEntity o in lst)
            {
                RetLst.Add(GetGenderFromGenderDB(o));
            }
            return RetLst;

        }
    }
}
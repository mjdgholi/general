﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;


namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/06>
    // Description:	<فرم گروه خونی>
    /// </summary>

    #region Class "BloodGroupDB"

    public class BloodGroupDB
    {
        #region Methods :

        public void AddBloodGroupDB(BloodGroupEntity BloodGroupEntityParam, out Guid BloodGroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BloodGroupId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@BloodGroupTitle", SqlDbType.NVarChar, 3),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(BloodGroupEntityParam.BloodGroupTitle.Trim());
            parameters[2].Value = BloodGroupEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_BloodGroupAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            BloodGroupId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateBloodGroupDB(BloodGroupEntity BloodGroupEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BloodGroupId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@BloodGroupTitle", SqlDbType.NVarChar, 3),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = BloodGroupEntityParam.BloodGroupId;
            parameters[1].Value = FarsiToArabic.ToArabic(BloodGroupEntityParam.BloodGroupTitle.Trim());
            parameters[2].Value = BloodGroupEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_BloodGroupUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteBloodGroupDB(BloodGroupEntity BloodGroupEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BloodGroupId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = BloodGroupEntityParam.BloodGroupId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_BloodGroupDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public BloodGroupEntity GetSingleBloodGroupDB(BloodGroupEntity BloodGroupEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@BloodGroupId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = BloodGroupEntityParam.BloodGroupId;

                reader = _intranetDB.RunProcedureReader("General.p_BloodGroupGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetBloodGroupDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<BloodGroupEntity> GetAllBloodGroupDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetBloodGroupDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("General.p_BloodGroupGetAll", new IDataParameter[] {}));
        }

        public List<BloodGroupEntity> GetAllIsActiveBloodGroupDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetBloodGroupDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("General.p_BloodGroupGetAllIsActive", new IDataParameter[] {}));
        }

        public List<BloodGroupEntity> GetPageBloodGroupDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "General.t_BloodGroup";
            parameters[5].Value = "BloodGroupId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetBloodGroupDBCollectionFromDataSet(ds, out count);
        }

        public BloodGroupEntity GetBloodGroupDBFromDataReader(IDataReader reader)
        {
            return new BloodGroupEntity(new Guid(reader["BloodGroupId"].ToString()),
                                        reader["BloodGroupTitle"].ToString(),
                                        reader["CreationDate"].ToString(),
                                        reader["ModificationDate"].ToString(),
                                        bool.Parse(reader["IsActive"].ToString()));
        }

        public List<BloodGroupEntity> GetBloodGroupDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<BloodGroupEntity> lst = new List<BloodGroupEntity>();
                while (reader.Read())
                    lst.Add(GetBloodGroupDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<BloodGroupEntity> GetBloodGroupDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetBloodGroupDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion
    }

    #endregion
}
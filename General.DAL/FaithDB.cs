﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;


namespace Intranet.DesktopModules.GeneralProject.General.DAL
{

    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <دین>
    /// </summary>
    

    #region Class "FaithDB"

    public class FaithDB
    {

        #endregion

        #region Methods :

        public void AddFaithDB(FaithEntity faithEntityParam, out Guid FaithId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@FaithId", SqlDbType.UniqueIdentifier),
                                              new SqlParameter("@FaithTitle", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@IsActive", SqlDbType.Bit),
                                              new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                          };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(faithEntityParam.FaithTitle.Trim());
            parameters[2].Value = faithEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_FaithAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            FaithId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateFaithDB(FaithEntity faithEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@FaithId", SqlDbType.UniqueIdentifier),
                                              new SqlParameter("@FaithTitle", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@IsActive", SqlDbType.Bit),
                                              new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                          };

            parameters[0].Value = faithEntityParam.FaithId;
            parameters[1].Value = FarsiToArabic.ToArabic(faithEntityParam.FaithTitle.Trim());
            parameters[2].Value = faithEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_FaithUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteFaithDB(FaithEntity FaithEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@FaithId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = FaithEntityParam.FaithId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_FaithDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public FaithEntity GetSingleFaithDB(FaithEntity FaithEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@FaithId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = FaithEntityParam.FaithId;

                reader = _intranetDB.RunProcedureReader("General.p_FaithGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetFaithDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<FaithEntity> GetAllFaithDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetFaithDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("General.p_FaithGetAll", new IDataParameter[] {}));
        }

        public List<FaithEntity> GetAllFaithIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetFaithDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("General.p_FaithGetAllIsActive", new IDataParameter[] { }));
        }

        public List<FaithEntity> GetPageFaithDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Faith";
            parameters[5].Value = "FaithId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetFaithDBCollectionFromDataSet(ds, out count);
        }

        public FaithEntity GetFaithDBFromDataReader(IDataReader reader)
        {
            return new FaithEntity(new Guid(reader["FaithId"].ToString()),
                                   reader["FaithTitle"].ToString(),
                                   reader["CreationDate"].ToString(),
                                   reader["ModificationDate"].ToString(),
                                   bool.Parse(reader["IsActive"].ToString()));
        }

        public List<FaithEntity> GetFaithDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<FaithEntity> lst = new List<FaithEntity>();
                while (reader.Read())
                    lst.Add(GetFaithDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<FaithEntity> GetFaithDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetFaithDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion

    }
}
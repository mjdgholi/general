﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;


namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/06>
    /// Description: <شهر>
    /// </summary>


    public class CityDB
    {
   #region Methods :
			
		public void AddCityDB(CityEntity  CityEntityParam,out Guid CityId)
		{
			IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
			SqlParameter[] parameters = 	{
								 new SqlParameter("@CityId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ProvinceId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@GeographicScopeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@CityTitlePersian", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@CityTitleEnglish", SqlDbType.NVarChar,100) ,
											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
			parameters[0].Direction = ParameterDirection.Output;
			parameters[1].Value = CityEntityParam.ProvinceId;
parameters[2].Value = CityEntityParam.GeographicScopeId;
parameters[3].Value = FarsiToArabic.ToArabic(CityEntityParam.CityTitlePersian.Trim());
parameters[4].Value = FarsiToArabic.ToArabic(CityEntityParam.CityTitleEnglish.Trim());
parameters[5].Value = CityEntityParam.IsActive;

			parameters[6].Direction = ParameterDirection.Output;						
			_intranetDB.RunProcedure("General.p_CityAdd", parameters);
			var messageError = parameters[6].Value.ToString();
          			if (messageError != "")
        			throw (new Exception(messageError));
           			CityId = new Guid(parameters[0].Value.ToString());
		}

		public void UpdateCityDB(CityEntity  CityEntityParam)
		{
			IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
			SqlParameter[] parameters = {
			                            	new SqlParameter("@CityId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ProvinceId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@GeographicScopeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@CityTitlePersian", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@CityTitleEnglish", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };
										
			parameters[0].Value = CityEntityParam.CityId;			
			parameters[1].Value = CityEntityParam.ProvinceId;
parameters[2].Value = CityEntityParam.GeographicScopeId;
parameters[3].Value = FarsiToArabic.ToArabic(CityEntityParam.CityTitlePersian.Trim());
parameters[4].Value = FarsiToArabic.ToArabic(CityEntityParam.CityTitleEnglish.Trim());
parameters[5].Value = CityEntityParam.IsActive;

			parameters[6].Direction = ParameterDirection.Output;
			
			_intranetDB.RunProcedure("General.p_CityUpdate", parameters);
			var messageError = parameters[6].Value.ToString();
        			if (messageError != "")
    		             throw (new Exception(messageError));
		}

		public void DeleteCityDB(CityEntity  CityEntityParam )
		{
			IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
			SqlParameter[] parameters = {
		                            		new SqlParameter("@CityId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
			parameters[0].Value = CityEntityParam.CityId;
			parameters[1].Direction = ParameterDirection.Output;
			_intranetDB.RunProcedure("General.p_CityDel", parameters);
			var messageError = parameters[1].Value.ToString();
      			if (messageError != "")
              		 throw (new Exception(messageError));
		}

		public CityEntity GetSingleCityDB(CityEntity  CityEntityParam )
		{
		   IDataReader reader = null;
          		   try
         		        {

               		 IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
           			 IDataParameter[] parameters = {
													new SqlParameter("@CityId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
					parameters[0].Value = CityEntityParam.CityId;
					
           			 reader = _intranetDB.RunProcedureReader("General.p_CityGetSingle", parameters);
              	              if (reader != null)
				if(reader.Read())
					return GetCityDBFromDataReader(reader);
              	  return null;
         		 }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }										
		}

		public  List<CityEntity> GetAllCityDB()
		{
			IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
			return GetCityDBCollectionFromDataReader(
						_intranetDB.RunProcedureReader
						("General.p_CityGetAll", new IDataParameter[] {}));
		}

        public List<CityEntity> GetAllIsActiveCityDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetCityDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_CityIsActiveGetAll", new IDataParameter[] { }));
        }

		public  List<CityEntity> GetPageCityDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy,out int count)
		{
			IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
			IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
			parameters[0].Value = PageSize;
			parameters[1].Value = CurrentPage;
			parameters[2].Value = WhereClause;
			parameters[3].Value = OrderBy;
			parameters[4].Value = "t_City";
			parameters[5].Value = "CityId";
			DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
			return GetCityDBCollectionFromDataSet(ds, out count);
		}
		
		public  CityEntity GetCityDBFromDataReader(IDataReader reader)
		{
			return new CityEntity(new Guid(reader["CityId"].ToString()), 
                                    Guid.Parse(reader["ProvinceId"].ToString()),
                                    Guid.Parse(reader["GeographicScopeId"].ToString()),
                                    reader["CityTitlePersian"].ToString(),
                                    reader["CityTitleEnglish"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
		}
		
		public List<CityEntity> GetCityDBCollectionFromDataReader(IDataReader reader)
		{
			try
            			{
			      List<CityEntity> lst = new List<CityEntity>();
     			       while(reader.Read())
        				        lst.Add(GetCityDBFromDataReader(reader));
      			  return lst;
			 }
           	                   	  finally
           			 {
           			       if (!reader.IsClosed)
               		       reader.Close();
        		              }
		}
		
		public List<CityEntity> GetCityDBCollectionFromDataSet(DataSet ds, out int count)
		{
			count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
			return  GetCityDBCollectionFromDataReader(ds.CreateDataReader());
		}

        public List<CityEntity> GetCityDBCollectionByGeographicScopeDB(CityEntity CityEntityParam)
{
	IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
	IDataParameter parameter = new SqlParameter("@GeographicScopeId", SqlDbType.UniqueIdentifier);
	parameter.Value = CityEntityParam.GeographicScopeId;
	return GetCityDBCollectionFromDataReader(_intranetDB.RunProcedureReader("General.p_CityGetByGeographicScope", new[] {parameter}));
}

        public List<CityEntity> GetCityDBCollectionByProvinceDB(CityEntity CityEntityParam)
{
	IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
	IDataParameter parameter = new SqlParameter("@ProvinceId", SqlDbType.UniqueIdentifier);
	parameter.Value = CityEntityParam.ProvinceId;
	return GetCityDBCollectionFromDataReader(_intranetDB.RunProcedureReader("General.p_CityGetByProvince", new[] {parameter}));
}
}
#endregion
    }

 
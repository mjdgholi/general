﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;


namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/06>
    // Description:	<فرم شناسه حرفی شناسنامه>
    /// </summary>

    #region Class "CertificateAlphabetDB"

    public class CertificateAlphabetDB
    {
        #region Methods :

        public void AddCertificateAlphabetDB(CertificateAlphabetEntity CertificateAlphabetEntityParam, out Guid CertificateAlphabetId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@CertificateAlphabetId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@CertificateAlphabetTitle", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(CertificateAlphabetEntityParam.CertificateAlphabetTitle.Trim());
            parameters[2].Value = CertificateAlphabetEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_CertificateAlphabetAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            CertificateAlphabetId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateCertificateAlphabetDB(CertificateAlphabetEntity CertificateAlphabetEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@CertificateAlphabetId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@CertificateAlphabetTitle", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = CertificateAlphabetEntityParam.CertificateAlphabetId;
            parameters[1].Value = FarsiToArabic.ToArabic(CertificateAlphabetEntityParam.CertificateAlphabetTitle.Trim());
            parameters[2].Value = CertificateAlphabetEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_CertificateAlphabetUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteCertificateAlphabetDB(CertificateAlphabetEntity CertificateAlphabetEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@CertificateAlphabetId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = CertificateAlphabetEntityParam.CertificateAlphabetId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_CertificateAlphabetDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public CertificateAlphabetEntity GetSingleCertificateAlphabetDB(CertificateAlphabetEntity CertificateAlphabetEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@CertificateAlphabetId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = CertificateAlphabetEntityParam.CertificateAlphabetId;

                reader = _intranetDB.RunProcedureReader("General.p_CertificateAlphabetGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetCertificateAlphabetDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<CertificateAlphabetEntity> GetAllCertificateAlphabetDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetCertificateAlphabetDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("General.p_CertificateAlphabetGetAll", new IDataParameter[] {}));
        }

        public List<CertificateAlphabetEntity> GetAllIsActiveCertificateAlphabetDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetCertificateAlphabetDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("General.p_CertificateAlphabetGetAllIsActive", new IDataParameter[] { }));
        }      

        public List<CertificateAlphabetEntity> GetPageCertificateAlphabetDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "General.t_CertificateAlphabet";
            parameters[5].Value = "CertificateAlphabetId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetCertificateAlphabetDBCollectionFromDataSet(ds, out count);
        }

        public CertificateAlphabetEntity GetCertificateAlphabetDBFromDataReader(IDataReader reader)
        {
            return new CertificateAlphabetEntity(new Guid(reader["CertificateAlphabetId"].ToString()),
                                                 reader["CertificateAlphabetTitle"].ToString(),
                                                 reader["CreationDate"].ToString(),
                                                 reader["ModificationDate"].ToString(),
                                                 bool.Parse(reader["IsActive"].ToString()));
        }

        public List<CertificateAlphabetEntity> GetCertificateAlphabetDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<CertificateAlphabetEntity> lst = new List<CertificateAlphabetEntity>();
                while (reader.Read())
                    lst.Add(GetCertificateAlphabetDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<CertificateAlphabetEntity> GetCertificateAlphabetDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetCertificateAlphabetDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion
    }

    #endregion
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;

namespace Intranet.DesktopModules.GeneralProject.GeneralProject.General.DAL
{

    /// <summary>
    /// متدهای عمومی سامانه دسترسی ها
    /// مجید قلی بیگیان
    /// </summary>
    public class AccessSystemGeneralDB
    {
        // چک میکند که آیا کاربر عضو گروهی است که تنها می تواند اطلاعات خود را ویرایش نماید
        public bool IsOnlyOwnInfoUser(int userId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@UserId", SqlDbType.Int),                
            };
            parameters[0].Value = userId;
            var ret = _intranetDB.RunProcedureDS("acs.p_CheckUserIsOnlyOwnInfo", parameters).Tables[0].Rows[0][0].ToString();
            return Convert.ToBoolean(Convert.ToInt16(ret));
        }
    }
}

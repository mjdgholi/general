﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using GeneralProject.General.Entity;
using Intranet.Common;

namespace GeneralProject.General.DAL
{

    //-----------------------------------------------------
    #region Class "ModuleArrangementDB"

    public class ModuleArrangementDB
    {


        #region Methods :

        public void AddModuleArrangementDB(ModuleArrangementEntity moduleArrangementEntityParam, out int ModuleArrangementId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ModuleArrangementId", SqlDbType.Int, 4),
								 new SqlParameter("@ModuleId", SqlDbType.Int) ,
											  new SqlParameter("@MainPicPath", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@MouseOverPicPath", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@PortalUrl", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@ModuleToolTip", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@ModuleOrder", SqlDbType.Int) ,
											  new SqlParameter("@IsAllUser", SqlDbType.Bit) ,
											  new SqlParameter("@IsKnownUser", SqlDbType.Bit) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
											  new SqlParameter("@CreationUserId", SqlDbType.Int) ,
											  new SqlParameter("@ModificationUserId", SqlDbType.Int) ,
											  new SqlParameter("@CreationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@IsInternal", SqlDbType.Bit) ,
											  new SqlParameter("@OwnerId", SqlDbType.Int) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = moduleArrangementEntityParam.ModuleId;
            parameters[2].Value = moduleArrangementEntityParam.MainPicPath;
            parameters[3].Value = moduleArrangementEntityParam.MouseOverPicPath;
            parameters[4].Value = moduleArrangementEntityParam.PortalUrl;
            parameters[5].Value = moduleArrangementEntityParam.ModuleToolTip;
            parameters[6].Value = moduleArrangementEntityParam.ModuleOrder;
            parameters[7].Value = moduleArrangementEntityParam.IsAllUser;
            parameters[8].Value = moduleArrangementEntityParam.IsKnownUser;
            parameters[9].Value = moduleArrangementEntityParam.IsActive;
            parameters[10].Value = moduleArrangementEntityParam.CreationUserId;
            parameters[11].Value = moduleArrangementEntityParam.ModificationUserId;
            parameters[12].Value = moduleArrangementEntityParam.CreationDate;
            parameters[13].Value = moduleArrangementEntityParam.ModificationDate;
            parameters[14].Value = moduleArrangementEntityParam.IsInternal;
            parameters[15].Value = moduleArrangementEntityParam.OwnerId;

            parameters[100].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_ModuleArrangementAdd", parameters);
            var messageError = parameters[100].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ModuleArrangementId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateModuleArrangementDB(ModuleArrangementEntity moduleArrangementEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ModuleArrangementId", SqlDbType.Int, 4),
						 new SqlParameter("@ModuleId", SqlDbType.Int) ,
											  new SqlParameter("@MainPicPath", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@MouseOverPicPath", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@PortalUrl", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@ModuleToolTip", SqlDbType.NVarChar,300) ,
											  new SqlParameter("@ModuleOrder", SqlDbType.Int) ,
											  new SqlParameter("@IsAllUser", SqlDbType.Bit) ,
											  new SqlParameter("@IsKnownUser", SqlDbType.Bit) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
											  new SqlParameter("@CreationUserId", SqlDbType.Int) ,
											  new SqlParameter("@ModificationUserId", SqlDbType.Int) ,
											  new SqlParameter("@CreationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@ModificationDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@IsInternal", SqlDbType.Bit) ,
											  new SqlParameter("@OwnerId", SqlDbType.Int) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = moduleArrangementEntityParam.ModuleArrangementId;
            parameters[1].Value = moduleArrangementEntityParam.ModuleId;
            parameters[2].Value = moduleArrangementEntityParam.MainPicPath;
            parameters[3].Value = moduleArrangementEntityParam.MouseOverPicPath;
            parameters[4].Value = moduleArrangementEntityParam.PortalUrl;
            parameters[5].Value = moduleArrangementEntityParam.ModuleToolTip;
            parameters[6].Value = moduleArrangementEntityParam.ModuleOrder;
            parameters[7].Value = moduleArrangementEntityParam.IsAllUser;
            parameters[8].Value = moduleArrangementEntityParam.IsKnownUser;
            parameters[9].Value = moduleArrangementEntityParam.IsActive;
            parameters[10].Value = moduleArrangementEntityParam.CreationUserId;
            parameters[11].Value = moduleArrangementEntityParam.ModificationUserId;
            parameters[12].Value = moduleArrangementEntityParam.CreationDate;
            parameters[13].Value = moduleArrangementEntityParam.ModificationDate;
            parameters[14].Value = moduleArrangementEntityParam.IsInternal;
            parameters[15].Value = moduleArrangementEntityParam.OwnerId;

            parameters[100].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_ModuleArrangementUpdate", parameters);
            var messageError = parameters[100].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteModuleArrangementDB(ModuleArrangementEntity ModuleArrangementEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ModuleArrangementId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ModuleArrangementEntityParam.ModuleArrangementId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_ModuleArrangementDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ModuleArrangementEntity GetSingleModuleArrangementDB(ModuleArrangementEntity ModuleArrangementEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@ModuleArrangementId", DataTypes.integer, 4, 										ModuleArrangementEntityParam.ModuleArrangementId)
						      };

                reader = _intranetDB.RunProcedureReader("General.p_ModuleArrangementGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetModuleArrangementDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ModuleArrangementEntity> GetAllModuleArrangementDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetModuleArrangementDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_ModuleArrangementGetAll", new IDataParameter[] { }));
        }

        public List<ModuleArrangementEntity> GetPageModuleArrangementDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ModuleArrangement";
            parameters[5].Value = "ModuleArrangementId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetModuleArrangementDBCollectionFromDataSet(ds, out count);
        }

        public ModuleArrangementEntity GetModuleArrangementDBFromDataReader(IDataReader reader)
        {
            return new ModuleArrangementEntity(int.Parse(reader["ModuleArrangementId"].ToString()),
                                    int.Parse(reader["ModuleId"].ToString()),
                                    reader["MainPicPath"].ToString(),
                                    reader["MouseOverPicPath"].ToString(),
                                    reader["PortalUrl"].ToString(),
                                    reader["ModuleToolTip"].ToString(),
                                    int.Parse(reader["ModuleOrder"].ToString()),
                                    bool.Parse(reader["IsAllUser"].ToString()),
                                    bool.Parse(reader["IsKnownUser"].ToString()),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    int.Parse(reader["CreationUserId"].ToString()),
                                    int.Parse(reader["ModificationUserId"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsInternal"].ToString()),
                                            (Convert.IsDBNull(reader["OwnerId"]) ? null : (int?)reader["OwnerId"]));
        }

        public List<ModuleArrangementEntity> GetModuleArrangementDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ModuleArrangementEntity> lst = new List<ModuleArrangementEntity>();
                while (reader.Read())
                    lst.Add(GetModuleArrangementDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ModuleArrangementEntity> GetModuleArrangementDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetModuleArrangementDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

        public List<ModuleArrangementEntity> GetSingleModuleArrangementByOwnerIdDB(ModuleArrangementEntity moduleArrangementEntityParam)
        {
     

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
		                            		new SqlParameter("@OwnerId", SqlDbType.Int, 4),
						
						  };
                parameters[0].Value = (moduleArrangementEntityParam.OwnerId==null?System.DBNull.Value:(object)moduleArrangementEntityParam.OwnerId);
                
                return GetModuleArrangementDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader("General.p_ModuleArrangementByOwnerIdGetSingle", parameters));

        }
    }

    #endregion
}
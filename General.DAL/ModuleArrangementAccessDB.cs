﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using GeneralProject.General.Entity;
using Intranet.Common;

namespace GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/04>
    /// Description: <دسترسی به آیکن منو>
    /// </summary>

    public class ModuleArrangementAccessDB
    {



        #region Methods :

        public void AddModuleArrangementAccessDB(ModuleArrangementAccessEntity moduleArrangementAccessEntityParam, out int ModuleArrangementAccessId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ModuleArrangementAccessId", SqlDbType.Int, 4),
								 new SqlParameter("@UserId", SqlDbType.Int) ,
											  new SqlParameter("@ModuleArrangementId", SqlDbType.Int) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = moduleArrangementAccessEntityParam.UserId;
            parameters[2].Value = moduleArrangementAccessEntityParam.ModuleArrangementId;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_ModuleArrangementAccessAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ModuleArrangementAccessId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateModuleArrangementAccessDB(ModuleArrangementAccessEntity moduleArrangementAccessEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ModuleArrangementAccessId", SqlDbType.Int, 4),
						 new SqlParameter("@UserId", SqlDbType.Int) ,
											  new SqlParameter("@ModuleArrangementId", SqlDbType.Int) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = moduleArrangementAccessEntityParam.ModuleArrangementAccessId;
            parameters[1].Value = moduleArrangementAccessEntityParam.UserId;
            parameters[2].Value = moduleArrangementAccessEntityParam.ModuleArrangementId;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_ModuleArrangementAccessUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteModuleArrangementAccessDB(ModuleArrangementAccessEntity ModuleArrangementAccessEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ModuleArrangementAccessId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ModuleArrangementAccessEntityParam.ModuleArrangementAccessId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_ModuleArrangementAccessDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ModuleArrangementAccessEntity GetSingleModuleArrangementAccessDB(ModuleArrangementAccessEntity ModuleArrangementAccessEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@ModuleArrangementAccessId", DataTypes.integer, 4,ModuleArrangementAccessEntityParam.ModuleArrangementAccessId)
						      };

                reader = _intranetDB.RunProcedureReader("General.p_ModuleArrangementAccessGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetModuleArrangementAccessDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ModuleArrangementAccessEntity> GetAllModuleArrangementAccessDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetModuleArrangementAccessDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_ModuleArrangementAccessGetAll", new IDataParameter[] { }));
        }

        public List<ModuleArrangementAccessEntity> GetPageModuleArrangementAccessDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ModuleArrangementAccess";
            parameters[5].Value = "ModuleArrangementAccessId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetModuleArrangementAccessDBCollectionFromDataSet(ds, out count);
        }

        public ModuleArrangementAccessEntity GetModuleArrangementAccessDBFromDataReader(IDataReader reader)
        {
            return new ModuleArrangementAccessEntity(int.Parse(reader["ModuleArrangementAccessId"].ToString()),
                                    int.Parse(reader["UserId"].ToString()),
                                    int.Parse(reader["ModuleArrangementId"].ToString()),
                                    int.Parse(reader["ModuleArrangementOwnerId"].ToString()));
        }

        public List<ModuleArrangementAccessEntity> GetModuleArrangementAccessDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ModuleArrangementAccessEntity> lst = new List<ModuleArrangementAccessEntity>();
                while (reader.Read())
                    lst.Add(GetModuleArrangementAccessDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ModuleArrangementAccessEntity> GetModuleArrangementAccessDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetModuleArrangementAccessDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }


}

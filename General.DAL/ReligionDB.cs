﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;

namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/08>
    /// Description: <مذهب>
    /// </summary>


    public class ReligionDB
    {
        #region Methods :

        public void AddReligionDB(ReligionEntity ReligionEntityParam, out Guid ReligionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ReligionId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ReligionTitle", SqlDbType.NVarChar,100) ,											  
								 new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value =  FarsiToArabic.ToArabic(ReligionEntityParam.ReligionTitle.Trim());            
            parameters[2].Value = ReligionEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_ReligionAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ReligionId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateReligionDB(ReligionEntity ReligionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ReligionId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ReligionTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = ReligionEntityParam.ReligionId;
            parameters[1].Value = FarsiToArabic.ToArabic(ReligionEntityParam.ReligionTitle.Trim());  
            parameters[2].Value = ReligionEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_ReligionUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteReligionDB(ReligionEntity ReligionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ReligionId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ReligionEntityParam.ReligionId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_ReligionDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ReligionEntity GetSingleReligionDB(ReligionEntity ReligionEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ReligionId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ReligionEntityParam.ReligionId;

                reader = _intranetDB.RunProcedureReader("General.p_ReligionGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetReligionDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ReligionEntity> GetAllReligionDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetReligionDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_ReligionGetAll", new IDataParameter[] { }));
        }
        public List<ReligionEntity> GetAllReligionIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetReligionDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_ReligionIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<ReligionEntity> GetPageReligionDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Religion";
            parameters[5].Value = "ReligionId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetReligionDBCollectionFromDataSet(ds, out count);
        }

        public ReligionEntity GetReligionDBFromDataReader(IDataReader reader)
        {
            return new ReligionEntity(Guid.Parse(reader["ReligionId"].ToString()),
                                    reader["ReligionTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ReligionEntity> GetReligionDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ReligionEntity> lst = new List<ReligionEntity>();
                while (reader.Read())
                    lst.Add(GetReligionDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ReligionEntity> GetReligionDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetReligionDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }


}

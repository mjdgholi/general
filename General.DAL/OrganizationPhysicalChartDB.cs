﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;


namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/23>
    /// Description: <واحد سازمانی>
    /// </summary>

    //-----------------------------------------------------
    #region Class "OrganizationPhysicalChartDB"

    public class OrganizationPhysicalChartDB
    {



        #region Methods :

        public void AddOrganizationPhysicalChartDB(OrganizationPhysicalChartEntity OrganizationPhysicalChartEntityParam, out Guid OrganizationPhysicalChartId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@OwnerId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OrganizationPhysicalChartTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = (OrganizationPhysicalChartEntityParam.OwnerId == null ? System.DBNull.Value : (object)OrganizationPhysicalChartEntityParam.OwnerId);
            parameters[2].Value = FarsiToArabic.ToArabic(OrganizationPhysicalChartEntityParam.OrganizationPhysicalChartTitle.Trim());
            parameters[3].Value = OrganizationPhysicalChartEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_OrganizationPhysicalChartAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            OrganizationPhysicalChartId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateOrganizationPhysicalChartDB(OrganizationPhysicalChartEntity OrganizationPhysicalChartEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@OwnerId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OrganizationPhysicalChartTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = OrganizationPhysicalChartEntityParam.OrganizationPhysicalChartId;
            parameters[1].Value = (OrganizationPhysicalChartEntityParam.OwnerId == null ? System.DBNull.Value : (object)OrganizationPhysicalChartEntityParam.OwnerId);
            parameters[2].Value = FarsiToArabic.ToArabic(OrganizationPhysicalChartEntityParam.OrganizationPhysicalChartTitle.Trim());
            parameters[3].Value = OrganizationPhysicalChartEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_OrganizationPhysicalChartUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteOrganizationPhysicalChartDB(OrganizationPhysicalChartEntity OrganizationPhysicalChartEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = OrganizationPhysicalChartEntityParam.OrganizationPhysicalChartId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_OrganizationPhysicalChartDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public OrganizationPhysicalChartEntity GetSingleOrganizationPhysicalChartDB(OrganizationPhysicalChartEntity OrganizationPhysicalChartEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = OrganizationPhysicalChartEntityParam.OrganizationPhysicalChartId;

                reader = _intranetDB.RunProcedureReader("General.p_OrganizationPhysicalChartGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetOrganizationPhysicalChartDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<OrganizationPhysicalChartEntity> GetAllOrganizationPhysicalChartDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetOrganizationPhysicalChartDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_OrganizationPhysicalChartGetAll", new IDataParameter[] { }));
        }
                public List<OrganizationPhysicalChartEntity> GetAllOrganizationPhysicalChartIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetOrganizationPhysicalChartDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_OrganizationPhysicalChartIsActiveGetAll", new IDataParameter[] { }));
        }
        
        public List<OrganizationPhysicalChartEntity> GetPageOrganizationPhysicalChartDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_OrganizationPhysicalChart";
            parameters[5].Value = "OrganizationPhysicalChartId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetOrganizationPhysicalChartDBCollectionFromDataSet(ds, out count);
        }

        public OrganizationPhysicalChartEntity GetOrganizationPhysicalChartDBFromDataReader(IDataReader reader)
        {
            return new OrganizationPhysicalChartEntity(Guid.Parse(reader["OrganizationPhysicalChartId"].ToString()),
                Convert.IsDBNull(reader["OwnerId"]) ? null : (Guid?)reader["OwnerId"],                                    
                                    reader["OrganizationPhysicalChartTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    Convert.IsDBNull(reader["IsActive"]) ? null :(bool?) Convert.ToBoolean(Convert.ToInt16(reader["IsActive"])),
                                    Convert.IsDBNull(reader["ChildCount"]) ? null : (int?)reader["ChildCount"],
             Convert.IsDBNull(reader["FlagSearch"]) ? null : (int?)reader["FlagSearch"]);
        }

        public List<OrganizationPhysicalChartEntity> GetOrganizationPhysicalChartDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<OrganizationPhysicalChartEntity> lst = new List<OrganizationPhysicalChartEntity>();
                while (reader.Read())
                    lst.Add(GetOrganizationPhysicalChartDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<OrganizationPhysicalChartEntity> GetOrganizationPhysicalChartDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetOrganizationPhysicalChartDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



        public List<OrganizationPhysicalChartEntity> GetAllOnDemandOrganizationPhysicalChartDB(OrganizationPhysicalChartEntity organizationPhysicalChartEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();

            IDataParameter[] parameters = {
													new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
            parameters[0].Value = (organizationPhysicalChartEntityParam.OrganizationPhysicalChartId==null?System.DBNull.Value:(object)organizationPhysicalChartEntityParam.OrganizationPhysicalChartId);

            return GetOrganizationPhysicalChartDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_OnDemandOrganizationPhysicalChartGetAll", parameters));
        }

        public List<OrganizationPhysicalChartEntity> SearchOnDemandOrganizationPhysicalChartDB(OrganizationPhysicalChartEntity organizationPhysicalChartEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();

            IDataParameter[] parameters = {
													new SqlParameter("@OrganizationPhysicalChartTitle", SqlDbType.NVarChar,-1)		                                   		  
						      };
            parameters[0].Value = organizationPhysicalChartEntity.OrganizationPhysicalChartTitle;

            return GetOrganizationPhysicalChartDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_OrganizationPhysicalChartOndemanSearch", parameters));
        }
    }

    #endregion
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;

namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/07>
    /// Description: <وضیعت ملیت>
    /// </summary>

    //-----------------------------------------------------
    #region Class "NationalityDB"

    public class NationalityDB
    {



        #region Methods :

        public void AddNationalityDB(NationalityEntity NationalityEntityParam, out Guid NationalityId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@NationalityId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@NationalityTitlePersian", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@NationalityTitleEnglish", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = NationalityEntityParam.CountryId;
            parameters[2].Value = FarsiToArabic.ToArabic(NationalityEntityParam.NationalityTitlePersian.Trim());
            parameters[3].Value = FarsiToArabic.ToArabic(NationalityEntityParam.NationalityTitleEnglish.Trim());
            parameters[4].Value = NationalityEntityParam.IsActive;

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_NationalityAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            NationalityId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateNationalityDB(NationalityEntity NationalityEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@NationalityId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@NationalityTitlePersian", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@NationalityTitleEnglish", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = NationalityEntityParam.NationalityId;
            parameters[1].Value = NationalityEntityParam.CountryId;
            parameters[2].Value = FarsiToArabic.ToArabic(NationalityEntityParam.NationalityTitlePersian.Trim());
            parameters[3].Value = FarsiToArabic.ToArabic(NationalityEntityParam.NationalityTitleEnglish.Trim());
            parameters[4].Value = NationalityEntityParam.IsActive;

            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_NationalityUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteNationalityDB(NationalityEntity NationalityEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@NationalityId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = NationalityEntityParam.NationalityId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_NationalityDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public NationalityEntity GetSingleNationalityDB(NationalityEntity NationalityEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@NationalityId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = NationalityEntityParam.NationalityId;

                reader = _intranetDB.RunProcedureReader("General.p_NationalityGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetNationalityDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<NationalityEntity> GetAllNationalityDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetNationalityDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_NationalityGetAll", new IDataParameter[] { }));
        }

        public List<NationalityEntity> GetAllNationalityIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetNationalityDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_NationalityIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<NationalityEntity> GetPageNationalityDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Nationality";
            parameters[5].Value = "NationalityId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetNationalityDBCollectionFromDataSet(ds, out count);
        }

        public NationalityEntity GetNationalityDBFromDataReader(IDataReader reader)
        {
            return new NationalityEntity(Guid.Parse(reader["NationalityId"].ToString()),
                                    Guid.Parse(reader["CountryId"].ToString()),
                                    reader["NationalityTitlePersian"].ToString(),
                                    reader["NationalityTitleEnglish"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<NationalityEntity> GetNationalityDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<NationalityEntity> lst = new List<NationalityEntity>();
                while (reader.Read())
                    lst.Add(GetNationalityDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<NationalityEntity> GetNationalityDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetNationalityDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<NationalityEntity> GetNationalityDBCollectionByCountryDB(NationalityEntity NationalityEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier);
            parameter.Value = NationalityEntityParam.CountryId;
            return GetNationalityDBCollectionFromDataReader(_intranetDB.RunProcedureReader("General.p_NationalityGetByCountry", new[] { parameter }));
        }


        #endregion



    }

    #endregion
}

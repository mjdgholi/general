﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using GeneralProject.General.Entity;
using Intranet.Common;

namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    public class UserDB
    {

        #region Methods :

        public void AddUserDB(UserEntity UserEntityParam, out int UserID)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@UserID", SqlDbType.Int, 4),
								 new SqlParameter("@FirstName", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@LastName", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@UserPass", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@UserStyle", SqlDbType.VarChar,50) ,
											  new SqlParameter("@UserName", SqlDbType.VarChar,50) ,
											  new SqlParameter("@PortalID", SqlDbType.Int) ,
											  new SqlParameter("@IsSuperUser", SqlDbType.Bit) ,
											  new SqlParameter("@IsLocked", SqlDbType.Bit) ,
											  new SqlParameter("@LastLoginDate", SqlDbType.DateTime) ,
											  new SqlParameter("@LastPasswordChangeDate", SqlDbType.DateTime) ,
											  new SqlParameter("@LastLockDate", SqlDbType.DateTime) ,
											  new SqlParameter("@LastUnLockDate", SqlDbType.DateTime) ,
											  new SqlParameter("@FailedPasswordAttemptCount", SqlDbType.Int) ,
											  new SqlParameter("@FailedPasswordAttemptStart", SqlDbType.DateTime) ,
											  new SqlParameter("@Mobile", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@HasChangedPassword", SqlDbType.Bit) ,
											  new SqlParameter("@JoinDate", SqlDbType.DateTime) ,
											  new SqlParameter("@ConfirmDate", SqlDbType.DateTime) ,
											  new SqlParameter("@IsAnonymous", SqlDbType.Bit) ,
											  new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@LastIP", SqlDbType.NVarChar,50) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = UserEntityParam.FirstName;
            parameters[2].Value = UserEntityParam.LastName;
            parameters[3].Value = UserEntityParam.UserPass;
            parameters[4].Value = UserEntityParam.Email;
            parameters[5].Value = UserEntityParam.UserStyle;
            parameters[6].Value = UserEntityParam.UserName;
            parameters[7].Value = UserEntityParam.PortalID;
            parameters[8].Value = UserEntityParam.IsSuperUser;
            parameters[9].Value = UserEntityParam.IsLocked;
            parameters[10].Value = UserEntityParam.LastLoginDate;
            parameters[11].Value = UserEntityParam.LastPasswordChangeDate;
            parameters[12].Value = UserEntityParam.LastLockDate;
            parameters[13].Value = UserEntityParam.LastUnLockDate;
            parameters[14].Value = UserEntityParam.FailedPasswordAttemptCount;
            parameters[15].Value = UserEntityParam.FailedPasswordAttemptStart;
            parameters[16].Value = UserEntityParam.Mobile;
            parameters[17].Value = UserEntityParam.HasChangedPassword;
            parameters[18].Value = UserEntityParam.JoinDate;
            parameters[19].Value = UserEntityParam.ConfirmDate;
            parameters[20].Value = UserEntityParam.IsAnonymous;
            parameters[21].Value = UserEntityParam.UserGuid;
            parameters[22].Value = UserEntityParam.LastIP;

            parameters[100].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("p_UserAdd", parameters);
            var messageError = parameters[100].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            UserID = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateUserDB(UserEntity userEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@UserID", SqlDbType.Int, 4),
						 new SqlParameter("@FirstName", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@LastName", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@UserPass", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@Email", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@UserStyle", SqlDbType.VarChar,50) ,
											  new SqlParameter("@UserName", SqlDbType.VarChar,50) ,
											  new SqlParameter("@PortalID", SqlDbType.Int) ,
											  new SqlParameter("@IsSuperUser", SqlDbType.Bit) ,
											  new SqlParameter("@IsLocked", SqlDbType.Bit) ,
											  new SqlParameter("@LastLoginDate", SqlDbType.DateTime) ,
											  new SqlParameter("@LastPasswordChangeDate", SqlDbType.DateTime) ,
											  new SqlParameter("@LastLockDate", SqlDbType.DateTime) ,
											  new SqlParameter("@LastUnLockDate", SqlDbType.DateTime) ,
											  new SqlParameter("@FailedPasswordAttemptCount", SqlDbType.Int) ,
											  new SqlParameter("@FailedPasswordAttemptStart", SqlDbType.DateTime) ,
											  new SqlParameter("@Mobile", SqlDbType.NVarChar,20) ,
											  new SqlParameter("@HasChangedPassword", SqlDbType.Bit) ,
											  new SqlParameter("@JoinDate", SqlDbType.DateTime) ,
											  new SqlParameter("@ConfirmDate", SqlDbType.DateTime) ,
											  new SqlParameter("@IsAnonymous", SqlDbType.Bit) ,
											  new SqlParameter("@UserGuid", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@LastIP", SqlDbType.NVarChar,50) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = userEntityParam.UserID;
            parameters[1].Value = userEntityParam.FirstName;
            parameters[2].Value = userEntityParam.LastName;
            parameters[3].Value = userEntityParam.UserPass;
            parameters[4].Value = userEntityParam.Email;
            parameters[5].Value = userEntityParam.UserStyle;
            parameters[6].Value = userEntityParam.UserName;
            parameters[7].Value = userEntityParam.PortalID;
            parameters[8].Value = userEntityParam.IsSuperUser;
            parameters[9].Value = userEntityParam.IsLocked;
            parameters[10].Value = userEntityParam.LastLoginDate;
            parameters[11].Value = userEntityParam.LastPasswordChangeDate;
            parameters[12].Value = userEntityParam.LastLockDate;
            parameters[13].Value = userEntityParam.LastUnLockDate;
            parameters[14].Value = userEntityParam.FailedPasswordAttemptCount;
            parameters[15].Value = userEntityParam.FailedPasswordAttemptStart;
            parameters[16].Value = userEntityParam.Mobile;
            parameters[17].Value = userEntityParam.HasChangedPassword;
            parameters[18].Value = userEntityParam.JoinDate;
            parameters[19].Value = userEntityParam.ConfirmDate;
            parameters[20].Value = userEntityParam.IsAnonymous;
            parameters[21].Value = userEntityParam.UserGuid;
            parameters[22].Value = userEntityParam.LastIP;

            parameters[23].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("p_UserUpdate", parameters);
            var messageError = parameters[23].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteUserDB(UserEntity UserEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@UserID", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = UserEntityParam.UserID;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("p_UserDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public UserEntity GetSingleUserDB(UserEntity UserEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@UserID", DataTypes.integer, 4, 										UserEntityParam.UserID)
						      };

                reader = _intranetDB.RunProcedureReader("p_UserGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetUserDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<UserEntity> GetAllUserDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetUserDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("p_UserGetAll", new IDataParameter[] { }));
        }
        public DataSet GetAllUserIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return 
                        _intranetDB.RunProcedureDS
                        ("General.p_UserIsActiveGetAll", new IDataParameter[] { });
        }
        public List<UserEntity> GetPageUserDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_User";
            parameters[5].Value = "UserID";
            DataSet ds = _intranetDB.RunProcedureDS("p_TablesGetPage", parameters);
            return GetUserDBCollectionFromDataSet(ds, out count);
        }

        public UserEntity GetUserDBFromDataReader(IDataReader reader)
        {
            return new UserEntity(int.Parse(reader["UserID"].ToString()),
                                    reader["FirstName"].ToString(),
                                    reader["LastName"].ToString(),
                                    reader["UserPass"].ToString(),
                                    reader["Email"].ToString(),
                                    reader["UserStyle"].ToString(),
                                    reader["UserName"].ToString(),
                                    int.Parse(reader["PortalID"].ToString()),
                                    bool.Parse(reader["IsSuperUser"].ToString()),
                                    bool.Parse(reader["IsLocked"].ToString()),
                                    DateTime.Parse(reader["LastLoginDate"].ToString()),
                                    DateTime.Parse(reader["LastPasswordChangeDate"].ToString()),
                                    DateTime.Parse(reader["LastLockDate"].ToString()),
                                    DateTime.Parse(reader["LastUnLockDate"].ToString()),
                                    int.Parse(reader["FailedPasswordAttemptCount"].ToString()),
                                    DateTime.Parse(reader["FailedPasswordAttemptStart"].ToString()),
                                    reader["Mobile"].ToString(),
                                    bool.Parse(reader["HasChangedPassword"].ToString()),
                                    DateTime.Parse(reader["JoinDate"].ToString()),
                                    DateTime.Parse(reader["ConfirmDate"].ToString()),
                                    bool.Parse(reader["IsAnonymous"].ToString()),
                                    Guid.Parse(reader["UserGuid"].ToString()),
                                    reader["LastIP"].ToString(),
                                    reader["FullName"].ToString());
        }

        public List<UserEntity> GetUserDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<UserEntity> lst = new List<UserEntity>();
                while (reader.Read())
                    lst.Add(GetUserDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<UserEntity> GetUserDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetUserDBCollectionFromDataReader(ds.CreateDataReader());
        }




        public  bool ChangePasswordDB(int userId, string passWord)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", SqlDbType.Int),
                                            new SqlParameter("@PassWord", SqlDbType.NVarChar, 100),
                                        };
            parameters[0].Value = userId;
            parameters[1].Value = passWord;
            return intranetDb.RunProcedure("[General].[p_ChangePassword]", parameters);
        }

        public  int AddUserDB(string userName,string fName,string lName,string passWord,bool isActive)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserName", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@FirstName", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@LastName", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@PassWord", SqlDbType.NVarChar, 100),   
                                            new SqlParameter("@IsActive",SqlDbType.Bit),                                       
                                        };
            parameters[0].Value = userName;
            parameters[1].Value = fName;
            parameters[2].Value = lName;
            parameters[3].Value = passWord;
            parameters[4].Value = isActive;
            var paramOut = new SqlParameter("@UserId", SqlDbType.Int, 4) { Direction = ParameterDirection.Output };
            return intranetDb.RunProcedure("[General].[p_AddUser]", parameters,paramOut);
        }

        public  bool UpdateUserDB(int userId,string userName, string fName, string lName,bool isActive)
        {
            IntranetDB intranetDb = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                             new SqlParameter("@UserId", SqlDbType.Int),  
                                            new SqlParameter("@UserName", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@FirstName", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@LastName", SqlDbType.NVarChar, 100),
                                               new SqlParameter("@IsActive",SqlDbType.Bit),                                                                                    
                                        };
            parameters[0].Value = userId;
            parameters[1].Value = userName;
            parameters[2].Value = fName;
            parameters[3].Value = lName;
            parameters[4].Value = isActive;
            return intranetDb.RunProcedure("[General].[p_UpdateUser]", parameters);
        }


    }
        #endregion
}


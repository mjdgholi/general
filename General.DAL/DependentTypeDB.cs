﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;


namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/09>
    /// Description: <نوع وابستگی>
    /// </summary>

    public class DependentTypeDB
    {
        #region Methods :

        public void AddDependentTypeDB(DependentTypeEntity DependentTypeEntityParam, out Guid DependentTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@DependentTypeTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(DependentTypeEntityParam.DependentTypeTitle.Trim());
            parameters[2].Value = DependentTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_DependentTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            DependentTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateDependentTypeDB(DependentTypeEntity DependentTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@DependentTypeTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = DependentTypeEntityParam.DependentTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(DependentTypeEntityParam.DependentTypeTitle.Trim());
            parameters[2].Value = DependentTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_DependentTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteDependentTypeDB(DependentTypeEntity DependentTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = DependentTypeEntityParam.DependentTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_DependentTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public DependentTypeEntity GetSingleDependentTypeDB(DependentTypeEntity DependentTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@DependentTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = DependentTypeEntityParam.DependentTypeId;

                reader = _intranetDB.RunProcedureReader("General.p_DependentTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetDependentTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DependentTypeEntity> GetAllDependentTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDependentTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_DependentTypeGetAll", new IDataParameter[] { }));
        }
        public List<DependentTypeEntity> GetAllDependentTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetDependentTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_DependentTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        

        public List<DependentTypeEntity> GetPageDependentTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_DependentType";
            parameters[5].Value = "DependentTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetDependentTypeDBCollectionFromDataSet(ds, out count);
        }

        public DependentTypeEntity GetDependentTypeDBFromDataReader(IDataReader reader)
        {
            return new DependentTypeEntity(Guid.Parse(reader["DependentTypeId"].ToString()),
                                    reader["DependentTypeTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<DependentTypeEntity> GetDependentTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<DependentTypeEntity> lst = new List<DependentTypeEntity>();
                while (reader.Read())
                    lst.Add(GetDependentTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<DependentTypeEntity> GetDependentTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetDependentTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }


}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;


namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/04>
    /// Description: <سازمان>
    /// </summary>

    public class OrganizationDB
    {
        #region Methods :

        public void AddOrganizationDB(OrganizationEntity OrganizationEntityParam, out Guid OrganizationId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@OrganizationId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@OrganizationPersianTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@OrganizationEnglishTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OrganizationEconomicCode", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@OrganizationTelephoneNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@OrganizationFoxNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@OrganizationAddress", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@OrganizationWebSite", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@OrganizationEmail", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@ZipCode", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@OrganizationDescription", SqlDbType.NVarChar,200) ,	
										      new SqlParameter("@IsActive", SqlDbType.Bit),
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationPersianTitle.Trim());
            parameters[2].Value = (OrganizationEntityParam.OrganizationEnglishTitle==""?System.DBNull.Value:(object)OrganizationEntityParam.OrganizationEnglishTitle);
            parameters[3].Value = (OrganizationEntityParam.CityId==new Guid()?System.DBNull.Value:(object)OrganizationEntityParam.CityId);
            parameters[4].Value = (OrganizationEntityParam.OrganizationEconomicCode==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationEconomicCode.Trim()));
            parameters[5].Value = (OrganizationEntityParam.OrganizationTelephoneNo==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationTelephoneNo.Trim()));
            parameters[6].Value = (OrganizationEntityParam.OrganizationFoxNo==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationFoxNo.Trim()));
            parameters[7].Value = (OrganizationEntityParam.OrganizationAddress==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationAddress.Trim()));
            parameters[8].Value = (OrganizationEntityParam.OrganizationWebSite==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationWebSite.Trim()));
            parameters[9].Value = (OrganizationEntityParam.OrganizationEmail==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationEmail.Trim()));
            parameters[10].Value = (OrganizationEntityParam.ZipCode==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(OrganizationEntityParam.ZipCode.Trim()));
            parameters[11].Value = (OrganizationEntityParam.OrganizationDescription==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationDescription.Trim()));
            parameters[12].Value = OrganizationEntityParam.IsActive;

            parameters[13].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_OrganizationAdd", parameters);
            var messageError = parameters[13].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            OrganizationId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateOrganizationDB(OrganizationEntity OrganizationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@OrganizationId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@OrganizationPersianTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@OrganizationEnglishTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@CityId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@OrganizationEconomicCode", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@OrganizationTelephoneNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@OrganizationFoxNo", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@OrganizationAddress", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@OrganizationWebSite", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@OrganizationEmail", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@ZipCode", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@OrganizationDescription", SqlDbType.NVarChar,200) ,	
	                                          new SqlParameter("@IsActive", SqlDbType.Bit),
						              new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = OrganizationEntityParam.OrganizationId;
            parameters[1].Value = FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationPersianTitle.Trim());
            parameters[2].Value = (OrganizationEntityParam.OrganizationEnglishTitle == "" ? System.DBNull.Value : (object)OrganizationEntityParam.OrganizationEnglishTitle);
            parameters[3].Value = (OrganizationEntityParam.CityId == new Guid() ? System.DBNull.Value : (object)OrganizationEntityParam.CityId);
            parameters[4].Value = (OrganizationEntityParam.OrganizationEconomicCode == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationEconomicCode.Trim()));
            parameters[5].Value = (OrganizationEntityParam.OrganizationTelephoneNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationTelephoneNo.Trim()));
            parameters[6].Value = (OrganizationEntityParam.OrganizationFoxNo == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationFoxNo.Trim()));
            parameters[7].Value = (OrganizationEntityParam.OrganizationAddress == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationAddress.Trim()));
            parameters[8].Value = (OrganizationEntityParam.OrganizationWebSite == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationWebSite.Trim()));
            parameters[9].Value = (OrganizationEntityParam.OrganizationEmail == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationEmail.Trim()));
            parameters[10].Value = (OrganizationEntityParam.ZipCode == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(OrganizationEntityParam.ZipCode.Trim()));
            parameters[11].Value = (OrganizationEntityParam.OrganizationDescription == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(OrganizationEntityParam.OrganizationDescription.Trim()));
            parameters[12].Value = OrganizationEntityParam.IsActive;
            parameters[13].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_OrganizationUpdate", parameters);
            var messageError = parameters[13].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteOrganizationDB(OrganizationEntity OrganizationEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@OrganizationId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = OrganizationEntityParam.OrganizationId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_OrganizationDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public OrganizationEntity GetSingleOrganizationDB(OrganizationEntity OrganizationEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@OrganizationId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = OrganizationEntityParam.OrganizationId;

                reader = _intranetDB.RunProcedureReader("General.p_OrganizationGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetOrganizationDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<OrganizationEntity> GetAllOrganizationDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetOrganizationDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_OrganizationGetAll", new IDataParameter[] { }));
        }
        public List<OrganizationEntity> GetAllOrganizationIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetOrganizationDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_OrganizationIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<OrganizationEntity> GetPageOrganizationDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Organization";
            parameters[5].Value = "OrganizationId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetOrganizationDBCollectionFromDataSet(ds, out count);
        }

        public OrganizationEntity GetOrganizationDBFromDataReader(IDataReader reader)
        {
            return new OrganizationEntity(Guid.Parse(reader["OrganizationId"].ToString()),
                                    reader["OrganizationPersianTitle"].ToString(),
                                    reader["OrganizationEnglishTitle"].ToString(),
                                    Convert.IsDBNull(reader["CityId"]) ? null : (Guid?)reader["CityId"],                                     
                                    reader["OrganizationEconomicCode"].ToString(),
                                    reader["OrganizationTelephoneNo"].ToString(),
                                    reader["OrganizationFoxNo"].ToString(),
                                    reader["OrganizationAddress"].ToString(),
                                    reader["OrganizationWebSite"].ToString(),
                                    reader["OrganizationEmail"].ToString(),
                                    reader["ZipCode"].ToString(),
                                    reader["OrganizationDescription"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    Convert.IsDBNull(reader["ProvinceId"]) ? null : (Guid?)reader["ProvinceId"]);
        }

        public List<OrganizationEntity> GetOrganizationDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<OrganizationEntity> lst = new List<OrganizationEntity>();
                while (reader.Read())
                    lst.Add(GetOrganizationDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<OrganizationEntity> GetOrganizationDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetOrganizationDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

}

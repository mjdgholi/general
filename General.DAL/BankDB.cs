﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/27>
    /// Description: <بانک>
    /// </summary>

    #region Class "BankDB"

    public class BankDB
    {

        #region Methods :

        public void AddBankDB(BankEntity BankEntityParam, out Guid BankId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BankId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@BankTitle", SqlDbType.NVarChar, 100),                                            
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(BankEntityParam.BankTitle.Trim());
            parameters[2].Value = BankEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_BankAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            BankId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateBankDB(BankEntity BankEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BankId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@BankTitle", SqlDbType.NVarChar, 100),                                           
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = BankEntityParam.BankId;
            parameters[1].Value = FarsiToArabic.ToArabic(BankEntityParam.BankTitle.Trim());
            parameters[2].Value = BankEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_BankUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteBankDB(BankEntity BankEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BankId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = BankEntityParam.BankId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_BankDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public BankEntity GetSingleBankDB(BankEntity BankEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@BankId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = BankEntityParam.BankId;

                reader = _intranetDB.RunProcedureReader("General.p_BankGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetBankDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<BankEntity> GetAllBankDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetBankDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("General.p_BankGetAll", new IDataParameter[] {}));
        }

        public List<BankEntity> GetAllIsActiveBankDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetBankDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("General.p_BankGetAllIsActive", new IDataParameter[] { }));
        }

        public List<BankEntity> GetPageBankDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "General.t_Bank";
            parameters[5].Value = "BankId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetBankDBCollectionFromDataSet(ds, out count);
        }

        public BankEntity GetBankDBFromDataReader(IDataReader reader)
        {
            return new BankEntity(new Guid(reader["BankId"].ToString()),
                                  reader["BankTitle"].ToString(),
                                  reader["CreationDate"].ToString(),
                                  reader["ModificationDate"].ToString(),
                                  bool.Parse(reader["IsActive"].ToString()));
        }

        public List<BankEntity> GetBankDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<BankEntity> lst = new List<BankEntity>();
                while (reader.Read())
                    lst.Add(GetBankDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<BankEntity> GetBankDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetBankDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;


namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/07>
    /// Description: <وضیعت تاهل>
    /// </summary>




    public class MarriageStatusDB
    {



        #region Methods :

        public void AddMarriageStatusDB(MarriageStatusEntity MarriageStatusEntityParam, out Guid MarriageStatusId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@MarriageStatusId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@MarriageStatusTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsMarried", SqlDbType.Bit) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(MarriageStatusEntityParam.MarriageStatusTitle.Trim());
            parameters[2].Value = MarriageStatusEntityParam.IsMarried;            
            parameters[3].Value = MarriageStatusEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_MarriageStatusAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            MarriageStatusId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateMarriageStatusDB(MarriageStatusEntity MarriageStatusEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@MarriageStatusId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@MarriageStatusTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsMarried", SqlDbType.Bit) ,			
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = MarriageStatusEntityParam.MarriageStatusId;
            parameters[1].Value = FarsiToArabic.ToArabic(MarriageStatusEntityParam.MarriageStatusTitle.Trim());
            parameters[2].Value = MarriageStatusEntityParam.IsMarried;
            parameters[3].Value = MarriageStatusEntityParam.IsActive;
            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_MarriageStatusUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteMarriageStatusDB(MarriageStatusEntity MarriageStatusEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@MarriageStatusId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = MarriageStatusEntityParam.MarriageStatusId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_MarriageStatusDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public MarriageStatusEntity GetSingleMarriageStatusDB(MarriageStatusEntity MarriageStatusEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@MarriageStatusId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = MarriageStatusEntityParam.MarriageStatusId;

                reader = _intranetDB.RunProcedureReader("General.p_MarriageStatusGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetMarriageStatusDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MarriageStatusEntity> GetAllMarriageStatusDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMarriageStatusDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_MarriageStatusGetAll", new IDataParameter[] { }));
        }
        public List<MarriageStatusEntity> GetAllMarriageStatusIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMarriageStatusDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_MarriageStatusIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<MarriageStatusEntity> GetPageMarriageStatusDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_MarriageStatus";
            parameters[5].Value = "MarriageStatusId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetMarriageStatusDBCollectionFromDataSet(ds, out count);
        }

        public MarriageStatusEntity GetMarriageStatusDBFromDataReader(IDataReader reader)
        {
            return new MarriageStatusEntity(Guid.Parse(reader["MarriageStatusId"].ToString()),
                                    reader["MarriageStatusTitle"].ToString(),
                                    bool.Parse(reader["IsMarried"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<MarriageStatusEntity> GetMarriageStatusDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<MarriageStatusEntity> lst = new List<MarriageStatusEntity>();
                while (reader.Read())
                    lst.Add(GetMarriageStatusDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MarriageStatusEntity> GetMarriageStatusDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetMarriageStatusDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

}

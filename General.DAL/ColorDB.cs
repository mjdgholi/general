﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;


namespace Intranet.DesktopModules.GeneralProject.General.DAL
{

    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/25>
    /// Description: <رنگ>
    /// </summary>

    public class ColorDB
    {
        #region Methods :

        public void AddColorDB(ColorEntity ColorEntityParam, out Guid ColorId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ColorId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ColorTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(ColorEntityParam.ColorTitle.Trim());
            parameters[2].Value = ColorEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_ColorAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ColorId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateColorDB(ColorEntity ColorEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ColorId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ColorTitle", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = ColorEntityParam.ColorId;
            parameters[1].Value = FarsiToArabic.ToArabic(ColorEntityParam.ColorTitle.Trim());
            parameters[2].Value = ColorEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_ColorUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteColorDB(ColorEntity ColorEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ColorId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ColorEntityParam.ColorId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_ColorDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ColorEntity GetSingleColorDB(ColorEntity ColorEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ColorId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ColorEntityParam.ColorId;

                reader = _intranetDB.RunProcedureReader("General.p_ColorGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetColorDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ColorEntity> GetAllColorDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetColorDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_ColorGetAll", new IDataParameter[] { }));
        }

        public List<ColorEntity> 
            GetAllColorIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetColorDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_ColorIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<ColorEntity> GetPageColorDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Color";
            parameters[5].Value = "ColorId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetColorDBCollectionFromDataSet(ds, out count);
        }

        public ColorEntity GetColorDBFromDataReader(IDataReader reader)
        {
            return new ColorEntity(Guid.Parse(reader["ColorId"].ToString()),
                                    reader["ColorTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ColorEntity> GetColorDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ColorEntity> lst = new List<ColorEntity>();
                while (reader.Read())
                    lst.Add(GetColorDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ColorEntity> GetColorDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetColorDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;


namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/06>
    /// Description: <حوزه جغرافیایی>
    /// </summary>

    //-----------------------------------------------------
    #region Class "GeographicScopeDB"

    public class GeographicScopeDB
    {
        #region Methods :

        public void AddGeographicScopeDB(GeographicScopeEntity GeographicScopeEntityParam, out Guid GeographicScopeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@GeographicScopeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@GeographicScopePersianTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@GeographicScopeEnglishTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Priority", SqlDbType.Int) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(GeographicScopeEntityParam.GeographicScopePersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(GeographicScopeEntityParam.GeographicScopeEnglishTitle.Trim());
            parameters[3].Value = GeographicScopeEntityParam.Priority;
            parameters[4].Value = GeographicScopeEntityParam.IsActive;

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_GeographicScopeAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            GeographicScopeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateGeographicScopeDB(GeographicScopeEntity GeographicScopeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@GeographicScopeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@GeographicScopePersianTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@GeographicScopeEnglishTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Priority", SqlDbType.Int) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = GeographicScopeEntityParam.GeographicScopeId;
            parameters[1].Value = FarsiToArabic.ToArabic(GeographicScopeEntityParam.GeographicScopePersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(GeographicScopeEntityParam.GeographicScopeEnglishTitle.Trim());
            parameters[3].Value = GeographicScopeEntityParam.Priority;
            parameters[4].Value = GeographicScopeEntityParam.IsActive;

            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_GeographicScopeUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteGeographicScopeDB(GeographicScopeEntity GeographicScopeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@GeographicScopeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = GeographicScopeEntityParam.GeographicScopeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_GeographicScopeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public GeographicScopeEntity GetSingleGeographicScopeDB(GeographicScopeEntity GeographicScopeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@GeographicScopeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = GeographicScopeEntityParam.GeographicScopeId;

                reader = _intranetDB.RunProcedureReader("General.p_GeographicScopeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetGeographicScopeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<GeographicScopeEntity> GetAllGeographicScopeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetGeographicScopeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_GeographicScopeGetAll", new IDataParameter[] { }));
        }

        public List<GeographicScopeEntity> GetAllGeographicScopeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            
            return GetGeographicScopeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_GeographicScopeIsActiveGetAll", new IDataParameter[] { }));
        }
        

        public List<GeographicScopeEntity> GetPageGeographicScopeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_GeographicScope";
            parameters[5].Value = "GeographicScopeId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetGeographicScopeDBCollectionFromDataSet(ds, out count);
        }

        public GeographicScopeEntity GetGeographicScopeDBFromDataReader(IDataReader reader)
        {
            return new GeographicScopeEntity(new Guid(reader["GeographicScopeId"].ToString()), 
                                    reader["GeographicScopePersianTitle"].ToString(),
                                    reader["GeographicScopeEnglishTitle"].ToString(),
                                    int.Parse(reader["Priority"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<GeographicScopeEntity> GetGeographicScopeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<GeographicScopeEntity> lst = new List<GeographicScopeEntity>();
                while (reader.Read())
                    lst.Add(GetGeographicScopeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<GeographicScopeEntity> GetGeographicScopeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetGeographicScopeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}

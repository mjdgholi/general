﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;

namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/06>
    // Description:	<جنسیت>
    /// </summary>
    #region Class "GenderDB"

    public class GenderDB
    {
        #region Methods :

        public void AddGenderDB(GenderEntity GenderEntityParam, out Guid GenderId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@GenderId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@GenderTitle", SqlDbType.NVarChar, 100),                                           
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(GenderEntityParam.GenderTitle.Trim());
            parameters[2].Value = GenderEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_GenderAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            GenderId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateGenderDB(GenderEntity GenderEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@GenderId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@GenderTitle", SqlDbType.NVarChar, 100),                                           
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = GenderEntityParam.GenderId;
            parameters[1].Value = FarsiToArabic.ToArabic(GenderEntityParam.GenderTitle.Trim());
            parameters[2].Value = GenderEntityParam.IsActive;
            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_GenderUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteGenderDB(GenderEntity GenderEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@GenderId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = GenderEntityParam.GenderId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_GenderDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public GenderEntity GetSingleGenderDB(GenderEntity GenderEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@GenderId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = GenderEntityParam.GenderId;

                reader = _intranetDB.RunProcedureReader("General.p_GenderGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetGenderDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<GenderEntity> GetAllGenderDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetGenderDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("General.p_GenderGetAll", new IDataParameter[] {}));
        }

        public List<GenderEntity> GetAllIsActiveGenderDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetGenderDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("General.[p_GenderGetAllIsActive]", new IDataParameter[] { }));
        }

        public List<GenderEntity> GetPageGenderDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "General.t_Gender";
            parameters[5].Value = "GenderId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetGenderDBCollectionFromDataSet(ds, out count);
        }

        public GenderEntity GetGenderDBFromDataReader(IDataReader reader)
        {
            return new GenderEntity(new Guid(reader["GenderId"].ToString()), 
                                    reader["GenderTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<GenderEntity> GetGenderDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<GenderEntity> lst = new List<GenderEntity>();
                while (reader.Read())
                    lst.Add(GetGenderDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<GenderEntity> GetGenderDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetGenderDBCollectionFromDataReader(ds.CreateDataReader());
        }
        #endregion

    }

    #endregion
}
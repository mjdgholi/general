﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/08>
    /// Description: <کشور>
    /// </summary>


    public class CountryDB
    {
        #region Methods :

        public void AddCountryDB(CountryEntity CountryEntityParam, out Guid CountryId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@CountryTitlePersian", SqlDbType.NVarChar,100) ,
								 new SqlParameter("@CountryTitleEnglish", SqlDbType.NVarChar,100) ,											  
								 new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(CountryEntityParam.CountryTitlePersian.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(CountryEntityParam.CountryTitleEnglish.Trim());
            parameters[3].Value = CountryEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_CountryAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            CountryId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateCountryDB(CountryEntity CountryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@CountryTitlePersian", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@CountryTitleEnglish", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = CountryEntityParam.CountryId;
            parameters[1].Value = FarsiToArabic.ToArabic(CountryEntityParam.CountryTitlePersian.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(CountryEntityParam.CountryTitleEnglish.Trim());
            parameters[3].Value = CountryEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_CountryUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteCountryDB(CountryEntity CountryEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = CountryEntityParam.CountryId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_CountryDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public CountryEntity GetSingleCountryDB(CountryEntity CountryEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = CountryEntityParam.CountryId;

                reader = _intranetDB.RunProcedureReader("General.p_CountryGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetCountryDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<CountryEntity> GetAllCountryDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetCountryDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_CountryGetAll", new IDataParameter[] { }));
        }

        public List<CountryEntity> GetAllCountryIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();


            return GetCountryDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_CountryIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<CountryEntity> GetPageCountryDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "General.t_Country";
            parameters[5].Value = "CountryId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetCountryDBCollectionFromDataSet(ds, out count);
        }

        public CountryEntity GetCountryDBFromDataReader(IDataReader reader)
        {
            return new CountryEntity(new Guid(reader["CountryId"].ToString()), 
                                    reader["CountryTitlePersian"].ToString(),
                                    reader["CountryTitleEnglish"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<CountryEntity> GetCountryDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<CountryEntity> lst = new List<CountryEntity>();
                while (reader.Read())
                    lst.Add(GetCountryDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<CountryEntity> GetCountryDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetCountryDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }


}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;


namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/06>
    /// Description: <استان>
    /// </summary>

    public class ProvinceDB
    {
        #region Methods :

        public void AddProvinceDB(ProvinceEntity ProvinceEntityParam, out Guid ProvinceId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								new SqlParameter("@ProvinceId", SqlDbType.UniqueIdentifier),
								new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier) ,
								new SqlParameter("@ProvinceTitlePersian", SqlDbType.NVarChar,100) ,
								new SqlParameter("@ProvinceTitleEnglish", SqlDbType.NVarChar,100) ,											  
								new SqlParameter("@IsActive", SqlDbType.Bit) ,
								new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = ProvinceEntityParam.CountryId;
            parameters[2].Value = FarsiToArabic.ToArabic(ProvinceEntityParam.ProvinceTitlePersian.Trim());
            parameters[3].Value = FarsiToArabic.ToArabic(ProvinceEntityParam.ProvinceTitleEnglish.Trim());
            parameters[4].Value = ProvinceEntityParam.IsActive;

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_ProvinceAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ProvinceId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateProvinceDB(ProvinceEntity ProvinceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ProvinceId", SqlDbType.UniqueIdentifier),
						                    new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier) ,
											new SqlParameter("@ProvinceTitlePersian", SqlDbType.NVarChar,100) ,
											new SqlParameter("@ProvinceTitleEnglish", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						                      new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = ProvinceEntityParam.ProvinceId;
            parameters[1].Value = ProvinceEntityParam.CountryId;
            parameters[2].Value = FarsiToArabic.ToArabic(ProvinceEntityParam.ProvinceTitlePersian.Trim());
            parameters[3].Value = FarsiToArabic.ToArabic(ProvinceEntityParam.ProvinceTitleEnglish.Trim());
            parameters[4].Value = ProvinceEntityParam.IsActive;

            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_ProvinceUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteProvinceDB(ProvinceEntity ProvinceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ProvinceId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ProvinceEntityParam.ProvinceId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_ProvinceDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ProvinceEntity GetSingleProvinceDB(ProvinceEntity ProvinceEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ProvinceId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ProvinceEntityParam.ProvinceId;

                reader = _intranetDB.RunProcedureReader("General.p_ProvinceGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetProvinceDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProvinceEntity> GetAllProvinceDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetProvinceDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_ProvinceGetAll", new IDataParameter[] { }));
        }
        public List<ProvinceEntity> GetAllProvinceIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
          
            return GetProvinceDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("General.p_ProvinceIsActiveGetAll", new IDataParameter[] { }));
        }


        // ---------- لود کردن بر اساس سامانه دسترسی ها
        public List<ProvinceEntity> GetAllProvinceIsActivePerRowAccessDB(int webUserId, string objectTitlesForRowAccessStr)
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@WebUserId", SqlDbType.Int),
                new SqlParameter("@ObjectTitlesForRowAccess_Str", SqlDbType.NVarChar, -1)
            };
            parameters[0].Value = webUserId;
            parameters[1].Value = objectTitlesForRowAccessStr;
            return GetProvinceDBCollectionFromDataReader(intranetDB.RunProcedureReader("General.p_ProvinceIsActiveGetAllPerRowAccess", parameters));
        }
        //-------------------------------------------------

        public List<ProvinceEntity> GetPageProvinceDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Province";
            parameters[5].Value = "ProvinceId";
            DataSet ds = _intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetProvinceDBCollectionFromDataSet(ds, out count);
        }

        public ProvinceEntity GetProvinceDBFromDataReader(IDataReader reader)
        {
            return new ProvinceEntity(new Guid(reader["ProvinceId"].ToString()), 
                                    Guid.Parse(reader["CountryId"].ToString()),
                                    reader["ProvinceTitlePersian"].ToString(),
                                    reader["ProvinceTitleEnglish"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ProvinceEntity> GetProvinceDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ProvinceEntity> lst = new List<ProvinceEntity>();
                while (reader.Read())
                    lst.Add(GetProvinceDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ProvinceEntity> GetProvinceDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetProvinceDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<ProvinceEntity> GetProvinceDBCollectionByCountryDB(ProvinceEntity ProvinceEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@CountryId", SqlDbType.UniqueIdentifier);
            parameter.Value = ProvinceEntityParam.CountryId;
            return GetProvinceDBCollectionFromDataReader(_intranetDB.RunProcedureReader("General.p_ProvinceGetByCountry", new[] { parameter }));
        }

        #endregion



    }
}

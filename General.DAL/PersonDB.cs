﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.GeneralProject.General.Entity;
using ITC.Library.Classes;
using Intranet.Common;


namespace Intranet.DesktopModules.GeneralProject.General.DAL
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/06>
    // Description:	<فرم اشخاص>
    /// </summary>

    #region Class "PersonDB"

    public class PersonDB
    {
        #region Methods :

        public void AddPersonDB(PersonEntity PersonEntityParam, out Guid PersonId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@GenderId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@BloodGroupId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@HasNationalNo", SqlDbType.Bit),
                                            new SqlParameter("@NationalNo", SqlDbType.NVarChar, 10),
                                            new SqlParameter("@FirstName", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@LastName", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@PreviousLastName", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@FatherName", SqlDbType.NVarChar, 50),
                                            new SqlParameter("@BirthDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@DeathDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@CertificateNo", SqlDbType.NVarChar, 50),
                                            new SqlParameter("@BirthPlaceCityId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@BirthPlaceSection", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@IssuePlaceCityId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@IssuePlaceSection", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@IssueDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@CertificateAlphabetId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@CertificateNumericSeries", SqlDbType.NVarChar, 10),
                                            new SqlParameter("@CertificateSerialNo", SqlDbType.NVarChar, 10),
                                            new SqlParameter("@EnglishFirstName", SqlDbType.NVarChar, 50),
                                            new SqlParameter("@EnglishLastName", SqlDbType.NVarChar, 100),                                            
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = PersonEntityParam.GenderId;
            parameters[2].Value = (PersonEntityParam.BloodGroupId==null?System.DBNull.Value:(object)PersonEntityParam.BloodGroupId);
            parameters[3].Value = PersonEntityParam.HasNationalNo;
            parameters[4].Value = PersonEntityParam.NationalNo;
            parameters[5].Value = FarsiToArabic.ToArabic(PersonEntityParam.FirstName.Trim());
            parameters[6].Value = FarsiToArabic.ToArabic(PersonEntityParam.LastName.Trim());
            parameters[7].Value = FarsiToArabic.ToArabic(PersonEntityParam.PreviousLastName.Trim());
            parameters[8].Value = FarsiToArabic.ToArabic(PersonEntityParam.FatherName.Trim());
            parameters[9].Value = PersonEntityParam.BirthDate;
            parameters[10].Value = (PersonEntityParam.DeathDate == "" ? System.DBNull.Value : (object)PersonEntityParam.DeathDate);
            parameters[11].Value = PersonEntityParam.CertificateNo;
            parameters[12].Value = PersonEntityParam.BirthPlaceCityId;
            parameters[13].Value = FarsiToArabic.ToArabic(PersonEntityParam.BirthPlaceSection.Trim());
            parameters[14].Value = (PersonEntityParam.IssuePlaceCityId==null?System.DBNull.Value:(object)PersonEntityParam.IssuePlaceCityId);
            parameters[15].Value = FarsiToArabic.ToArabic(PersonEntityParam.IssuePlaceSection.Trim());
            parameters[16].Value = (PersonEntityParam.IssueDate==""?System.DBNull.Value:(object)PersonEntityParam.IssueDate);
            parameters[17].Value = (PersonEntityParam.CertificateAlphabetId==null?System.DBNull.Value:(object)PersonEntityParam.CertificateAlphabetId);
            parameters[18].Value = PersonEntityParam.CertificateNumericSeries;
            parameters[19].Value = PersonEntityParam.CertificateSerialNo.Trim();
            parameters[20].Value = FarsiToArabic.ToArabic(PersonEntityParam.EnglishFirstName.Trim());
            parameters[21].Value = FarsiToArabic.ToArabic(PersonEntityParam.EnglishLastName.Trim());
            parameters[22].Value = PersonEntityParam.IsActive;
            parameters[23].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_PersonAdd", parameters);
            var messageError = parameters[23].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            PersonId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdatePersonDB(PersonEntity PersonEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@GenderId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@BloodGroupId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@HasNationalNo", SqlDbType.Bit),
                                            new SqlParameter("@NationalNo", SqlDbType.NVarChar, 10),
                                            new SqlParameter("@FirstName", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@LastName", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@PreviousLastName", SqlDbType.NVarChar, 200),
                                            new SqlParameter("@FatherName", SqlDbType.NVarChar, 50),
                                            new SqlParameter("@BirthDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@DeathDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@CertificateNo", SqlDbType.NVarChar, 50),
                                            new SqlParameter("@BirthPlaceCityId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@BirthPlaceSection", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@IssuePlaceCityId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@IssuePlaceSection", SqlDbType.NVarChar, 100),
                                            new SqlParameter("@IssueDate", SqlDbType.SmallDateTime),
                                            new SqlParameter("@CertificateAlphabetId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@CertificateNumericSeries", SqlDbType.NVarChar, 10),
                                            new SqlParameter("@CertificateSerialNo", SqlDbType.NVarChar, 10),
                                            new SqlParameter("@EnglishFirstName", SqlDbType.NVarChar, 50),
                                            new SqlParameter("@EnglishLastName", SqlDbType.NVarChar, 100),                                            
                                            new SqlParameter("@IsActive", SqlDbType.Bit),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };

            parameters[0].Value = PersonEntityParam.PersonId;
            parameters[1].Value = PersonEntityParam.GenderId;
            parameters[2].Value = (PersonEntityParam.BloodGroupId == null ? System.DBNull.Value : (object)PersonEntityParam.BloodGroupId);
            parameters[3].Value = PersonEntityParam.HasNationalNo;
            parameters[4].Value = PersonEntityParam.NationalNo;
            parameters[5].Value = FarsiToArabic.ToArabic(PersonEntityParam.FirstName.Trim());
            parameters[6].Value = FarsiToArabic.ToArabic(PersonEntityParam.LastName.Trim());
            parameters[7].Value = FarsiToArabic.ToArabic(PersonEntityParam.PreviousLastName.Trim());
            parameters[8].Value = FarsiToArabic.ToArabic(PersonEntityParam.FatherName.Trim());
            parameters[9].Value = PersonEntityParam.BirthDate;
            parameters[10].Value = (PersonEntityParam.DeathDate == "" ? System.DBNull.Value : (object)PersonEntityParam.DeathDate);
            parameters[11].Value = PersonEntityParam.CertificateNo;
            parameters[12].Value = PersonEntityParam.BirthPlaceCityId;
            parameters[13].Value = FarsiToArabic.ToArabic(PersonEntityParam.BirthPlaceSection.Trim());
            parameters[14].Value = (PersonEntityParam.IssuePlaceCityId == null ? System.DBNull.Value : (object)PersonEntityParam.IssuePlaceCityId);            
            parameters[15].Value = FarsiToArabic.ToArabic(PersonEntityParam.IssuePlaceSection.Trim());
            parameters[16].Value = (PersonEntityParam.IssueDate == "" ? System.DBNull.Value : (object)PersonEntityParam.IssueDate);
            parameters[17].Value = (PersonEntityParam.CertificateAlphabetId == null ? System.DBNull.Value : (object)PersonEntityParam.CertificateAlphabetId);
            parameters[18].Value = PersonEntityParam.CertificateNumericSeries;
            parameters[19].Value = PersonEntityParam.CertificateSerialNo.Trim();
            parameters[20].Value = FarsiToArabic.ToArabic(PersonEntityParam.EnglishFirstName.Trim());
            parameters[21].Value = FarsiToArabic.ToArabic(PersonEntityParam.EnglishLastName.Trim());
            parameters[22].Value = PersonEntityParam.IsActive;
            parameters[23].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("General.p_PersonUpdate", parameters);
            var messageError = parameters[23].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeletePersonDB(PersonEntity PersonEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
                                            new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier),
                                            new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
                                        };
            parameters[0].Value = PersonEntityParam.PersonId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("General.p_PersonDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public PersonEntity GetSinglePersonDB(PersonEntity PersonEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
                                                  new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier)
                                              };
                parameters[0].Value = PersonEntityParam.PersonId;

                reader = _intranetDB.RunProcedureReader("General.p_PersonGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetPersonDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<PersonEntity> GetAllPersonDB()
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            return GetPersonDBCollectionFromDataReader(
                intranetDB.RunProcedureReader
                    ("General.p_PersonGetAll", new IDataParameter[] {}));
        }

        public List<PersonEntity> GetPagePersonDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            var intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
                                              new SqlParameter("@PageSize", SqlDbType.Int),
                                              new SqlParameter("@CurrentPage", SqlDbType.Int),
                                              new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                                              new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                                              new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                                              new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
                                          };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "General.t_Person";
            parameters[5].Value = "PersonId";
            DataSet ds = intranetDB.RunProcedureDS("General.p_TablesGetPage", parameters);
            return GetPersonDBCollectionFromDataSet(ds, out count);
        }

        public PersonEntity GetPersonDBFromDataReader(IDataReader reader)
        {
            return new PersonEntity(new Guid(reader["PersonId"].ToString()),
                                    Guid.Parse(reader["GenderId"].ToString()),
                                    Convert.IsDBNull(reader["BloodGroupId"]) ? null : (Guid?)reader["BloodGroupId"],                           
                                    bool.Parse(reader["HasNationalNo"].ToString()),
                                    reader["NationalNo"].ToString(),
                                    reader["FirstName"].ToString(),
                                    reader["LastName"].ToString(),
                                    reader["PreviousLastName"].ToString(),
                                    reader["FatherName"].ToString(),
                                    reader["BirthDate"].ToString(),
                                    reader["DeathDate"].ToString(),
                                    reader["CertificateNo"].ToString(),
                                    Convert.IsDBNull(reader["BirthPlaceCityId"]) ? null : (Guid?)reader["BirthPlaceCityId"],                                       
                                    reader["BirthPlaceSection"].ToString(),
                                    Convert.IsDBNull(reader["IssuePlaceCityId"]) ? null : (Guid?)reader["IssuePlaceCityId"],                                         
                                    reader["IssuePlaceSection"].ToString(),
                                    reader["IssueDate"].ToString(),
                                    Convert.IsDBNull(reader["CertificateAlphabetId"]) ? null : (Guid?)reader["CertificateAlphabetId"],                                     
                                    reader["CertificateNumericSeries"].ToString(),
                                    reader["CertificateSerialNo"].ToString(),
                                    reader["EnglishFirstName"].ToString(),
                                    reader["EnglishLastName"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<PersonEntity> GetPersonDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<PersonEntity> lst = new List<PersonEntity>();
                while (reader.Read())
                    lst.Add(GetPersonDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<PersonEntity> GetPersonDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetPersonDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<PersonEntity> GetPersonDBCollectionByBloodGroupDB(int BloodGroupId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@BloodGroupId", SqlDbType.UniqueIdentifier);
            parameter.Value = BloodGroupId;
            return GetPersonDBCollectionFromDataReader(_intranetDB.RunProcedureReader("General.p_PersonGetByBloodGroup", new[] {parameter}));
        }

        public List<PersonEntity> GetPersonDBCollectionByBirthPlaceCityDB(int BirthPlaceCityId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@BirthPlaceCityId", SqlDbType.UniqueIdentifier);
            parameter.Value = BirthPlaceCityId;
            return GetPersonDBCollectionFromDataReader(_intranetDB.RunProcedureReader("General.p_PersonGetByBirthPlaceCity", new[] {parameter}));
        }

        public List<PersonEntity> GetPersonDBCollectionByIssuePlaceCityDB(int IssuePlaceCityId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@IssuePlaceCityId", SqlDbType.UniqueIdentifier);
            parameter.Value = IssuePlaceCityId;
            return GetPersonDBCollectionFromDataReader(_intranetDB.RunProcedureReader("General.p_PersonGetByIssuePlaceCity", new[] {parameter}));
        }

        public List<PersonEntity> GetPersonDBCollectionByGenderDB(int GenderId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@GenderId", SqlDbType.UniqueIdentifier);
            parameter.Value = GenderId;
            return GetPersonDBCollectionFromDataReader(_intranetDB.RunProcedureReader("General.p_PersonGetByGender", new[] {parameter}));
        }

        #endregion

    }

    #endregion
}
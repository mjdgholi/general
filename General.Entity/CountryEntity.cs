﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/08>
    /// Description: <کشور>
    /// </summary>
   public class CountryEntity
    {
       #region Properties :

        [DisplayName("شناسه کشور")]
       public Guid CountryId { get; set; }
         [DisplayName("عنوان فارسی کشور")]
        public string CountryTitlePersian { get; set; }
        [DisplayName("عنوان انگلیسی کشور")]
        public string CountryTitleEnglish { get; set; }
        [DisplayName("تاریخ ایجاد رکورد")]
        public string CreationDate { get; set; }
        [DisplayName("تاریخ ویرایش رکورد")]
        public string ModificationDate { get; set; }
        [DisplayName("وضیعت رکورد")]
        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public CountryEntity()
        {
        }

        public CountryEntity(Guid _CountryId, string CountryTitlePersian, string CountryTitleEnglish, string CreationDate, string ModificationDate, bool IsActive)
        {
            CountryId = _CountryId;
            this.CountryTitlePersian = CountryTitlePersian;
            this.CountryTitleEnglish = CountryTitleEnglish;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}

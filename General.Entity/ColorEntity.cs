﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
   public class ColorEntity
    {
             #region Properties :

        public Guid ColorId { get; set; }
        public string ColorTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public ColorEntity()
        {
        }

        public ColorEntity(Guid _ColorId, string ColorTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            ColorId = _ColorId;
            this.ColorTitle = ColorTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}

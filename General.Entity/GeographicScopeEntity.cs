﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/06>
    /// Description: <حوزه جغرافیایی>
    /// </summary>
   public class GeographicScopeEntity
    {
                #region Properties :

        public Guid GeographicScopeId { get; set; }
        public string GeographicScopePersianTitle { get; set; }

        public string GeographicScopeEnglishTitle { get; set; }

        public int Priority { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public GeographicScopeEntity()
        {
        }

        public GeographicScopeEntity(Guid _GeographicScopeId, string GeographicScopePersianTitle, string GeographicScopeEnglishTitle, int Priority, string CreationDate, string ModificationDate, bool IsActive)
        {
            GeographicScopeId = _GeographicScopeId;
            this.GeographicScopePersianTitle = GeographicScopePersianTitle;
            this.GeographicScopeEnglishTitle = GeographicScopeEnglishTitle;
            this.Priority = Priority;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}

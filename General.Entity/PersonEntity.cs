﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/06>
    // Description:	<فرم اشخاص>
    /// </summary>
    public class PersonEntity
    {
        #region Properties :

        public Guid PersonId { get; set; }
        public Guid GenderId { get; set; }

        public Guid? BloodGroupId { get; set; }

        public bool HasNationalNo { get; set; }

        public string NationalNo { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PreviousLastName { get; set; }

        public string FatherName { get; set; }

        public string BirthDate { get; set; }

        public string DeathDate { get; set; }

        public string CertificateNo { get; set; }

        public Guid? BirthPlaceCityId { get; set; }

        public string BirthPlaceSection { get; set; }

        public Guid? IssuePlaceCityId { get; set; }

        public string IssuePlaceSection { get; set; }

        public string IssueDate { get; set; }

        public Guid? CertificateAlphabetId { get; set; }

        public string CertificateNumericSeries { get; set; }

        public string CertificateSerialNo { get; set; }

        public string EnglishFirstName { get; set; }

        public string EnglishLastName { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public PersonEntity()
        {
        }

        public PersonEntity(Guid personId, Guid genderId, Guid? bloodGroupId, bool hasNationalNo, string nationalNo, string firstName, string lastName, string previousLastName, string fatherName, string birthDate, string DeathDate, string CertificateNo, Guid? BirthPlaceCityId, string BirthPlaceSection, Guid? IssuePlaceCityId, string IssuePlaceSection, string IssueDate, Guid? CertificateAlphabetId, string CertificateNumericSeries, string CertificateSerialNo, string EnglishFirstName, string EnglishLastName, string CreationDate, string ModificationDate, bool IsActive)
        {
            PersonId = personId;
            this.GenderId = genderId;
            this.BloodGroupId = bloodGroupId;
            this.HasNationalNo = hasNationalNo;
            this.NationalNo = nationalNo;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.PreviousLastName = previousLastName;
            this.FatherName = fatherName;
            this.BirthDate = birthDate;
            this.DeathDate = DeathDate;
            this.CertificateNo = CertificateNo;
            this.BirthPlaceCityId = BirthPlaceCityId;
            this.BirthPlaceSection = BirthPlaceSection;
            this.IssuePlaceCityId = IssuePlaceCityId;
            this.IssuePlaceSection = IssuePlaceSection;
            this.IssueDate = IssueDate;
            this.CertificateAlphabetId = CertificateAlphabetId;
            this.CertificateNumericSeries = CertificateNumericSeries;
            this.CertificateSerialNo = CertificateSerialNo;
            this.EnglishFirstName = EnglishFirstName;
            this.EnglishLastName = EnglishLastName;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
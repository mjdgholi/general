﻿namespace GeneralProject.General.Entity
{
    public class ModuleArrangementEntity
    {
         
        #region Properties :

        public int ModuleArrangementId { get; set; }
        public int ModuleId { get; set; }

        public string MainPicPath { get; set; }

        public string MouseOverPicPath { get; set; }

        public string PortalUrl { get; set; }

        public string ModuleToolTip { get; set; }

        public int ModuleOrder { get; set; }

        public bool IsAllUser { get; set; }

        public bool IsKnownUser { get; set; }

        public bool IsActive { get; set; }

        public int CreationUserId { get; set; }

        public int ModificationUserId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsInternal { get; set; }

        public int? OwnerId { get; set; }



        #endregion

        #region Constrauctors :

        public ModuleArrangementEntity()
        {
        }

        public ModuleArrangementEntity(int _ModuleArrangementId, int ModuleId, string MainPicPath, string MouseOverPicPath, string PortalUrl, string ModuleToolTip, int ModuleOrder, bool IsAllUser, bool IsKnownUser, bool IsActive, int CreationUserId, int ModificationUserId, string CreationDate, string ModificationDate, bool IsInternal, int? OwnerId)
        {
            ModuleArrangementId = _ModuleArrangementId;
            this.ModuleId = ModuleId;
            this.MainPicPath = MainPicPath;
            this.MouseOverPicPath = MouseOverPicPath;
            this.PortalUrl = PortalUrl;
            this.ModuleToolTip = ModuleToolTip;
            this.ModuleOrder = ModuleOrder;
            this.IsAllUser = IsAllUser;
            this.IsKnownUser = IsKnownUser;
            this.IsActive = IsActive;
            this.CreationUserId = CreationUserId;
            this.ModificationUserId = ModificationUserId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsInternal = IsInternal;
            this.OwnerId = OwnerId;

        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/09>
    /// Description: <نوع وابستگی>
    /// </summary>
  public  class DependentTypeEntity
    {
        
        #region Properties :

        public Guid DependentTypeId { get; set; }
        public string DependentTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public DependentTypeEntity()
        {
        }

        public DependentTypeEntity(Guid _DependentTypeId, string DependentTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            DependentTypeId = _DependentTypeId;
            this.DependentTypeTitle = DependentTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}

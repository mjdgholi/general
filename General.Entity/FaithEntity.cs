﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/07>
    /// Description: <دین>
    /// </summary>
    
    public class FaithEntity
    {
        #region Properties :

        [DisplayName("شناسه دین")]
        public Guid FaithId { get; set; }

        [DisplayName("عنوان دین")]
        public string FaithTitle { get; set; }

        [DisplayName("تاریخ ایجاد")]
        public string CreationDate { get; set; }

        [DisplayName("تاریخ ویرایش")]
        public string ModificationDate { get; set; }

        [DisplayName("آیا فعال است؟")]
        public bool IsActive { get; set; }

        #endregion

        public FaithEntity()
        {
        }

        public FaithEntity(Guid faithId, string faithTitle, string creationDate, string modificationDate, bool isActive)
        {
            FaithId = faithId;
            this.FaithTitle = faithTitle;
            this.CreationDate = creationDate;
            this.ModificationDate = modificationDate;
            this.IsActive = isActive;
        }
    }
}
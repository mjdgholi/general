﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/08>
    /// Description: <مذهب>
    /// </summary>

   public class ReligionEntity
    {
               #region Properties :

        public Guid ReligionId { get; set; }
        public string ReligionTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public ReligionEntity()
        {
        }

        public ReligionEntity(Guid _ReligionId, string ReligionTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            ReligionId = _ReligionId;
            this.ReligionTitle = ReligionTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}

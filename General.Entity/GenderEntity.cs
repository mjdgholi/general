﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/06>
    // Description:	<جنسیت>
    /// </summary>
    public class GenderEntity
    {
        #region Properties :

        public Guid GenderId { get; set; }
        public string GenderTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public GenderEntity()
        {
        }

        public GenderEntity(Guid _GenderId, string GenderTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            GenderId = _GenderId;
            this.GenderTitle = GenderTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
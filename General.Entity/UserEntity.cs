﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralProject.General.Entity
{
   public class UserEntity
    {
        

        #region Properties :

        public int UserID { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserPass { get; set; }

        public string Email { get; set; }

        public string UserStyle { get; set; }

        public string UserName { get; set; }

        public int PortalID { get; set; }

        public bool IsSuperUser { get; set; }

        public bool IsLocked { get; set; }

        public DateTime LastLoginDate { get; set; }

        public DateTime LastPasswordChangeDate { get; set; }

        public DateTime LastLockDate { get; set; }

        public DateTime LastUnLockDate { get; set; }

        public int FailedPasswordAttemptCount { get; set; }

        public DateTime FailedPasswordAttemptStart { get; set; }

        public string Mobile { get; set; }

        public bool HasChangedPassword { get; set; }

        public DateTime JoinDate { get; set; }

        public DateTime ConfirmDate { get; set; }

        public bool IsAnonymous { get; set; }

        public Guid UserGuid { get; set; }

        public string LastIP { get; set; }
       public string FullName { get; set; }


        #endregion

        #region Constrauctors :

        public UserEntity()
        {
        }

        public UserEntity(int _UserID, string FirstName, string LastName, string UserPass, string Email, string UserStyle, string UserName, int PortalID, bool IsSuperUser, bool IsLocked, DateTime LastLoginDate, DateTime LastPasswordChangeDate, DateTime LastLockDate, DateTime LastUnLockDate, int FailedPasswordAttemptCount, DateTime FailedPasswordAttemptStart, string Mobile, bool HasChangedPassword, DateTime JoinDate, DateTime ConfirmDate, bool IsAnonymous, Guid UserGuid, string LastIP,string FullName)
        {
            UserID = _UserID;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.UserPass = UserPass;
            this.Email = Email;
            this.UserStyle = UserStyle;
            this.UserName = UserName;
            this.PortalID = PortalID;
            this.IsSuperUser = IsSuperUser;
            this.IsLocked = IsLocked;
            this.LastLoginDate = LastLoginDate;
            this.LastPasswordChangeDate = LastPasswordChangeDate;
            this.LastLockDate = LastLockDate;
            this.LastUnLockDate = LastUnLockDate;
            this.FailedPasswordAttemptCount = FailedPasswordAttemptCount;
            this.FailedPasswordAttemptStart = FailedPasswordAttemptStart;
            this.Mobile = Mobile;
            this.HasChangedPassword = HasChangedPassword;
            this.JoinDate = JoinDate;
            this.ConfirmDate = ConfirmDate;
            this.IsAnonymous = IsAnonymous;
            this.UserGuid = UserGuid;
            this.LastIP = LastIP;
            this.FullName = FullName;

        }

        #endregion
    }
}

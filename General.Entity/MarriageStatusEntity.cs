﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/07>
    /// Description: <وضیعت تاهل>
    /// </summary>
  public  class MarriageStatusEntity
    {
                 #region Properties :

        public Guid MarriageStatusId { get; set; }
        public string MarriageStatusTitle { get; set; }

        public bool IsMarried { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public MarriageStatusEntity()
        {
        }

        public MarriageStatusEntity(Guid _MarriageStatusId, string MarriageStatusTitle, bool IsMarried, string CreationDate, string ModificationDate, bool IsActive)
        {
            MarriageStatusId = _MarriageStatusId;
            this.MarriageStatusTitle = MarriageStatusTitle;
            this.IsMarried = IsMarried;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}

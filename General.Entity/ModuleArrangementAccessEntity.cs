﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/08/04>
    /// Description: <دسترسی به آیکن منو>
    /// </summary>
   public class ModuleArrangementAccessEntity
    {
                #region Properties :

        public int ModuleArrangementAccessId { get; set; }
        public int UserId { get; set; }

        public int ModuleArrangementId { get; set; }
        public int ModuleArrangementOwnerId  { get; set; }

        #endregion

        #region Constrauctors :

        public ModuleArrangementAccessEntity()
        {
        }

        public ModuleArrangementAccessEntity(int _ModuleArrangementAccessId, int UserId, int ModuleArrangementId, int ModuleArrangementOwnerId)
        {
            ModuleArrangementAccessId = _ModuleArrangementAccessId;
            this.UserId = UserId;
            this.ModuleArrangementId = ModuleArrangementId;
            this.ModuleArrangementOwnerId = ModuleArrangementOwnerId;
        }

        #endregion
    }
}

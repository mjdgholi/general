﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/08>
    /// Description: <وضیعت ملیت>
    /// </summary>
  public  class NationalityEntity
    {
                #region Properties :

        public Guid NationalityId { get; set; }
        public Guid CountryId { get; set; }

        public string NationalityTitlePersian { get; set; }

        public string NationalityTitleEnglish { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public NationalityEntity()
        {
        }

        public NationalityEntity(Guid _NationalityId, Guid CountryId, string NationalityTitlePersian, string NationalityTitleEnglish, string CreationDate, string ModificationDate, bool IsActive)
        {
            NationalityId = _NationalityId;
            this.CountryId = CountryId;
            this.NationalityTitlePersian = NationalityTitlePersian;
            this.NationalityTitleEnglish = NationalityTitleEnglish;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/06>
    /// Description: <استان>
    /// </summary>
 public class ProvinceEntity
    {
      #region Properties :

        public Guid ProvinceId { get; set; }
        public Guid CountryId { get; set; }

        public string ProvinceTitlePersian { get; set; }

        public string ProvinceTitleEnglish { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }

        #endregion

        #region Constrauctors :

        public ProvinceEntity()
        {
        }

        public ProvinceEntity(Guid _ProvinceId, Guid CountryId, string ProvinceTitlePersian, string ProvinceTitleEnglish, string CreationDate, string ModificationDate, bool IsActive)
        {
            ProvinceId = _ProvinceId;
            this.CountryId = CountryId;
            this.ProvinceTitlePersian = ProvinceTitlePersian;
            this.ProvinceTitleEnglish = ProvinceTitleEnglish;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}

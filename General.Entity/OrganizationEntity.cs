﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/02/04>
    /// Description: <سازمان>
    /// </summary>

   public class OrganizationEntity
    {
        #region Properties :

        public Guid OrganizationId { get; set; }
        public string OrganizationPersianTitle { get; set; }

        public string OrganizationEnglishTitle { get; set; }

        public Guid? CityId { get; set; }

        public string OrganizationEconomicCode { get; set; }

        public string OrganizationTelephoneNo { get; set; }

        public string OrganizationFoxNo { get; set; }

        public string OrganizationAddress { get; set; }

        public string OrganizationWebSite { get; set; }

        public string OrganizationEmail { get; set; }

        public string ZipCode { get; set; }

        public string OrganizationDescription { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }
        public bool IsActive { get; set; }

        public Guid? ProvinceId { get; set; }

        #endregion

        #region Constrauctors :

        public OrganizationEntity()
        {
        }

        public OrganizationEntity(Guid _OrganizationId, string OrganizationPersianTitle, string OrganizationEnglishTitle, Guid? CityId, string OrganizationEconomicCode, string OrganizationTelephoneNo, string OrganizationFoxNo, string OrganizationAddress, string OrganizationWebSite, string OrganizationEmail, string ZipCode, string OrganizationDescription, string CreationDate, string ModificationDate, bool IsActive, Guid? PrivinceId)
        {
            OrganizationId = _OrganizationId;
            this.OrganizationPersianTitle = OrganizationPersianTitle;
            this.OrganizationEnglishTitle = OrganizationEnglishTitle;
            this.CityId = CityId;
            this.OrganizationEconomicCode = OrganizationEconomicCode;
            this.OrganizationTelephoneNo = OrganizationTelephoneNo;
            this.OrganizationFoxNo = OrganizationFoxNo;
            this.OrganizationAddress = OrganizationAddress;
            this.OrganizationWebSite = OrganizationWebSite;
            this.OrganizationEmail = OrganizationEmail;
            this.ZipCode = ZipCode;
            this.OrganizationDescription = OrganizationDescription;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;
            this.ProvinceId = PrivinceId;
        }

        #endregion
    }
}

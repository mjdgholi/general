﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/06>
    // Description:	<فرم شناسه حرفی شناسنامه>
    /// </summary>
    public class CertificateAlphabetEntity
    {
        #region Properties :

        public Guid CertificateAlphabetId { get; set; }
        public string CertificateAlphabetTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }

        #endregion

        #region Constrauctors :

        public CertificateAlphabetEntity()
        {
        }

        public CertificateAlphabetEntity(Guid _CertificateAlphabetId, string CertificateAlphabetTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            CertificateAlphabetId = _CertificateAlphabetId;
            this.CertificateAlphabetTitle = CertificateAlphabetTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/06>
    /// Description: <شهر>
    /// </summary>
 public   class CityEntity
    {
        		#region Properties :
		
		public Guid? CityId{get;set;}
		public Guid ProvinceId{get; set;}

  public Guid GeographicScopeId{get; set;}

  public string CityTitlePersian{get; set;}

  public string CityTitleEnglish{get; set;}

  public string CreationDate{get; set;}

  public string ModificationDate{get; set;}

  public bool IsActive{get; set;}

  
		
		#endregion
		
		#region Constrauctors :
		
		public CityEntity()
		{
		}

        public CityEntity(Guid? _CityId, Guid ProvinceId, Guid GeographicScopeId, string CityTitlePersian, string CityTitleEnglish, string CreationDate, string ModificationDate, bool IsActive)
        {
            CityId = _CityId;
            this.ProvinceId = ProvinceId;
            this.GeographicScopeId = GeographicScopeId;
            this.CityTitlePersian = CityTitlePersian;
            this.CityTitleEnglish = CityTitleEnglish;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <M.Gholibeygian>
    /// Create date: <1393/01/27>
    /// Description: <بانک>
    /// </summary>
    public class BankEntity
    {

        #region Properties :

        public Guid BankId { get; set; }
        public string BankTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public BankEntity()
        {
        }

        public BankEntity(Guid _BankId, string BankTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            BankId = _BankId;
            this.BankTitle = BankTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    // Author:		<majid Gholibeygian>
    // Create date: <1393/01/06>
    // Description:	<فرم گروه خونی>
    /// </summary>
    public class BloodGroupEntity
    {
        #region Properties :

        public Guid BloodGroupId { get; set; }
        public string BloodGroupTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public BloodGroupEntity()
        {
        }

        public BloodGroupEntity(Guid _BloodGroupId, string BloodGroupTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            BloodGroupId = _BloodGroupId;
            this.BloodGroupTitle = BloodGroupTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
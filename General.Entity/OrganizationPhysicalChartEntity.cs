﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.GeneralProject.General.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/01/23>
    /// Description: <واحد سازمانی>
    /// </summary>
  public  class OrganizationPhysicalChartEntity
    {
                #region Properties :

        public Guid? OrganizationPhysicalChartId { get; set; }
        public Guid? OwnerId { get; set; }

        public string OrganizationPhysicalChartTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool? IsActive { get; set; }
        public int? ChildCount   { get; set; }
        public int? FlagSearch { get; set; }

        #endregion

        #region Constrauctors :

        public OrganizationPhysicalChartEntity()
        {
        }

        public OrganizationPhysicalChartEntity(Guid? _OrganizationPhysicalChartId, Guid? OwnerId, string OrganizationPhysicalChartTitle, string CreationDate, string ModificationDate, bool? IsActive, int? ChildCount, int? FlagSearch)
        {
            OrganizationPhysicalChartId = _OrganizationPhysicalChartId;
            this.OwnerId = OwnerId;
            this.OrganizationPhysicalChartTitle = OrganizationPhysicalChartTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;
            this.ChildCount = ChildCount;
            this.FlagSearch = FlagSearch;
        }

        #endregion
    }
}
